{-# LANGUAGE QuasiQuotes #-}
module Foundation (
    head',
    App(..),
    AppMessage(..),
    Form,
    Handler,
    Route(..),
    Widget,
    resourcesApp,
    unsafeHandler,
    languageName) where

import           ClassyPrelude.Yesod
import           Network.Mail.Mime
import           Text.Blaze.Html.Renderer.Utf8 (renderHtml)
import           Text.Hamlet                   (hamletFile)
import           Text.Shakespeare.Text         (stext)
import           Yesod.Auth
import           Yesod.Auth.BrowserId          (authBrowserId)
import           Yesod.Auth.Email
import           Yesod.Core.Types              (Logger)
import qualified Yesod.Core.Unsafe             as Unsafe

import qualified Database                      as D
import           Model                         as M
import           Settings
import           TableColumns

staticFiles "static"

-- | The foundation datatype for your application. This can be a good place to
-- keep settings and values requiring initialization before your application
-- starts running, such as database connections. Every handler will have
-- access to the data present here.
data App = App
    { appSettings    :: AppSettings
    , appStatic      :: Static -- ^ Settings for static file serving.
    , appServant     :: WaiSubsite
    , appDatabase    :: D.Database
    , appHttpManager :: Manager
    , appLogger      :: Logger
    }

mkYesodData "App" $(parseRoutesFile "config/routes")

mkMessage "App" "messages" "en"

plural :: Int -> String -> String -> String
plural 1 x _ = x
plural _ _ y = y

-- | get the first item in a list or the default value
head' :: [t] -> t -> t
head' [] a = a
head' (x:_) _ = x

-- | A convenient synonym for creating forms.
type Form x = Html -> MForm (HandlerT App IO) (FormResult x, Widget)

instance Yesod App where
    -- get root from settings
    approot = ApprootMaster (appRoot . appSettings)

    defaultLayout widget = do
        auth <- maybeAuth
        let user = case auth of
                Nothing -> Nothing
                Just (Entity uid (User name _ _ _ _ _)) -> Just (uid, name)
        pc <- widgetToPageContent widget
        mmessage <- getMessage
        langs <- languages
        let lang = getSupportedLanguage langs
        setLanguage lang
        r <- getMessageRender
        withUrlRenderer $(hamletFile "templates/page-wrapper.hamlet")

    -- allow uploads of 2MB
    maximumContentLength _ _ = Just 2097152

    authRoute _ = Just $ AuthR LoginR

    -- | Who is allowed to access which pages?
    isAuthorized NewJobR{} _ = isLoggedIn
    isAuthorized (PageEditR pageid) _ = pageOwner pageid >>= ownerIs
    isAuthorized PageNewR _ = isLoggedIn
    isAuthorized (CommentEditR commentid) _ = commentOwner commentid >>= ownerIs
    isAuthorized (CommentNewR _) _ = isLoggedIn
    isAuthorized UploadsR _ = isLoggedIn
    isAuthorized (UserEditR userid) _  = ownerIs userid
    isAuthorized _ _ = return Authorized

ownerIs :: AuthId App -> Handler AuthResult
ownerIs userid = do
    mu <- maybeAuthId
    return $ case mu of
        Nothing -> AuthenticationRequired
        Just userid' | userid == userid' -> Authorized
        Just _ -> Unauthorized "This is not your page."

pageOwner :: PageId -> Handler UserId
pageOwner pageid = do
    prose <- runDB $ do
        page <- get404 pageid
        get404 $ pageCurrent page
    return $ proseAuthor prose

commentOwner :: CommentId -> Handler UserId
commentOwner commentid = do
    prose <- runDB $ do
        comment <- get404 commentid
        get404 $ commentComment comment
    return $ proseAuthor prose

availableLanguages :: [(Lang,Text)]
availableLanguages =
    [ ( "de", "deutsch")
    , ( "en", "english")
    , ( "fr", "français")
    , ( "it", "italiano")
    , ( "nl", "nederlands")
    , ( "zh_CN", "简体中文(中国)")
    , ( "zh_TW", "正體中文(台灣)")
    ]

languageName :: Lang -> Text
languageName lang = head' filtered lang
  where filtered = map snd $ filter (\(l,_) -> lang == l) availableLanguages

getSupportedLanguage :: [Text] -> Text
getSupportedLanguage langs = head' supported "en"
  where
    langs' = langs ++ map (take 2) langs
    allLangs = map fst availableLanguages
    supported = filter (`elem` allLangs) langs'

instance YesodPersist App where
    type YesodPersistBackend App = SqlBackend
    runDB action = do
        App _ _ _ db _ _ <- getYesod
        D.runDb action db

authenticateUser :: (YesodPersist site, YesodPersistBackend site ~ SqlBackend, AuthId master ~ Key User) => Text -> HandlerT site IO (AuthenticationResult master)
authenticateUser ident = runDB $ do
    let email = ident
    joined <- liftIO getCurrentTime
    userid <- D.insertOrGet $ User email email Nothing Nothing False joined
    return $ Authenticated userid

instance YesodAuth App where
    type AuthId App = UserId

    authenticate (Creds plugin ident _) | plugin == "browserid" =
        -- browerid uses email address as identifier
        authenticateUser ident

    authenticate (Creds plugin ident _)
            | plugin == "email" || plugin == "email-verify" =
        authenticateUser ident

    authenticate (Creds plugin _ _) = do
        liftIO $ putStrLn ("authenticate plugin '" ++ plugin ++ "'")
        return $ ServerError "Not implemented"

    loginDest _ = HomeR
    logoutDest _ = HomeR

    authPlugins _ = [authEmail,authBrowserId def]

    authHttpManager = appHttpManager

instance YesodAuthEmail App where
    type AuthEmailId App = UserId

    afterPasswordRoute _ = HomeR

    addUnverified email verkey = do
        joined <- liftIO getCurrentTime
        runDB $ insert $ User email email Nothing (Just verkey) False joined

    sendVerifyEmail email _ verurl =
        liftIO $ renderSendMail (emptyMail $ Address Nothing "noreply")
            { mailTo = [Address Nothing email]
            , mailHeaders =
                [ ("From", "ODF Community Server <noreply@odf.blobbox.org>")
                , ("Subject", "Verify your email address")
                ]
            , mailParts = [[textPart, htmlPart']]
            }
      where
        textPart = Part
            { partType = "text/plain; charset=utf-8"
            , partEncoding = None
            , partFilename = Nothing
            , partContent = encodeUtf8
                [stext|
                    Please confirm your email address by clicking on the link below.

                    #{verurl}

                    Cookies must be enabled!

                    Thank you
                |]
            , partHeaders = []
            }
        htmlPart' = Part
            { partType = "text/html; charset=utf-8"
            , partEncoding = None
            , partFilename = Nothing
            , partContent = renderHtml
                [shamlet|
                    <p>Please confirm your email address by clicking on the link below.
                    <p>
                        <a href=#{verurl}>#{verurl}
                    <p>Cookies must be enabled!
                    <p>Thank you
                |]
            , partHeaders = []
            }
    getVerifyKey = runDB . fmap (join . fmap userVerkey) . get
    setVerifyKey uid key = runDB $ update uid [UserVerkey =. Just key]
    verifyAccount uid = runDB $ do
        mu <- get uid
        case mu of
            Nothing -> return Nothing
            Just _ -> do
                update uid [UserActive =. True]
                return $ Just uid
    getPassword = runDB . fmap (join . fmap userPassword) . get
    setPassword uid pass = runDB $ update uid [UserPassword =. Just pass]
    getEmailCreds email = runDB $ do
        mu <- getBy $ UniqueUser email
        case mu of
            Nothing -> return Nothing
            Just (Entity uid u) -> return $ Just EmailCreds
                { emailCredsId = uid
                , emailCredsAuthId = Just uid
                , emailCredsStatus = isJust $ userPassword u
                , emailCredsVerkey = userVerkey u
                , emailCredsEmail = email
                }
    getEmail = runDB . fmap (fmap userEmail) . get

instance YesodAuthPersist App where
    type AuthEntity App = User

instance RenderMessage App FormMessage where
    renderMessage _ _ = defaultFormMessage

isLoggedIn :: HandlerT App IO AuthResult
isLoggedIn = do
    mu <- maybeAuthId
    return $ case mu of
        Nothing -> AuthenticationRequired
        _ -> Authorized

unsafeHandler :: App -> Handler a -> IO a
unsafeHandler = Unsafe.fakeHandlerGetLogger appLogger
