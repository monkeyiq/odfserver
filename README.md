# Compiling the server

The odf server requires Yesod, a Haskell web framework. This is available on Windows, Mac OS and Linux. So far it has been tested in NixOS, Ubuntu and Debian.

## Getting bootstrap

ODF server uses bootstrap for styling. The required bootstrap files are not part of the git repository but should be downloaded separately:
```
git clone https://gitlab.com/odfplugfest/odfserver.git
wget https://github.com/twbs/bootstrap/releases/download/v3.3.6/bootstrap-3.3.6-dist.zip
unzip -d static bootstrap-3.3.6-dist.zip
```

## Compiling on Ubuntu

```bash
sudo apt-get install git ghc cabal-install happy alex
git clone https://gitlab.com/odfplugfest/odfserver.git
cd odfserver
cabal update
cabal install --dependencies-only # this takes half an hour
cabal configure -f dev -f sqlite --enable-tests
cabal build
```

## Compiling on Debian

The current stable version of Debian (8 a.k.a. Jessie) has GHC 7.6, which is too old. The Nix Package manager can be used to install a recent version of GHC.

```bash
sudo apt-get install curl
curl https://nixos.org/nix/install | sh
. $HOME/.nix-profile/etc/profile.d/nix.sh
```
From this point, follow the instructions for NixOS.

## Compiling on NixOS

```bash
nix-env -i git
git clone https://gitlab.com/odfplugfest/odfserver.git
cd odfserver
nix-shell # opens a shell specific to this package, first time can be slow
cabal configure -f dev -f sqlite --enable-tests
cabal build
```

## Compilation issues

### "failed to map segment from shared object"

If ghc fails with the error "failed to map segment from shared object", run these commands to work around it:
```bash
mkdir temp
export TMPDIR=$(pwd)/temp
cabal build
```

# Developing the server

Once the server is built, it can be run from the compiled version:
```bash
dist/build/odftestserver/odftestserver
```
or run from the language shell (REPL):
```bash
cabal repl
```
In the REPL, the server can be started with `main` and stopped with ctrl-c. When the code has changed, it can be updated with `:reload` in the REPL.

Alternatively, one can use `APPROOT=http://localhost:3000 yesod devel -e '-f sqlite'`.

For HTTPS, a certificat is needed. This can be generated with
```bash
openssl req -newkey rsa:2048 -new -nodes -keyout key.pem -out csr.pem
openssl req -newkey rsa:2048 -new -nodes -x509 -days 3650 -keyout key.pem -out cert.pem
```

# Running tests

Tests can be run interactively. Unless a pattern is provided, all tests are run.
```
$ cabal repl
*Main> :main --match Prose
```
