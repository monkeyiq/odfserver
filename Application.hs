{-# OPTIONS_GHC -fno-warn-orphans #-}
module Application
    ( getApplicationDev
    , appMain
    , develMain
    , makeFoundation
    , makeLogWare
    -- * for DevelMain
    , getApplicationRepl
    , shutdownApp
    -- * for GHCI
    , handler
    , db
    ) where

import           Control.Monad.Logger                 (liftLoc, runLoggingT)
import           Database.Persist.Sql                 (runSqlPool)
import           Import
import           Language.Haskell.TH.Syntax           (qLocation)
import           Network.Wai                          (Middleware)
import           Network.Wai.Handler.Warp             (Settings,
                                                       defaultSettings, defaultShouldDisplayException,
                                                       getPort, setHost,
                                                       setOnException, setPort)
import           Network.Wai.Handler.WarpTLS
import           Network.Wai.Middleware.RequestLogger (Destination (Logger),
                                                       IPAddrSource (..),
                                                       OutputFormat (..),
                                                       destination,
                                                       mkRequestLogger,
                                                       outputFormat)
import           System.Log.FastLogger                (defaultBufSize,
                                                       newStdoutLoggerSet,
                                                       toLogStr)
import           Text.Read                            (read)
import           Text.Regex.Posix

import           Database                             (Database, createDatabase)
import           RPC.Impl                             (ServantConfig (..))
import           ServantApplication

#if SQLITE
import           Database.Persist.Sqlite              (createSqlitePool,
                                                       sqlDatabase)
#else
import           Database.Persist.Postgresql          (createPostgresqlPool,
                                                       pgConnStr, pgPoolSize)
#endif

-- Import all relevant handler modules here.
-- Don't forget to add new modules to your cabal file!
import           Handler.Comment
import           Handler.Common
import           Handler.Feed
import           Handler.Home
import           Handler.Job
import           Handler.Page
import           Handler.Planet
import           Handler.PlanetEdit
import           Handler.PlanetModerate
import           Handler.PlanetUpdate
import           Handler.Sanity
import           Handler.ScoreBoard
import           Handler.Test
import           Handler.TestNew
import           Handler.TestResult
import           Handler.TestSet
import           Handler.TestSetNew
import           Handler.Upload
import           Handler.User

-- This line actually creates our YesodDispatch instance. It is the second half
-- of the call to mkYesodData which occurs in Foundation.hs. Please see the
-- comments there for more details.
mkYesodDispatch "App" resourcesApp

-- | This function allocates resources (such as a database connection pool),
-- performs initialization and returns a foundation datatype value. This is also
-- the place to put your migrate statements to have automatic database
-- migrations handled by Yesod.
makeFoundation :: AppSettings -> IO App
makeFoundation appSettings = do
    -- Some basic initializations: HTTP connection manager, logger, and static
    -- subsite.
    appHttpManager <- newManager
    appLogger <- newStdoutLoggerSet defaultBufSize >>= makeYesodLogger
    appStatic <-
        (if appMutableStatic appSettings then staticDevel else static)
        (appStaticDir appSettings)

    -- We need a log function to create a connection pool. We need a connection
    -- pool to create our foundation. And we need our foundation to get a
    -- logging function. To get out of this loop, we initially create a
    -- temporary foundation without a real connection pool, get a log function
    -- from there, and then create the real foundation.
    let mkFoundation :: Database -> App
        mkFoundation appDatabase = App
            { appSettings    = appSettings
            , appStatic      = appStatic
            , appServant     = WaiSubsite (servantApp $ ServantConfig appDatabase)
            , appDatabase    = appDatabase
            , appHttpManager = appHttpManager
            , appLogger      = appLogger
            }
        -- The App {..} syntax is an example of record wild cards. For more
        -- information, see:
        -- https://ocharles.org.uk/blog/posts/2014-12-04-record-wildcards.html
        tempFoundation = mkFoundation $ error "connPool forced in tempFoundation"
        logFunc = messageLoggerSource tempFoundation appLogger

    -- Create the database connection pool
#if SQLITE
    pool <- flip runLoggingT logFunc $ createSqlitePool
        (sqlDatabase $ appDatabaseConf appSettings)
        1 -- use 1 for pool size to avoid sqlite busy error
--        (sqlPoolSize $ appDatabaseConf appSettings)
#else
    pool <- flip runLoggingT logFunc $ createPostgresqlPool
        (pgConnStr  $ appDatabaseConf appSettings)
        (pgPoolSize $ appDatabaseConf appSettings)
#endif

    -- Perform database migration using our application's logging settings.
    runLoggingT (runSqlPool (runMigration migrateAll) pool) logFunc

    -- Return the foundation
    database <- createDatabase pool
    return $ mkFoundation database

-- | Convert our foundation to a WAI Application by calling @toWaiAppPlain@ and
-- applying some additional middlewares.
makeApplication :: App -> IO Application
makeApplication foundation = do
    logWare <- makeLogWare foundation
    -- Create the WAI application and apply middlewares
    appPlain <- toWaiAppPlain foundation
    return $ logWare $ defaultMiddlewaresNoLogging appPlain

makeLogWare :: App -> IO Middleware
makeLogWare foundation =
    mkRequestLogger def
        { outputFormat =
            if appDetailedRequestLogging $ appSettings foundation
                then Detailed True
                else Apache
                        (if appIpFromHeader $ appSettings foundation
                            then FromFallback
                            else FromSocket)
        , destination = Logger $ loggerSet $ appLogger foundation
        }

parseAppRoot :: Text -> (Text, Int)
parseAppRoot ar = (pack h,port)
  where
    (re :: String) = "^http(s?)://([^:]+)(:([0-9]+))?$"
    (_,_,_,[s,h,_,p]) = unpack ar =~ re :: (String,String,String,[String])
    port = if p == ""
            then if s == "s" then 443 else 80
            else read p

-- | Warp settings for the given foundation value.
warpSettings :: App -> Settings
warpSettings foundation =
      setPort port
    $ setHost (appHost $ appSettings foundation)
    $ setOnException (\_req e ->
        when (defaultShouldDisplayException e) $ messageLoggerSource
            foundation
            (appLogger foundation)
            $(qLocation >>= liftLoc)
            "yesod"
            LevelError
            (toLogStr $ "Exception from Warp: " ++ show e))
      defaultSettings
  where
    (_, port) = parseAppRoot $ appRoot $ appSettings foundation

getApplicationHelper :: IO (Settings, App, Application)
getApplicationHelper = do
    settings <- getAppSettings
    foundation <- makeFoundation settings
    wsettings <- getDevSettings $ warpSettings foundation
    app <- makeApplication foundation
    return (wsettings, foundation, app)

-- | For yesod devel, return the Warp settings and WAI Application.
getApplicationDev :: IO (Settings, Application)
getApplicationDev = do
    (wsettings, _, app) <- getApplicationHelper
    return (wsettings, app)

getAppSettings :: IO AppSettings
getAppSettings = loadAppSettings [configSettingsYml] [] useEnv

-- | main function for use by yesod devel
develMain :: IO ()
develMain = develMainHelper getApplicationDev

-- | The @main@ function for an executable running this site.
appMain :: IO ()
appMain = do
    -- Get the settings from all relevant sources
    settings <- loadAppSettingsArgs
        -- fall back to compile-time values, set to [] to require values at runtime
        [configSettingsYmlValue]

        -- allow environment variables to override
        useEnv

    -- Generate the foundation from the settings
    foundation <- makeFoundation settings

    -- Generate a WAI Application from the foundation
    app <- makeApplication foundation

    -- Run the application with Warp
    let wsettings = warpSettings foundation

    let tls  = (tlsSettings "certificate.pem" "key.pem")
                   { onInsecure = AllowInsecure }
    runTLS tls wsettings app


--------------------------------------------------------------
-- Functions for DevelMain.hs (a way to run the app from GHCi)
--------------------------------------------------------------
getApplicationRepl :: IO (Int, App, Application)
getApplicationRepl = do
    (wsettings, foundation, app) <- getApplicationHelper
    return (getPort wsettings, foundation, app)

shutdownApp :: App -> IO ()
shutdownApp _ = return ()


---------------------------------------------
-- Functions for use in development with GHCi
---------------------------------------------

-- | Run a handler
handler :: Handler a -> IO a
handler h = getAppSettings >>= makeFoundation >>= flip unsafeHandler h

-- | Run DB queries
db :: ReaderT SqlBackend (HandlerT App IO) a -> IO a
db = handler . runDB
