module FactoryAPI (FactoryAPI, RegisterRequest(..)) where

import           Data.Aeson.TH   (defaultOptions, deriveJSON)
import           Servant.API

import           CommonTypes     (FactoryKey)
import           RetrieveJobs    (RetrieveJobsRequest, RetrieveJobsResponse)
import           UploadJobResult (UploadJobResult, UploadJobResultResponse)

-- | A request for registration of a factory. A factory does not need to
-- supply any information when registering.
data RegisterRequest = RegisterRequest {}
$(deriveJSON defaultOptions ''RegisterRequest)

-- The API via which factories talk to the test server
type FactoryAPI =
       "register" :> ReqBody '[JSON] RegisterRequest
                  :> Post    '[JSON] FactoryKey
  :<|> "retrievejobs" :> ReqBody '[JSON] RetrieveJobsRequest
                      :> Post    '[JSON] RetrieveJobsResponse
  :<|> "uploadjobresult" :> ReqBody '[JSON] UploadJobResult
                         :> Post    '[JSON] UploadJobResultResponse
