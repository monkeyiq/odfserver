module SofficeService (createSofficeServices) where

import           Control.Exception     (tryJust)
import           Control.Monad         (guard)
import qualified Data.ByteString.Lazy  as L
import           Data.Map              (Map, fromList, member, (!))
import qualified Data.Text             as T
import           Data.Text.IO          (writeFile)
import           Prelude               hiding (round, writeFile)
import           System.Directory
import           System.Exit           (ExitCode (..))
import           System.FilePath       ((</>))
import           System.Info           (arch, os)
import           System.IO.Error       (isDoesNotExistError)
import           System.IO.Temp
import           System.Process

import           FileType
import qualified Service               as S
import qualified ServiceImplementation as SI
import           Software

data SofficeService = SofficeService {
    sofficePath     :: FilePath,
    serviceInstance :: S.ServiceInstance
}

instance SI.ServiceImplementation SofficeService where
    getServiceInstance = serviceInstance
    runService = runSofficeService

xcu :: T.Text -> T.Text
xcu version = T.concat ["<?xml version='1.0' encoding='UTF-8'?>\n",
    "<oor:items xmlns:oor='http://openoffice.org/2001/registry''>\n",
    "  <item oor:path='/org.openoffice.Setup/Office'>\n",
    "    <prop oor:name='ooSetupInstCompleted' oor:op='fuse'>\n",
    "      <value>true</value>\n",
    "    </prop>\n",
    "  </item>\n",
    "  <item oor:path='/org.openoffice.Office.Common/Save/ODF'>\n",
    "    <prop oor:name='DefaultVersion' oor:op='fuse'>\n",
    "      <value>", version, "</value>\n",
    "    </prop>\n",
    "  </item>\n",
    "</oor:items>"]

-- | LibreOffice can be told to save a specific version by writing the
-- default version into a configuration file.
writeConfiguration :: FilePath -> FileType -> IO ()
writeConfiguration configDir fileType = do
    createDirectory configDir
    createDirectory $ configDir </> "user"
    let odfVersion = getOdfVersion fileType
        version
          | odfVersion == Just V1_1    = "2"    -- ODF 1.1 (UI says 1.0/1.1,
                                      -- but it saves 1.1 features, so it's 1.1)
          | odfVersion == Just V1_2EXT = "3" -- ODF 1.2 extended
          | odfVersion == Just V1_2    = "4" -- ODF 1.2
          | fileType == PDF = "4" -- does not matter
          | otherwise = error $ "Wrong output file type: " ++ show fileType
        content = xcu version
    writeFile (configDir </> "user" </> "registrymodifications.xcu") content

odf1_0, odf1_1, odf1_2, odf1_2ext, odfAll :: [(FileType, String)]
odf1_0 = [(ODT1_0, "odt"), (ODS1_0, "ods"), (ODP1_0, "odp"), (ODG1_0, "odg")]
odf1_1 = [(ODT1_1, "odt"), (ODS1_1, "ods"), (ODP1_1, "odp"), (ODG1_1, "odg")]
odf1_2 = [(ODT1_2, "odt"), (ODS1_2, "ods"), (ODP1_2, "odp"), (ODG1_2, "odg")]
odf1_2ext = [(ODT1_2EXT, "odt"), (ODS1_2EXT, "ods"), (ODP1_2EXT, "odp"),
             (ODG1_2EXT, "odg")]

odfAll = odf1_0 ++ odf1_1 ++ odf1_2 ++ odf1_2ext

filters :: Map String String
filters = fromList [("odt", "writer8"), ("odg", "draw8"), ("ods", "calc8"),
                    ("odp", "impress8"), ("pdf", "writer_pdf_Export")]

extensions :: Map FileType String
extensions = fromList $ odfAll ++ [ (PDF, "pdf"), (PNG, "png") ]

getExtension :: FileType -> String
getExtension t | member t extensions = extensions ! t
getExtension _ = "" -- this should never happen

getFilter :: FileType -> String
getFilter t | member ext filters = ext ++ ":" ++ filters ! ext
    where ext = getExtension t
getFilter _ = ""

runSofficeService :: SofficeService -> L.ByteString -> IO SI.ServiceResult
runSofficeService soffice input = withSystemTempDirectory "sotmp" $ \dir -> do
    let outDir = dir </> "out"
    createDirectory outDir
    let exe = sofficePath soffice
        service = S.service $ serviceInstance soffice
        inext = getExtension $ S.inputType service
        outext = getExtension $ S.outputType service
        filter' = getFilter $ S.outputType service
        inpath = dir </> "file." ++ inext
        outpath = outDir </> "file." ++ outext
        configDir = dir </> "config"
    L.writeFile inpath input
    writeConfiguration configDir $ S.outputType service
    putStrLn $ exe ++ " --headless -env:UserInstallation=file://" ++ configDir
        ++ " --convert-to " ++ filter' ++ " --outdir " ++ outDir ++ " " ++ inpath
    (exitCode, stdout, stderr) <- readProcessWithExitCode exe
        ["--headless", "-env:UserInstallation=file://" ++ configDir,
         "--convert-to", filter', "--outdir", outDir, inpath] ""
    eitherOutput <- tryJust (guard . isDoesNotExistError) (L.readFile outpath)
    let maybeOutput = case eitherOutput of
                      Left _ -> Nothing
                      Right o -> Just o
    let code = if exitCode == ExitSuccess then 0 else 1
    return $ SI.ServiceResult maybeOutput code (T.pack stdout) (T.pack stderr)

getSoftware :: FilePath -> IO Software
getSoftware path = do
    (_, stdout, _) <- readProcessWithExitCode path ["--version"] ""
    -- split stdout on spaces, expected string is e.g.
    -- "LibreOfficeDev 5.0.1.2.0 00m0(Build:2)"
    case map T.pack $ words stdout of
        (program:version':_) -> return Software {
                    softwareFamily  = "LibreOffice",
                    softwareName    = program,
                    softwareVersion = version',
                    osName          = T.pack os,
                    osVersion       = "",
                    platform        = T.pack arch }
        _ -> return $ Software "Unknown" "Unknown" "Unknown" "Unknown" "Unknown" "Unknown"

isSupportOutput :: FileType -> Bool
isSupportOutput fileType = getOdfVersion fileType `elem` [Just V1_1, Just V1_2, Just V1_2EXT]

createPdfServiceInstances :: Software -> [S.ServiceInstance]
createPdfServiceInstances soft = map ins odfAll
    where ins i = S.ServiceInstance soft $ serv $ fst i
          serv i = S.Service "convert" i PDF

createRoundtripInstances :: Software -> [S.ServiceInstance]
createRoundtripInstances soft = map ins $ filter isSupportOutput $ map fst odfAll
    where ins i = S.ServiceInstance soft $ serv i
          serv i = S.Service "roundtrip" i i

createConvertInstances :: Software -> [S.ServiceInstance]
createConvertInstances soft = map ins combinations
    where
        ins (a, b) = S.ServiceInstance soft $ S.Service "convert" a b
        types = [odtTypes, odsTypes, odpTypes, odgTypes]
        mix l = [(a, b) | a <- l, b <- l, a /= b && isSupportOutput b]
        combinations = concatMap mix types

createSofficeServices :: IO [SofficeService]
createSofficeServices = do
    maybePath <- findExecutable "soffice"
    case maybePath of
        Nothing -> return []
        Just path -> do
            sware <- getSoftware path
            let pdf = createPdfServiceInstances sware
                round = createRoundtripInstances sware
                convert = createConvertInstances sware
            return $ map (SofficeService path) (pdf ++ round ++ convert)
