module TestResultTableWidget (
    getTestResultTableWidget,
    getValidation
) where

import           Data.List          (nub)
import           Data.Maybe         (fromJust)
import           Database.Esqueleto
import           Import             hiding (Value, groupBy, isNothing, on,
                                     (==.))

import           FileType

-- | get validation messages for each blob
-- If a no errors were found, the blob is associated with [].
-- If no validation was run, the blob is not present in the returned list.
getValidation :: MonadIO m => [BlobId] -> SqlPersistT m [(BlobId,[Validation])]
getValidation blobs = do
    -- get the list of all jobs for which a validation job ran successfully
    (rows :: [(Value BlobId, Maybe (Entity Validation))]) <- select $ from $ \(job `InnerJoin` service `InnerJoin` result
                             `LeftOuterJoin` validation) -> do
        on $ just (job ^. JobId) ==. validation ?. ValidationJob
        on $ job ^. JobId ==. result ^. JobResultJob
        on $ job ^. JobService ==. service ^. ServiceId
        where_ $ job ^. JobInput `in_` valList blobs
        where_ $ service ^. ServiceOutputType ==. val VALIDATIONREPORT
        where_ $ not_ (isNothing (result ^. JobResultResult))
        return (job ^. JobInput, validation)
    let rows' = map (unValue *** (<$>) entityVal) rows
    return $ mapMaybe (findValidation rows') blobs
  where
    cmpKey b (a,_) = b == a
    findValidation rows blobid =
        case find (cmpKey blobid) rows of
            Nothing -> Nothing
            Just _ -> Just (blobid, mapMaybe snd $ filter (cmpKey blobid) rows)

getOutputs :: MonadIO m => TestId -> SqlPersistT m [(Entity Software,Entity SoftwareVersion,Entity OperatingSystem,Entity OSVersion,Entity Service,Value (Maybe BlobId),Value (Maybe BlobId))]
getOutputs testid =
    select $ from $ \(autotest `InnerJoin` result `InnerJoin` aresult
                     `InnerJoin` jobresult `InnerJoin` job
                     `InnerJoin` instance' `InnerJoin` service
                     `InnerJoin` sos `InnerJoin` version `InnerJoin` software
                     `InnerJoin` family `InnerJoin` platform
                     `InnerJoin` osversion `InnerJoin` os
                     `LeftOuterJoin` pdfjob `InnerJoin` pdf) -> do
        on $ pdfjob ?. JobId ==. pdf ?. JobResultJob
        on $ just (autotest ^. AutoTestInputFile) ==. pdfjob ?. JobInput
             &&. just (result ^. TestResultSoftware) ==. pdfjob ?. JobSoftware
        on $ osversion ^. OSVersionOs ==. os ^. OperatingSystemId
        on $ sos ^. SoftwareOSOs ==. osversion ^. OSVersionId
        on $ sos ^. SoftwareOSPlatform ==. platform ^. PlatformId
        on $ software ^. SoftwareSoftwareFamily ==. family ^. SoftwareFamilyId
        on $ version ^. SoftwareVersionSoftware ==. software ^. SoftwareId
        on $ sos ^. SoftwareOSSoftware ==. version ^. SoftwareVersionId
        on $ instance' ^. ServiceInstanceSoftware ==. sos ^. SoftwareOSId
        on $ instance' ^. ServiceInstanceService  ==. service ^. ServiceId
        on $ job ^. JobServiceInstance ==. just (instance' ^. ServiceInstanceId)
        on $ jobresult ^. JobResultJob ==. job ^. JobId
        on $ aresult ^. AutoTestResultJob ==. jobresult ^. JobResultId
        on $ result ^. TestResultId ==. aresult ^. AutoTestResultResult
        on $ autotest ^. AutoTestTest ==. result ^. TestResultTest
        where_ $ autotest ^. AutoTestTest ==. val testid
        groupBy (software ^. SoftwareId, version ^. SoftwareVersionId,
                 os ^. OperatingSystemId, osversion ^. OSVersionId,
                 service ^. ServiceId)
        orderBy [asc (software ^. SoftwareName),
                 asc (version ^. SoftwareVersionName),
                 asc (os ^. OperatingSystemName),
                 asc (osversion ^. OSVersionName),
                 asc (service ^. ServiceId)]
        return (software, version, os, osversion, service,
                joinV $ min_ (jobresult ^. JobResultResult),
                joinV $ min_ (joinV (pdf ?. JobResultResult)))

getXPathResults :: MonadIO m => TestId -> [BlobId] -> SqlPersistT m [(BlobId,AutoTestXPath,Bool)]
getXPathResults testid blobs = do
    rows <- select $ from $ \(result `InnerJoin` xpath `InnerJoin` use
                             `InnerJoin` autotest) -> do
        on $ use ^. AutoTestXPathUseTest ==. autotest ^. AutoTestId
        on $ result ^. XPathResultXpath ==. use ^. AutoTestXPathUseXpath
        on $ result ^. XPathResultXpath ==. xpath ^. AutoTestXPathId
        where_ $ result ^. XPathResultBlob `in_` valList blobs
        where_ $ autotest ^. AutoTestTest ==. val testid
        groupBy (result ^. XPathResultBlob, xpath ^. AutoTestXPathId,
                 result ^. XPathResultResult)
        orderBy [asc (result ^. XPathResultBlob)]
        return ( result ^. XPathResultBlob
               , xpath
               , result ^. XPathResultResult)
    return $ map (\(Value a, Entity _ b, Value c) -> (a, b, c)) rows

createOutput :: (Entity Software,Entity SoftwareVersion,Entity OperatingSystem,Entity OSVersion,Entity Service,Value (Maybe BlobId),Value (Maybe BlobId)) -> Output
createOutput (Entity _ software, Entity _ version, Entity _ os, Entity _ osv, Entity _ service, modf, mpdf) = Output {
    outputName = unwords [softwareName software,
                          softwareVersionName version,
                          operatingSystemName os,
                          oSVersionName osv],
    outputErrors = Nothing,
    outputType = serviceOutputType service,
    outputFile = unValue modf,
    outputPdf = unValue mpdf
}

data Input = Input {
    inputErrors :: Maybe [Validation],
    inputType   :: FileType,
    inputFile   :: BlobId
}

data Output = Output {
    outputName   :: Text,
    outputErrors :: Maybe [Validation],
    outputPdf    :: Maybe BlobId,
    outputFile   :: Maybe BlobId,
    outputType   :: FileType
}

data XPathGroup = XPathGroup {
    xpathPath   :: Text,
    groupXPaths :: [AutoTestXPath]
}

group' :: [(a, AutoTestXPath, c)] -> [XPathGroup]
group' l = map (keyList x) keys'
  where
    x = map snd' l
    keys' = nub $ map autoTestXPathPath x
    keyList k a = XPathGroup a $ nub $ filter ((a ==) . autoTestXPathPath) k
    snd' (_,b,_) = b

validationWidget :: Maybe [Validation] -> Widget
validationWidget merrors = do
    let haserrors = isJust merrors && merrors /= Just []
    $(widgetFile "validationwidget")

xpathCell :: [(BlobId,AutoTestXPath,Bool)] -> BlobId -> AutoTestXPath -> Bool
xpathCell xpaths blobid xpath = all (\(_,_,b) -> b)
    $ filter (\(a,b,_) -> a == blobid && b == xpath) xpaths

getTestResultTableWidget :: TestId -> Handler Widget
getTestResultTableWidget testid = do
    (input, software, xpaths) <- runDB $ do
        mautoTest <- getBy $ UniqueTest testid
        let inputBlobId = autoTestInputFile $ entityVal $ fromJust mautoTest
        inputBlob <- get404 inputBlobId
        rows <- getOutputs testid
        let software = map createOutput rows
            blobids = inputBlobId : mapMaybe outputFile software
        v <- getValidation blobids
        xpaths <- getXPathResults testid blobids
        let input = Input Nothing (blobFileType inputBlob) inputBlobId
        return (input {inputErrors = lookup inputBlobId v},
                map (addValidation v) software, xpaths)
    let xpathGroups = group' xpaths
    return $(widgetFile "testresulttable")
  where
    addValidation v s =
        case outputFile s of
            Nothing -> s
            Just f -> s { outputErrors = lookup f v }

