module DbWrap (DbWrap(..), createDbWrap, runDbRead2, runDbWrite, runDbRead) where

import           ClassyPrelude.Yesod
import qualified Control.Concurrent.ReadWriteLock as RWL
import           Control.Monad.IO.Class           (MonadIO)
import           Data.Pool                        (Pool)
import           Database.Persist.Sql             (runSqlPool)

data DbWrap = DbWrap {
    pool :: Pool SqlBackend,
    lock :: RWL.RWLock
}

createDbWrap :: Pool SqlBackend -> IO DbWrap
createDbWrap p = do
    lock' <- RWL.new
    return $ DbWrap p lock'

runDbWrite :: SqlPersistT IO a -> DbWrap -> IO a
runDbWrite action wrap = RWL.withWrite (lock wrap) (runDb action wrap)

runDbRead :: SqlPersistT IO a -> DbWrap -> IO a
runDbRead action wrap = RWL.withRead (lock wrap) (runDb action wrap)

runDb :: SqlPersistT IO a -> DbWrap -> IO a
runDb action wrap = runSqlPool action (pool wrap)

runDbRead2 :: (Control.Monad.IO.Class.MonadIO m, MonadBaseControl IO m) =>
            SqlPersistT m b -> DbWrap -> m b
runDbRead2 action wrap = do
    -- todo: use bracket
    liftIO $ RWL.acquireRead (lock wrap)
    r <- runSqlPool action (pool wrap)
    liftIO $ RWL.releaseRead (lock wrap)
    return r
