{-# OPTIONS_GHC -fno-warn-missing-signatures #-}

import           Control.Concurrent         (threadDelay)
import           Control.Monad              (when)
import           Control.Monad.Trans.Either (runEitherT)
import           Data.List                  (find)
import           Data.Text                  (pack)
import           Data.Time                  (diffUTCTime)
import           Data.Time.Clock            (getCurrentTime)
import           Prelude
import           Servant                    (Proxy (..))
import           Servant.API
import           Servant.Client
import           System.Console.GetOpt
import           System.Environment

import           CommonTypes                (FactoryKey (..))
import           Database                   (Database, createDatabase,
                                             getFactoryKey, setFactoryKey)
import           FactoryAPI
import qualified RetrieveJobs               as RJ
import           Service                    (ServiceInstance)
import qualified ServiceImplementation      as S
import           SofficeService             (createSofficeServices)
import           SqlitePool
import           UploadJobResult            (UploadJobResult (..))

type ClientAPI = "factory" :> FactoryAPI
clientAPI :: Proxy ClientAPI
clientAPI = Proxy
api = client clientAPI

registerFactory :: Database -> BaseUrl -> IO (Either String FactoryKey)
registerFactory db baseurl = do
    let register :<|> _ :<|> _ = api baseurl
    res <- runEitherT $ register RegisterRequest
    case res of
        Left err -> return $ Left $ show err
        Right key -> do
            fkey <- setFactoryKey db key
            return $ Right fkey

findService :: S.ServiceImplementation a => [a] -> ServiceInstance -> Maybe a
findService services si = find (\s -> S.getServiceInstance s == si) services

runJob :: S.ServiceImplementation a => FactoryKey -> [a] -> RJ.Job -> IO UploadJobResult
runJob key services job = do
    let maybeService = findService services $ RJ.service job
        jobid = RJ.number job
    startTime <- getCurrentTime
    case maybeService of
        Nothing -> return UploadJobResult {
                factory    = key,
                number     = jobid,
                started    = startTime,
                finished   = startTime,
                outputFile = Nothing,
                exitCode   = -1,
                stdout     = "",
                stderr     = "No service matches."
            }
        Just service -> do
            sr <- S.runService service (RJ.inputFile job)
            endTime <- getCurrentTime
            return UploadJobResult {
                factory    = key,
                number     = jobid,
                started    = startTime,
                finished   = endTime,
                outputFile = S.outputFile sr,
                exitCode   = S.exitCode sr,
                stdout     = S.stdout sr,
                stderr     = S.stderr sr
            }

-- keep listening for jobs until an exception occurs
listenForJobs :: S.ServiceImplementation a => [a] -> FactoryKey -> Database -> BaseUrl -> IO ()
listenForJobs services key db baseurl = do
    let ins = map S.getServiceInstance services
    -- request jobs and keep connection open for 29 seconds (a bit less
    -- than 30 second http timeout for servant)
    startTime <- getCurrentTime
    putStrLn $ "listening " ++ show startTime
    res <- runEitherT $ retrievejobs $ RJ.RetrieveJobsRequest key 29000000 0 ins
    endTime <- getCurrentTime
    case res of
        Left msg -> do
            -- error connecting to server, if error took <5s wait for 5 seconds
            print msg
            when (diffUTCTime endTime startTime < 5) $ do
                putStrLn "sleeping"
                threadDelay 5000000
        Right (RJ.RetrieveJobsResponse jobs) -> do
            putStrLn $ "Received " ++ show (length jobs) ++ " jobs"
            results <- mapM (runJob key services) jobs
            mapM_ uploadJobResult results
            return ()
    listenForJobs services key db baseurl
  where
    _ :<|> retrievejobs :<|> uploadjobresult = api baseurl
    uploadJobResult result = runEitherT $ uploadjobresult result

runFactory :: BaseUrl -> String -> IO ()
runFactory baseurl databasePath = do
    services <- createSofficeServices
    pool <- createSqlitePool $ pack databasePath
    db <- createDatabase pool
    keyOrNothing <- getFactoryKey db
    keyOrNothing2 <- case keyOrNothing of
        Nothing -> registerFactory db baseurl
        Just key -> return $ Right key
    case keyOrNothing2 of
        Left err -> do
            putStrLn $ "Cannot register factory:" ++ err
            return ()
        Right key -> do
            putStrLn $ "This factory is registered as " ++ show (factoryId key)
            listenForJobs services key db baseurl

data Options = Options
    { optUrl      :: BaseUrl
    , optDatabase :: String
    } deriving Show

defaultOptions = Options
    { optUrl      = BaseUrl Http "localhost" 3443
    , optDatabase = "factory.db"
    }

getUrl :: String -> BaseUrl
getUrl u = case parseBaseUrl u of
              Right baseurl -> baseurl
              Left err -> error err

options :: [OptDescr (Options -> Options)]
options =
    [ Option ['u'] ["url"]
        (ReqArg (\u opts -> opts { optUrl = getUrl u }) "URL")
        "host to get jobs from"
    , Option ['d'] ["database"]
        (ReqArg (\d opts -> opts { optDatabase = d }) "FILE")
        "file for storing local data"
    ]

factoryOpts :: [String] -> IO (Options, [String])
factoryOpts argv = do
    progName <- getProgName
    let header = "Usage: " ++ progName ++ " [OPTION...]"
    case getOpt Permute options argv of
        (o,n,[]  ) -> return (foldl (flip id) defaultOptions o, n)
        (_,_,errs) -> ioError (userError (concat errs ++ usageInfo header options))

main :: IO ()
main = do
    args <- getArgs
    (opts, _) <- factoryOpts args
    runFactory (optUrl opts) (optDatabase opts)
