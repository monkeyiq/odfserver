module TestsWidget (
    getTestsWidget
) where

import           Data.List          (nubBy)
import           Database.Esqueleto
import           Import             hiding (Asc, Desc, Value, count, filter,
                                     formatTime, groupBy, isNothing, on, (==.),
                                     (||.))
import qualified Import             as I (on)

import           TableColumns
import           TableWidgetUtils
import           TimeUtil

-- keep only the first sort instruction per column
cleanOrder :: [TestsWidgetSort] -> [TestsWidgetSort]
cleanOrder = nubBy ((==) `I.on` testSortCol)

getTests :: MonadIO m => TestsView -> SqlPersistT m [(Entity Test, Entity User, Value (Maybe Int), Value Int, Value (Maybe Double))]
getTests jv =
    select $ from $ \(test `InnerJoin` user `LeftOuterJoin` result) -> do
        on $ just (test ^. TestId) ==. result ?. TestResultTest
        on $ test ^. TestAuthor ==. user ^. UserId
        mapM_ (addFilter test user) $ testsViewFilter jv
        let (resultCount :: SqlExpr (Value Int))
                = count $ result ?. TestResultTest
            (pass :: SqlExpr (Value Int))
                = case_ [ when_ ((result ?. TestResultPass) ==. just (val True))
                                 then_ $ val 1]
                               (else_ $ val 0)
            (passCount :: SqlExpr (Value (Maybe Int)))
                = sum_ pass
            (frac :: SqlExpr (Value (Maybe Double))) = avg_ pass
        orderBy $ map (toOrder test user passCount resultCount frac) $ testsViewSort jv
        offset $ fromIntegral $ testsViewOffset jv
        limit $ fromIntegral $ testsViewLimit jv
        groupBy (test ^. TestId, user ^. UserId)
        return (test, user, passCount, resultCount, frac)
  where
    toOrder test _ _ _ _ (TestsWidgetSort TestsWidgetName a) = s a $ test ^. TestName
    toOrder test _ _ _ _ (TestsWidgetSort TestsWidgetCreated a) = s a $ test ^. TestCreated
    toOrder _ _ p _ _ (TestsWidgetSort TestsWidgetPassCount a) = s a p
    toOrder _ _ _ r _ (TestsWidgetSort TestsWidgetResultCount a) = s a r
    toOrder _ _ _ _ f (TestsWidgetSort TestsWidgetPassFraction a) = s a f
    toOrder _ user _ _ _ (TestsWidgetSort TestsWidgetUser a) = s a $ user ^. UserName
    s a f = case a of
                SortAsc -> asc f
                SortDesc -> desc f
    addFilter _ user (TestsWidgetFilterUser userid) =
        where_ $ user ^. UserId ==. val userid

getTestsWidget :: Bool -> TestsView -> Handler Widget
getTestsWidget navLinks testsview = getTestsWidget' navLinks testsview'
  where
    order = cleanOrder $ testsViewSort testsview
                         ++ [TestsWidgetSort TestsWidgetCreated SortDesc]
    testsview' = testsview {testsViewSort = order}

getTestsWidget' :: Bool -> TestsView -> Handler Widget
getTestsWidget' navLinks testsview = do
    let showUser = TestsWidgetUser `elem` testsViewCols testsview
    rows' <- runDB $ getTests testsview
    let rows = zip [start + 1 ..] rows'
        mPrevPage = if start > 0
                    then Just $ changeOffset (max 0 $ start - size)
                    else Nothing
        mNextPage = if size == length rows
                    then Just $ changeOffset (start + size)
                    else Nothing
    return $(widgetFile "testswidget")
  where
    start = testsViewOffset testsview
    size =testsViewLimit testsview
    changeOffset o = TestsR $ testsview { testsViewOffset = o }
    condensed = not navLinks
    order = testsViewSort testsview
    sortButton c = makeSortWidget (tor c SortAsc) (tor c SortDesc)
    tor c d = if o == order then Nothing else Just $ TestsR $ testsview {testsViewSort = o}
      where o = cleanOrder (TestsWidgetSort c d : order)

