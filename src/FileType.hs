module FileType (
    FileType(..),
    OdfVersion(..),
    allOdfTypes,
    getFileType,
    getOdfVersion,
    odtTypes,
    odsTypes,
    odpTypes,
    odgTypes,
    showExt,
    showNice,
    showOdfVersion
) where

import           ClassyPrelude.Yesod
import           Data.Aeson.TH       (defaultOptions, deriveJSON)
import           Data.Binary
import           Data.List           (elemIndex, (!!))

data OdfVersion = V1_0 | V1_1 | V1_2 | V1_2EXT deriving (Show, Eq)

data FileType = Other
      | ODT1_0 | ODT1_1 | ODT1_2 | ODT1_2EXT
      | OTT1_0 | OTT1_1 | OTT1_2 | OTT1_2EXT
      | ODG1_0 | ODG1_1 | ODG1_2 | ODG1_2EXT
      | OTG1_0 | OTG1_1 | OTG1_2 | OTG1_2EXT
      | ODP1_0 | ODP1_1 | ODP1_2 | ODP1_2EXT
      | OTP1_0 | OTP1_1 | OTP1_2 | OTP1_2EXT
      | ODS1_0 | ODS1_1 | ODS1_2 | ODS1_2EXT
      | OTS1_0 | OTS1_1 | OTS1_2 | OTS1_2EXT
      | ODC1_0 | ODC1_1 | ODC1_2 | ODC1_2EXT
      | OTC1_0 | OTC1_1 | OTC1_2 | OTC1_2EXT
      | ODI1_0 | ODI1_1 | ODI1_2 | ODI1_2EXT
      | OTI1_0 | OTI1_1 | OTI1_2 | OTI1_2EXT
      | ODF1_0 | ODF1_1 | ODF1_2 | ODF1_2EXT
      | OTF1_0 | OTF1_1 | OTF1_2 | OTF1_2EXT
      | ODM1_0 | ODM1_1 | ODM1_2 | ODM1_2EXT
      | OTH1_0 | OTH1_1 | OTH1_2 | OTH1_2EXT
      | ODB1_2 | ODB1_2EXT
      | ODT1_0XML | ODT1_1XML | ODT1_2XML | ODT1_2EXTXML
      | OTT1_0XML | OTT1_1XML | OTT1_2XML | OTT1_2EXTXML
      | ODG1_0XML | ODG1_1XML | ODG1_2XML | ODG1_2EXTXML
      | OTG1_0XML | OTG1_1XML | OTG1_2XML | OTG1_2EXTXML
      | ODP1_0XML | ODP1_1XML | ODP1_2XML | ODP1_2EXTXML
      | OTP1_0XML | OTP1_1XML | OTP1_2XML | OTP1_2EXTXML
      | ODS1_0XML | ODS1_1XML | ODS1_2XML | ODS1_2EXTXML
      | OTS1_0XML | OTS1_1XML | OTS1_2XML | OTS1_2EXTXML
      | ODC1_0XML | ODC1_1XML | ODC1_2XML | ODC1_2EXTXML
      | OTC1_0XML | OTC1_1XML | OTC1_2XML | OTC1_2EXTXML
      | ODI1_0XML | ODI1_1XML | ODI1_2XML | ODI1_2EXTXML
      | OTI1_0XML | OTI1_1XML | OTI1_2XML | OTI1_2EXTXML
      | ODF1_0XML | ODF1_1XML | ODF1_2XML | ODF1_2EXTXML
      | OTF1_0XML | OTF1_1XML | OTF1_2XML | OTF1_2EXTXML
      | ODM1_0XML | ODM1_1XML | ODM1_2XML | ODM1_2EXTXML
      | OTH1_0XML | OTH1_1XML | OTH1_2XML | OTH1_2EXTXML
      | ODB1_2XML | ODB1_2EXTXML
      | XML
      | PDF
      | PNG
      | PNGS
      | VALIDATIONREPORT -- file format that reports on file validation
      deriving (Show, Read, Eq, Ord, Enum, Bounded, Generic)
$(deriveJSON defaultOptions ''FileType)
 -- filetype ERRORS is a validation report in xml form
instance Binary FileType

-- allow use of type in Persistent
derivePersistField "FileType"

odtMimeType :: Text
odtMimeType = "application/vnd.oasis.opendocument.text"
ottMimeType :: Text
ottMimeType = "application/vnd.oasis.opendocument.text-template"
odgMimeType :: Text
odgMimeType = "application/vnd.oasis.opendocument.graphics"
otgMimeType :: Text
otgMimeType = "application/vnd.oasis.opendocument.graphics-template"
odpMimeType :: Text
odpMimeType = "application/vnd.oasis.opendocument.presentation"
otpMimeType :: Text
otpMimeType = "application/vnd.oasis.opendocument.presentation-template"
odsMimeType :: Text
odsMimeType = "application/vnd.oasis.opendocument.spreadsheet"
otsMimeType :: Text
otsMimeType = "application/vnd.oasis.opendocument.spreadsheet-template"
odcMimeType :: Text
odcMimeType = "application/vnd.oasis.opendocument.chart"
otcMimeType :: Text
otcMimeType = "application/vnd.oasis.opendocument.chart-template"
odiMimeType :: Text
odiMimeType = "application/vnd.oasis.opendocument.image"
otiMimeType :: Text
otiMimeType = "application/vnd.oasis.opendocument.image-template"
odfMimeType :: Text
odfMimeType = "application/vnd.oasis.opendocument.formula"
otfMimeType :: Text
otfMimeType = "application/vnd.oasis.opendocument.formula-template"
odmMimeType :: Text
odmMimeType = "application/vnd.oasis.opendocument.text-master"
othMimeType :: Text
othMimeType = "application/vnd.oasis.opendocument.text-web"
odbMimeType :: Text
odbMimeType = "application/vnd.oasis.opendocument.base"
pdfMimeType :: Text
pdfMimeType = "application/pdf"
pngMimeType :: Text
pngMimeType = "application/png"

getFileType :: Text -> Maybe Text -> Bool -> Bool -> FileType
getFileType mimetype maybeVersion xml extended = getFileType2 mimetype version xml
  where
    version = if extended
                  then V1_2EXT
                  else case maybeVersion of
                      Just "1.0" -> V1_0
                      Just "1.1" -> V1_1
                      Just "1.2" -> V1_2
                      _ -> V1_1 -- In ODF 1.2, the version must be set
                      -- explicitly, so the file is 1.0 or 1.1. 1.1 has
                      -- more features, e.g. <draw:custom-shape/>, so it is
                      -- safer to assume 1.1.
                      --
                      --

showExt :: FileType -> Text
showExt t | t `elem` (odtTypes ++ odtXmlTypes) = "odt"
showExt t | t `elem` (ottTypes ++ ottXmlTypes) = "ott"
showExt t | t `elem` (odgTypes ++ odgXmlTypes) = "odg"
showExt t | t `elem` (otgTypes ++ otgXmlTypes) = "otg"
showExt t | t `elem` (odpTypes ++ odpXmlTypes) = "odp"
showExt t | t `elem` (otpTypes ++ otpXmlTypes) = "otp"
showExt t | t `elem` (odsTypes ++ odsXmlTypes) = "ods"
showExt t | t `elem` (otsTypes ++ otsXmlTypes) = "ots"
showExt t | t `elem` (odcTypes ++ odcXmlTypes) = "odc"
showExt t | t `elem` (otcTypes ++ otcXmlTypes) = "otc"
showExt t | t `elem` (odiTypes ++ odiXmlTypes) = "odi"
showExt t | t `elem` (otiTypes ++ otiXmlTypes) = "oti"
showExt t | t `elem` (odsTypes ++ odsXmlTypes) = "ods"
showExt t | t `elem` (otsTypes ++ otsXmlTypes) = "ots"
showExt t | t `elem` (odmTypes ++ odmXmlTypes) = "odm"
showExt t | t `elem` (othTypes ++ othXmlTypes) = "oth"
showExt XML = "xml"
showExt PDF = "pdf"
showExt PNG = "png"
showExt VALIDATIONREPORT = "xml"
showExt _ = ""

showNice :: FileType -> Text
showNice t = showExt t ++ case showOdfVersion t of
                              "" -> ""
                              v -> " " ++ v

showOdfVersion :: FileType -> Text
showOdfVersion t = case getOdfVersion t of
                        Nothing -> ""
                        Just V1_0    -> "1.0"
                        Just V1_1    -> "1.1"
                        Just V1_2    -> "1.2"
                        Just V1_2EXT -> "1.2 Extended"

odtTypes, odtXmlTypes, ottTypes, ottXmlTypes :: [FileType]
odtTypes    = [ODT1_0, ODT1_1, ODT1_2, ODT1_2EXT]
odtXmlTypes = [ODT1_0XML, ODT1_1XML, ODT1_2XML, ODT1_2EXTXML]
ottTypes    = [OTT1_0, OTT1_1, OTT1_2, OTT1_2EXT]
ottXmlTypes = [OTT1_0XML, OTT1_1XML, OTT1_2XML, OTT1_2EXTXML]
odgTypes, odgXmlTypes, otgTypes, otgXmlTypes :: [FileType]
odgTypes    = [ODG1_0, ODG1_1, ODG1_2, ODG1_2EXT]
odgXmlTypes = [ODG1_0XML, ODG1_1XML, ODG1_2XML, ODG1_2EXTXML]
otgTypes    = [OTG1_0, OTG1_1, OTG1_2, OTG1_2EXT]
otgXmlTypes = [OTG1_0XML, OTG1_1XML, OTG1_2XML, OTG1_2EXTXML]
odpTypes, odpXmlTypes, otpTypes, otpXmlTypes :: [FileType]
odpTypes    = [ODP1_0, ODP1_1, ODP1_2, ODP1_2EXT]
odpXmlTypes = [ODP1_0XML, ODP1_1XML, ODP1_2XML, ODP1_2EXTXML]
otpTypes    = [OTP1_0, OTP1_1, OTP1_2, OTP1_2EXT]
otpXmlTypes = [OTP1_0XML, OTP1_1XML, OTP1_2XML, OTP1_2EXTXML]
odsTypes, odsXmlTypes, otsTypes, otsXmlTypes :: [FileType]
odsTypes    = [ODS1_0, ODS1_1, ODS1_2, ODS1_2EXT]
odsXmlTypes = [ODS1_0XML, ODS1_1XML, ODS1_2XML, ODS1_2EXTXML]
otsTypes    = [OTS1_0, OTS1_1, OTS1_2, OTS1_2EXT]
otsXmlTypes = [OTS1_0XML, OTS1_1XML, OTS1_2XML, OTS1_2EXTXML]
odcTypes, odcXmlTypes, otcTypes, otcXmlTypes :: [FileType]
odcTypes    = [ODC1_0, ODC1_1, ODC1_2, ODC1_2EXT]
odcXmlTypes = [ODC1_0XML, ODC1_1XML, ODC1_2XML, ODC1_2EXTXML]
otcTypes    = [OTC1_0, OTC1_1, OTC1_2, OTC1_2EXT]
otcXmlTypes = [OTC1_0XML, OTC1_1XML, OTC1_2XML, OTC1_2EXTXML]
odiTypes, odiXmlTypes, otiTypes, otiXmlTypes :: [FileType]
odiTypes    = [ODI1_0, ODI1_1, ODI1_2, ODI1_2EXT]
odiXmlTypes = [ODI1_0XML, ODI1_1XML, ODI1_2XML, ODI1_2EXTXML]
otiTypes    = [OTI1_0, OTI1_1, OTI1_2, OTI1_2EXT]
otiXmlTypes = [OTI1_0XML, OTI1_1XML, OTI1_2XML, OTI1_2EXTXML]
odfTypes, odfXmlTypes, otfTypes, otfXmlTypes :: [FileType]
odfTypes    = [ODF1_0, ODF1_1, ODF1_2, ODF1_2EXT]
odfXmlTypes = [ODF1_0XML, ODF1_1XML, ODF1_2XML, ODF1_2EXTXML]
otfTypes    = [OTF1_0, OTF1_1, OTF1_2, OTF1_2EXT]
otfXmlTypes = [OTF1_0XML, OTF1_1XML, OTF1_2XML, OTF1_2EXTXML]
odmTypes, odmXmlTypes :: [FileType]
odmTypes    = [ODM1_0, ODM1_1, ODM1_2, ODM1_2EXT]
odmXmlTypes = [ODM1_0XML, ODM1_1XML, ODM1_2XML, ODM1_2EXTXML]
othTypes, othXmlTypes :: [FileType]
othTypes    = [OTH1_0, OTH1_1, OTH1_2, OTH1_2EXT]
othXmlTypes = [OTH1_0XML, OTH1_1XML, OTH1_2XML, OTH1_2EXTXML]

allOdfTypes :: [[FileType]]
allOdfTypes = [ odtTypes, odtXmlTypes, ottTypes, ottXmlTypes
              , odgTypes, odgXmlTypes, otgTypes, otgXmlTypes
              , odpTypes, odpXmlTypes, otpTypes, otpXmlTypes
              , odsTypes, odsXmlTypes, otsTypes, otsXmlTypes
              , odcTypes, odcXmlTypes, otcTypes, otcXmlTypes
              , odiTypes, odiXmlTypes, otiTypes, otiXmlTypes
              , odfTypes, odfXmlTypes, otfTypes, otfXmlTypes
              , odmTypes, odmXmlTypes, othTypes, othXmlTypes ]

getOdfVersion :: FileType -> Maybe OdfVersion
getOdfVersion fileType = case mindex of
                             Just 0 -> Just V1_0
                             Just 1 -> Just V1_1
                             Just 2 -> Just V1_2
                             Just 3 -> Just V1_2EXT
                             _ -> Nothing
  where
    mindex = listToMaybe $ mapMaybe (elemIndex fileType) allOdfTypes

getFileType2 :: Text -> OdfVersion -> Bool -> FileType
getFileType2 mimetype version xml
    | mimetype == odtMimeType = variant odtTypes odtXmlTypes
    | mimetype == ottMimeType = variant ottTypes ottXmlTypes
    | mimetype == odgMimeType = variant odgTypes odgXmlTypes
    | mimetype == otgMimeType = variant otgTypes otgXmlTypes
    | mimetype == odpMimeType = variant odpTypes odpXmlTypes
    | mimetype == otpMimeType = variant otpTypes otpXmlTypes
    | mimetype == odsMimeType = variant odsTypes odsXmlTypes
    | mimetype == otsMimeType = variant otsTypes otsXmlTypes
    | mimetype == odcMimeType = variant odcTypes odcXmlTypes
    | mimetype == otcMimeType = variant otcTypes otcXmlTypes
    | mimetype == odiMimeType = variant odiTypes odiXmlTypes
    | mimetype == otiMimeType = variant otiTypes otiXmlTypes
    | mimetype == odfMimeType = variant odfTypes odfXmlTypes
    | mimetype == otfMimeType = variant otfTypes otfXmlTypes
    | mimetype == odmMimeType = variant odmTypes odmXmlTypes
    | mimetype == othMimeType = variant othTypes othXmlTypes
    | mimetype == odbMimeType =
       if xml then if extended then ODB1_2EXTXML else ODB1_2XML
       else if extended then ODB1_2EXT else ODB1_2
    | mimetype == pdfMimeType = PDF
    | mimetype == pngMimeType = PNG
    | otherwise = Other
    where
        extended = version == V1_2EXT
        variant zs xs = if xml then xs !! n else zs !! n
          where n = case version of
                        V1_0    -> 0
                        V1_1    -> 1
                        V1_2    -> 2
                        V1_2EXT -> 3
