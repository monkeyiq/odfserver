module MimeType (FileInfo(..), getFileInfo) where

import           ClassyPrelude.Yesod  hiding (Element, FileInfo, path)
import           Codec.Archive.Zip
import qualified Data.ByteString.Lazy as L
import qualified Data.Map.Lazy        as M
import           Data.Maybe           (fromJust)
import           Data.Text.Encoding   (decodeUtf8')
import           Text.XML

import           FileType             (FileType (..), getFileType)

data FileInfo = FileInfo {
    mimetype :: Text,
    fileType :: FileType
}

parseSettings :: ParseSettings
parseSettings = def { psRetainNamespaces = True }

slice :: Int64 -> Int64 -> L.ByteString -> L.ByteString
slice start len = L.take len . L.drop start

getOpenPackageMimeType :: L.ByteString -> Maybe Text
getOpenPackageMimeType bytes =
    if name == "mimetype" && L.length mimetype' == clength
    then case decodeUtf8' $ L.toStrict mimetype' of
        Left _ -> Nothing
        Right m -> Just m
    else Nothing
    where
        name = slice 30 8 bytes
        clength = fromIntegral $ toInteger $ L.index bytes 22
        mimetype' = slice 38 clength bytes

isZip :: L.ByteString -> Bool
isZip = L.isPrefixOf zipHeader
    where zipHeader = L.pack [0x50,0x4b,0x03,0x04]

octetStream :: Text
octetStream = "application/octet-stream"

applicationXml :: Text
applicationXml = "application/xml"

officens :: Text
officens = "urn:oasis:names:tc:opendocument:xmlns:office:1.0"

manifestns :: Text
manifestns = "urn:oasis:names:tc:opendocument:xmlns:manifest:1.0"

attr :: M.Map Name Text -> Text -> Text -> Maybe Text
attr atts ns name = M.lookup (Name name (Just ns) Nothing) atts

attribute :: Element -> Text -> Text -> Maybe Text
attribute element = attr (elementAttributes element)

getMimeTypeBin :: L.ByteString -> Maybe Text
getMimeTypeBin bytes | isZip bytes =
    case getOpenPackageMimeType bytes of
        Just mime -> Just mime
        Nothing -> Just "application/zip"
getMimeTypeBin bytes | L.isPrefixOf "%PDF-" bytes = Just "application/pdf"
getMimeTypeBin bytes | L.isPrefixOf pngmagic bytes = Just "image/png"
    where pngmagic = L.pack [0x89,0x50,0x4e,0x47,0x0d,0x0a,0x1a,0x0a]
getMimeTypeBin bytes | L.isPrefixOf jpgmagic bytes = Just "image/jpeg"
    where jpgmagic = L.pack [0xff,0xd8,0xff]
getMimeTypeBin bytes | L.isPrefixOf gifmagic1 bytes
                       || L.isPrefixOf gifmagic2 bytes = Just "image/gif"
    where gifmagic1 = "GIF87a"
          gifmagic2 = "GIF89a"
getMimeTypeBin _ = Nothing

getMimeTypeXml :: Element -> Text
getMimeTypeXml (Element (Name name ns _) atts _)
    | ns == Just officens && name == "document" && isJust mimetype' =
      fromJust mimetype'
    | ns == Just svgns && name == "svg" = "image/svg+xml"
    | ns == Just rdfns && name == "RDF" = "application/rdf+xml"
    | otherwise = applicationXml
    where mimetype' = attr atts officens "mimetype"
          svgns = "http://www.w3.org/2000/svg"
          rdfns = "http://www.w3.org/1999/02/22-rdf-syntax-ns#"

-- | Return the version from the 'version' attribute for the given
-- namespace.
-- If the attribute is not present, this function returns Nothing.
getOdfVersionXml :: Text -> L.ByteString -> Maybe Text
getOdfVersionXml ns bytes = case parseLBS parseSettings bytes of
    Right (Document _ root _) -> attribute root ns "version"
    Left _ -> Nothing

-- | Read the version attribute from the given XML file.
-- If the file is not present, not valid XML or has no 'version'
-- attribute on the document element, Nothing is returned.
getOdfVersion :: Archive -> Text -> FilePath -> Maybe Text
getOdfVersion archive ns path = case findEntryByPath path archive of
    Just entry -> getOdfVersionXml ns $ fromEntry entry
    Nothing -> Nothing

-- | Get the ODF version from the ODF file
-- Look in XML files in the zip container for a 'version' attribute on the
-- document element. If no version can be found, return Nothing.
getZipOdfVersion :: L.ByteString -> Maybe Text
getZipOdfVersion bytes = case toArchiveOrFail bytes of
    -- look for odf version in files from small to large
    Right archive -> listToMaybe $ catMaybes [
        getOdfVersion archive officens "meta.xml",
        getOdfVersion archive officens "settings.xml",
        getOdfVersion archive officens "styles.xml",
        getOdfVersion archive manifestns "META-INF/manifest.xml",
        getOdfVersion archive officens "content.xml"]
    Left _ -> Nothing

cleanOdfVersion :: Maybe Text -> Maybe Text
cleanOdfVersion versionOrNothing = case versionOrNothing of
    Just version -> if version `elem` ["1.0", "1.1", "1.2"]
        then Just version else Nothing
    Nothing -> Nothing

makeFileInfo :: Text -> Maybe Text -> Bool -> Bool -> FileInfo
makeFileInfo mimetype' versionOrMaybe odfxml extended =
    FileInfo mimetype' $ getFileType mimetype' versionOrMaybe odfxml extended

getFileInfo :: L.ByteString -> FileInfo
getFileInfo bytes =
    case getMimeTypeBin bytes of
        Just mimetype' ->
            if "application/vnd.oasis.opendocument." `isPrefixOf` mimetype'
            then makeFileInfo mimetype' version False ext
            else makeFileInfo mimetype' Nothing False False
                where version = cleanOdfVersion $ m $ getZipOdfVersion bytes
        Nothing -> case parseLBS parseSettings bytes of
            Right (Document _ root _) ->
                if "application/vnd.oasis.opendocument." `isPrefixOf` mimetype'
                then makeFileInfo mimetype' (cleanOdfVersion $ m version) True ext
                else makeFileInfo mimetype' Nothing False False
                where mimetype' = getMimeTypeXml root
                      version = attribute root officens "version"
            Left _ -> makeFileInfo octetStream Nothing False False
    where ext = True
          m Nothing = Nothing
          m t = Just $ fromJust t
