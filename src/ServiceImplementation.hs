module ServiceImplementation where

import           Data.ByteString.Lazy (ByteString)
import           Data.Int             (Int)
import           Data.Maybe
import           Data.Text            (Text)
import           System.IO            (IO)

import           Service              (ServiceInstance)

data ServiceResult = ServiceResult {
    outputFile :: Maybe ByteString,
    exitCode   :: Int,
    stdout     :: Text,
    stderr     :: Text
}

class ServiceImplementation a where
    getServiceInstance :: a -> ServiceInstance
    runService :: a -> ByteString -> IO ServiceResult
