module TestScheduler (
    evaluateAutoTests,
    scheduleTestJobs
) where

import           Codec.Archive.Zip
import           Data.List                            (nub)
import           Data.Map                             (fromListWith, toList)
import           Database.Esqueleto
import           Database.Esqueleto.Internal.Language (Insertion)
import           Import                               hiding (Value, groupBy,
                                                       isNothing, on, toList,
                                                       (==.))
import           Text.XML.HXT.Core                    hiding (Blob)

import qualified Database                             as D
import           FileType
import           TestInputCreator
import           XPath                                (parseXPath)

onJob :: SqlExpr (Entity Blob) -> SqlExpr (Entity Service) -> SqlExpr (Entity ServiceInstance) -> SqlExpr (Maybe (Entity Job)) -> SqlQuery ()
onJob blob service instance' job = do
    on (job ?. JobInput ==. just (blob ^. BlobId)
        &&. job ?. JobService ==. just (service ^. ServiceId)
        &&. job ?. JobSoftware ==. just (instance' ^. ServiceInstanceSoftware))
    on (service ^. ServiceId ==. instance' ^. ServiceInstanceService)
    on (blob ^. BlobFileType ==. service ^. ServiceInputType)

onJob' :: SqlExpr (Entity Blob) -> SqlExpr (Entity Service) -> SqlExpr (Entity ServiceInstance) -> SqlExpr (Maybe (Entity Job)) -> SqlExpr (Entity AutoTest) -> SqlExpr (Entity Test) -> SqlQuery ()
onJob' blob service instance' job autotest test = do
        onJob blob service instance' job
        on (autotest ^. AutoTestInputFile ==. blob ^.BlobId)
        on (autotest ^. AutoTestTest ==. test ^. TestId)

makeJob :: UTCTime -> SqlExpr (Value UserId) -> SqlExpr (Entity Blob) -> SqlExpr (Entity Service) -> SqlExpr (Entity ServiceInstance) -> SqlExpr (Maybe (Entity Job)) -> SqlQuery (SqlExpr (Insertion Job))
makeJob now author blob service instance' job = do
    where_ (isNothing $ job ?. JobId)
    groupBy (author, blob ^. BlobId, instance' ^. ServiceInstanceSoftware, service ^. ServiceId)
    return $ Job <#  author
                 <&>  val now
                 <&> (blob ^. BlobId)
                 <&> (instance' ^. ServiceInstanceSoftware)
                 <&> (service ^. ServiceId)
                 <&> val Nothing

-- | Create jobs for all tests that do not yet have jobs
-- A job is created for each available combination of service and
-- software_os
scheduleTests ::MonadIO m => UTCTime -> SqlPersistT m ()
scheduleTests now =
    insertSelect $ from $ \(autotest `InnerJoin` output `InnerJoin` test `InnerJoin` blob `InnerJoin` service `InnerJoin` instance' `LeftOuterJoin` job) -> do
        onJob' blob service instance' job autotest test
        on (autotest ^. AutoTestId ==. output ^. AutoTestOutputTypeTest)
        where_ (service ^. ServiceOutputType ==. output ^. AutoTestOutputTypeFileType)
        makeJob now (test ^. TestAuthor) blob service instance' job

scheduleTestPdfs ::MonadIO m => UTCTime -> SqlPersistT m ()
scheduleTestPdfs now =
    insertSelect $ from $ \(autotest `InnerJoin` test `InnerJoin` blob `InnerJoin` service `InnerJoin` instance' `LeftOuterJoin` job) -> do
        onJob' blob service instance' job autotest test
        where_ (service ^. ServiceOutputType ==. val PDF)
        makeJob now (test ^. TestAuthor) blob service instance' job

-- | Each job result for which there is a validation service is schedule
-- a validation job.
scheduleJobValidation :: MonadIO m => UTCTime -> SqlPersistT m ()
scheduleJobValidation now =
    insertSelect $ from $ \(job' `InnerJoin` jobresult `InnerJoin` blob `InnerJoin` service `InnerJoin` instance' `LeftOuterJoin` job) -> do
        onJob blob service instance' job
        on (jobresult ^. JobResultResult ==. just (blob ^. BlobId))
        on (job' ^. JobId ==. jobresult ^. JobResultJob)
        where_ (service ^. ServiceOutputType ==. val VALIDATIONREPORT)
        makeJob now (job' ^. JobUser) blob service instance' job

scheduleUploadValidation :: MonadIO m => UTCTime -> SqlPersistT m ()
scheduleUploadValidation now =
    insertSelect $ from $ \(upload `InnerJoin` blob `InnerJoin` service `InnerJoin` instance' `LeftOuterJoin` job) -> do
        onJob blob service instance' job
        on (upload ^. UploadContent  ==. blob ^. BlobId)
        where_ (service ^. ServiceOutputType ==. val VALIDATIONREPORT)
        makeJob now (upload ^. UploadUploader) blob service instance' job

scheduleTestValidation :: MonadIO m => UTCTime -> SqlPersistT m ()
scheduleTestValidation now =
    insertSelect $ from $ \(test `InnerJoin` autotest `InnerJoin` blob `InnerJoin` service `InnerJoin` instance' `LeftOuterJoin` job) -> do
        onJob blob service instance' job
        on (autotest ^. AutoTestInputFile  ==. blob ^. BlobId)
        on (test ^. TestId  ==. autotest ^. AutoTestTest)
        where_ (service ^. ServiceOutputType ==. val VALIDATIONREPORT)
        makeJob now (test ^. TestAuthor) blob service instance' job

scheduleTestJobs :: (MonadIO m, MonadBaseControl IO m) => D.Database -> m ()
scheduleTestJobs db = do
    now <- liftIO getCurrentTime
    flip D.runDb db $ do
        scheduleJobValidation now
        scheduleUploadValidation now
        scheduleTestValidation now
        scheduleTests now
        scheduleTestPdfs now
    liftIO $ D.signalNewJob db

getUnevaluatedInputXPaths :: MonadIO m => ReaderT SqlBackend m [(BlobId,Entity AutoTestXPath)]
getUnevaluatedInputXPaths = do
    -- get all the xpaths without result
    rows <- select $ from $ \(autotest `InnerJoin` use `InnerJoin` xpath
                             `LeftOuterJoin` result) -> do
        on (result ?. XPathResultBlob ==. just (autotest ^. AutoTestInputFile)
            &&. result ?. XPathResultXpath ==. just (xpath ^. AutoTestXPathId))
        on (use ^. AutoTestXPathUseXpath ==. xpath ^. AutoTestXPathId)
        on (autotest ^. AutoTestId ==. use ^. AutoTestXPathUseTest)
        where_ (isNothing $ result ?. XPathResultId)
        return (autotest ^. AutoTestInputFile, xpath)
    return $ map (\(Value a, b) -> (a, b)) rows

getUnevaluatedOutputXPaths :: MonadIO m => ReaderT SqlBackend m [(BlobId,Entity AutoTestXPath)]
getUnevaluatedOutputXPaths = do
    -- get all the xpaths without result
    rows <- select $ from $ \(autotest `InnerJoin` job `InnerJoin` jobresult
                             `InnerJoin` use `InnerJoin` xpath
                             `LeftOuterJoin` result) -> do
        on (result ?. XPathResultBlob ==. jobresult ^. JobResultResult
            &&. result ?. XPathResultXpath ==. just (xpath ^. AutoTestXPathId))
        on (use ^. AutoTestXPathUseXpath ==. xpath ^. AutoTestXPathId)
        on (autotest ^. AutoTestId ==. use ^. AutoTestXPathUseTest)
        on (job ^. JobId ==. jobresult ^. JobResultJob)
        on (autotest ^. AutoTestInputFile ==. job ^. JobInput)
        where_ (isNothing $ result ?. XPathResultId)
        where_ (not_ (isNothing $ jobresult ^. JobResultResult))
        return (jobresult ^. JobResultResult, xpath)
    return $ map (\(Value (Just a), b) -> (a, b)) rows

groupBy' :: (Ord b, Eq a) => (a -> b) -> [a] -> [(b,[a])]
groupBy' f l = toList $ map nub $ fromListWith (++) $ map (\a -> (f a, [a])) l

evaluateXPaths' :: MonadIO m => (BlobId, [Entity AutoTestXPath]) -> ReaderT SqlBackend m [XPathResult]
evaluateXPaths' (blobid, xpaths) = do
    mblob <- get blobid
    case mblob of
        Nothing -> return []
        Just blob ->
            case toArchiveOrFail $ fromStrict $ blobContent blob of
                Left _ -> return []
                Right archive -> do
                    r' <- mapM (check' archive) (groupBy' getPath xpaths)
                    return $ concat r'
  where
    getPath (Entity _ (AutoTestXPath path _)) = path
    check' archive (path, xp) = do
        let mentry = findEntryByPath (unpack path) archive
        case mentry of
            Nothing -> return $ map failed xp
            Just entry -> do
                mtree <- liftIO $ parseXML $ blobToString $ fromEntry entry
                case mtree of
                    Left _ -> return $ map failed xp
                    Right tree -> return $ map (check'' tree) xp
    check'' tree (Entity key (AutoTestXPath _ xpath)) =
        case parseXPath $ unpack xpath of
            Left _ -> XPathResult blobid key False
            Right expr -> check''' key $ expr tree
    check''' key (Just "True") = XPathResult blobid key True
    check''' key _ = XPathResult blobid key False
    failed (Entity key _) = XPathResult blobid key False

evaluateXPaths :: MonadIO m => [(BlobId,Entity AutoTestXPath)] -> ReaderT SqlBackend m ()
evaluateXPaths xpaths = do
    r <- mapM evaluateXPaths' grouped
    D.insertMany_ 3 $ concat r
  where
    up (k, v) = (k,[v])
    grouped = toList $ fromListWith (++) $ map up xpaths

evaluateUnevaluatedXPaths :: MonadIO m => ReaderT SqlBackend m ()
evaluateUnevaluatedXPaths = do
    input <- getUnevaluatedInputXPaths
    output <- getUnevaluatedOutputXPaths
    evaluateXPaths (input ++ output)

evaluateAutoTests :: MonadIO m => ReaderT SqlBackend m ()
evaluateAutoTests = do
    evaluateUnevaluatedXPaths
    -- Find out for which autotests, there is no AutoTestResult yet, but
    -- for which all xpaths have been evaluated.
    r <- select $ from $ \(autotest `InnerJoin` use `InnerJoin` job
                          `InnerJoin` jobresult `InnerJoin` xpathresult
                          `InnerJoin` xpath) -> do
        on (use ^. AutoTestXPathUseXpath ==. xpath ^. AutoTestXPathId)
        on (jobresult ^. JobResultResult ==. just (xpathresult ^. XPathResultBlob))
        on (job ^. JobId ==. jobresult ^. JobResultJob)
        on (autotest ^. AutoTestInputFile ==. job ^. JobInput)
        on (autotest ^. AutoTestId ==. use ^. AutoTestXPathUseTest)
        where_ (xpath ^. AutoTestXPathId ==. xpathresult ^. XPathResultXpath)
        where_ (notExists $
                from $ \(testresult `InnerJoin` autoresult) -> do
                on (testresult ^. TestResultId
                        ==. autoresult ^. AutoTestResultResult)
                where_ (autoresult ^. AutoTestResultJob
                       ==. jobresult ^. JobResultId)
                where_ (testresult ^. TestResultTest
                          ==. autotest ^. AutoTestTest))
        let (ok :: SqlExpr (Value Int)) =
                case_ [ when_ (xpathresult ^. XPathResultResult ==. val True)
                        then_ $ val 1]
                      (else_ $ val 0)
        groupBy (autotest ^. AutoTestId, jobresult ^. JobResultId, job ^. JobId)
        return (autotest ^. AutoTestTest, jobresult ^. JobResultId, job ^. JobSoftware, countRows, sum_ ok)
    forM_ r $ \(Value testid, Value jobresultid, Value softwareid, Value (rows :: Int), Value (mpassed :: Maybe Int)) ->
        case mpassed of
            (Just passed) -> do
                let test = TestResult testid softwareid (rows == passed)
                testresultid <- insert test
                insert_ $ AutoTestResult testresultid jobresultid
            _ -> return ()
    return ()
