module Software where

import           ClassyPrelude.Yesod
import           Data.Aeson.TH       (defaultOptions, deriveJSON)

data Software = Software {
    softwareFamily  :: Text,
    softwareName    :: Text,
    softwareVersion :: Text,
    osName          :: Text,
    osVersion       :: Text,
    platform        :: Text
} deriving (Eq)
$(deriveJSON defaultOptions ''Software)
