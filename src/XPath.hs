module XPath (
    parseXPath,
    prefixes
) where

import           Data.Text                    (splitOn)
import           Data.Tree.NTree.TypeDefs
import           Import                       hiding (Value, check,
                                               compareLength, isNothing, on,
                                               toLower, (==.))
import           Text.Read                    (read)
import           Text.Regex.Posix
import           Text.XML.HXT.Core            (XNode (..), XmlTree)
import           Text.XML.HXT.XPath.XPathEval

prefixes :: [(Text,Text)]
prefixes = map (first tons) [ ("chart", "chart")
           , ("dr3d", "dr3d")
           , ("drawing", "draw")
           , ("form", "form")
           , ("manifest", "manifest")
           , ("meta", "meta")
           , ("office", "office")
           , ("presentation", "presentation")
           , ("script", "script")
           , ("smil-compatible", "smil")
           , ("style", "style")
           , ("svg-compatible", "svg")
           , ("table", "table")
           , ("text", "text")
           , ("xsl-fo-compatible", "fo") ]
           ++ [ ("http://purl.org/dc/elements/1.1/", "dc"),
                ("http://www.w3.org/1999/xhtml", "xhtml"),
                ("http://www.w3.org/1999/xlink", "xlink")]
  where
    tons n = "urn:oasis:names:tc:opendocument:xmlns:" ++ n ++ ":1.0"

compareLength :: Maybe Text -> String -> Maybe Text
compareLength Nothing _ = Nothing
compareLength (Just a) b | a == pack b = Just "True"
compareLength _ _ = Just "False"

atMay :: [a] -> Int -> Maybe a
atMay a i = listToMaybe $ drop i a

getItem :: Text -> Int -> String -> Maybe Text
getItem a pos sep = atMay (splitOn (pack sep) a) pos

compareItem :: Maybe Text -> Int -> String -> String -> Maybe Text
compareItem Nothing _ _ _ = Nothing
compareItem (Just a) pos sep val | val == "" =
        Just $ fromMaybe "" $ getItem a pos sep
compareItem (Just a) pos sep val =
    if getItem a pos sep == Just (pack val) then Just "True" else Just "False"

parseXPath' :: String -> Bool -> Either String (XmlTree -> Maybe Text)
parseXPath' xpath nested =
    case clerMatch of
        (_,_,_,[a,len]) ->
            case parseXPath' a True of
                 Left err -> Left err
                 Right f -> Right (\tree -> compareLength (f tree) len)
        _ -> case iterMatch of
                (_,_,_,[a,pos,sep,_,val]) ->
                    case parseXPath' a True of
                        Left err -> Left err
                        Right f -> Right (\tree -> compareItem (f tree) (read pos) sep val)

                _ -> case parsed of
                        Left err -> Left err
                        Right expr -> Right $ eval (getXPath' expr)
  where
    atts = map (\(a,b) -> (unpack b, unpack a)) prefixes
    eval expr xml = case expr xml of
                        [NTree (XText val) []] -> Just $ pack val
                        _ -> Nothing
    (cler :: String) = "^\\w*text:compareLength\\((.*),\\w*['\"](.*)['\"]\\)\\w*$"
    clerMatch = xpath =~ cler :: (String,String,String,[String])
    (iter :: String) = "^\\w*text:item\\((.*),\\w*([0-9]+)\\w*,\\w*['\"](.*)['\"]\\w*\\)\\w*(=['\"](.*)['\"])?\\w*$"
    iterMatch = xpath =~ iter :: (String,String,String,[String])
    parsed = if nested
                 then parse $ "string(" ++ xpath ++ ")"
                 else parse xpath
    parse = parseXPathExprWithNsEnv atts

parseXPath :: String -> Either String (XmlTree -> Maybe Text)
parseXPath s = parseXPath' s False
