module RetrieveJobs where

import           ClassyPrelude.Yesod
import           Data.Aeson.TH        (defaultOptions, deriveJSON)
import           Data.ByteString.Lazy as L (ByteString)

import           CommonTypes          (FactoryKey)
import           Service              (ServiceInstance)
import           Types.ByteString     ()

data RetrieveJobsRequest = RetrieveJobsRequest {
    factoryKey       :: FactoryKey,
    maxWaitTime      :: Int,
    nextNumber       :: Int,
    serviceInstances :: [ServiceInstance]
}
$(deriveJSON defaultOptions ''RetrieveJobsRequest)

data Job = Job {
    number    :: Int,
    service   :: ServiceInstance,
    inputFile :: L.ByteString
}
$(deriveJSON defaultOptions ''Job)

data RetrieveJobsResponse = RetrieveJobsResponse {
    jobs :: [Job]
}
$(deriveJSON defaultOptions ''RetrieveJobsResponse)
