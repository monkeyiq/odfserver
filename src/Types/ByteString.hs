{-# OPTIONS_GHC -fno-warn-orphans #-}
module Types.ByteString where

import           ClassyPrelude.Yesod
import           Data.Aeson.Types            (typeMismatch)
import           Data.ByteString.Base64.Lazy (decode, encode)
import           Data.ByteString.Lazy        as L (ByteString)
import           Data.Text.Lazy              as T (fromStrict, toStrict)

--- serialize ByteString as base64 in JSON ---

instance FromJSON L.ByteString where
    parseJSON (String text) =
        case decode $ encodeUtf8 $ T.fromStrict text of
            Left _      -> mzero
            Right bytes -> return bytes
    parseJSON invalid = typeMismatch "Lazy.ByteString" invalid
instance ToJSON L.ByteString where
    toJSON string = String $ T.toStrict $ decodeUtf8 $ encode string
