module Service where

import           ClassyPrelude.Yesod
import           Data.Aeson.TH       (defaultOptions, deriveJSON)

import           FileType            (FileType)
import           Software            (Software)

data Service = Service {
    name       :: Text,
    inputType  :: FileType,
    outputType :: FileType
} deriving (Eq)
$(deriveJSON defaultOptions ''Service)

data ServiceInstance = ServiceInstance {
    software :: Software,
    service  :: Service
} deriving (Eq)
$(deriveJSON defaultOptions ''ServiceInstance)
