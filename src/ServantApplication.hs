{-# OPTIONS_GHC -fno-warn-orphans #-}
module ServantApplication (servantApp) where

import           ClassyPrelude.Yesod        hiding (Proxy, Request, fromString)
import           Control.Monad.Trans.Either
import qualified Data.ByteString.Lazy       as LS
import           Data.ByteString.Lazy.UTF8  (fromString)
import           Network.Wai
import           Servant                    hiding (toText)
import           Servant.Docs
import           Servant.Docs.Pandoc
import           Text.Pandoc

import           CommonTypes                (FactoryKey (..), epochTime)
import           FactoryAPI
import           FileType
import           RetrieveJobs
import           RPC.Impl                   (AppM, ServantConfig)
import           RPC.Impl.Register          (register)
import           RPC.Impl.RetrieveJobs      (retrievejobs)
import           RPC.Impl.UploadJobResult   (uploadjobresult)
import           Service
import           Software
import           UploadJobResult

factoryAPI :: Proxy FactoryAPI
factoryAPI = Proxy

server :: ServerT FactoryAPI AppM
server = register :<|> retrievejobs :<|> uploadjobresult

-- register

sampleFactoryKey :: FactoryKey
sampleFactoryKey = FactoryKey "8f742ff4-d210-4df7-93d5-0b8d70cc872e" "76a8507d-dfdf-4a6a-b5f8-e5a26e9ad3cd"

instance ToSample RegisterRequest RegisterRequest where
  toSamples _ = [("An empty body.", RegisterRequest)]

instance ToSample FactoryKey FactoryKey where
  toSamples _ = [("The factoryId and factoryKey. The factoryId is the public indentifier for the factory. The factoryKey is the private key with which the factory authenticates itself to the server.", sampleFactoryKey)]

-- retrievejobs --

sampleServiceInstance :: ServiceInstance
sampleServiceInstance = ServiceInstance
    Software {
        softwareFamily  = "Microsoft Office",
        softwareName    = "Microsoft Office",
        softwareVersion = "2016 ...",
        osName          = "Windows 10",
        osVersion       = "patch level ...",
        platform        = "x86_64" } $
    Service "convert" ODT1_1 ODT1_2

sampleRetrieveJobsRequest :: RetrieveJobsRequest
sampleRetrieveJobsRequest = RetrieveJobsRequest sampleFactoryKey 0 0 [
    sampleServiceInstance
  ]

sampleRetrieveJobsResponse :: RetrieveJobsResponse
sampleRetrieveJobsResponse = RetrieveJobsResponse [ Job{
    number   = 0,
    service  = sampleServiceInstance,
    inputFile = ""}]

instance ToSample RetrieveJobsRequest RetrieveJobsRequest where
  toSamples _ = [("?", sampleRetrieveJobsRequest)]

instance ToSample RetrieveJobsResponse RetrieveJobsResponse where
  toSamples _ = [("An array with jobs.", sampleRetrieveJobsResponse)]

-- uploadjobresult --

sampleUploadJobResult :: UploadJobResult
sampleUploadJobResult = UploadJobResult {
    factory    = sampleFactoryKey,
    number     = 0,
    started    = epochTime,
    finished   = epochTime,
    outputFile = Nothing,
    exitCode   = 0,
    stdout     = "",
    stderr     = ""
}

instance ToSample UploadJobResult UploadJobResult where
  toSamples _ = [("HI HI", sampleUploadJobResult)]

instance ToSample UploadJobResultResponse UploadJobResultResponse where
  toSamples _ = [("HO HO", UploadJobResultResponse)]

docsBS :: LS.ByteString
docsBS = fromString $ writeHtmlString def . pandoc
    $ docsWithIntros [intro] factoryAPI
  where
    intro = DocIntro "ODF Test Server Factory API"
      [ "This is the factory interface to the ODF test server. A factory is a service that connects and ODF implementation, such as Microsoft Office or LibreOffice, to the test server. The server gives the factory ODF documents to run a job on. The factory sends the result back to the factory."
      , "A job is usually the roundtripping or conversion of an ODF document. Roundtripping means that the ODF implementation loads the file and saves it back to the same ODF version. Conversion means that the implementation loads the ODF file and saves it as a different ODF version. A conversion can be from ODF 1.2 to ODF 1.1 or from ODF 1.2 to PDF."
      , "# register"
      , "The register request is for aquiring a unique id and a key from the server. The factory is identified by the id and the key is used by the factory to authenticate itself to the server."
      ]

type DocsAPI = FactoryAPI :<|> "help" :> Raw

api :: Proxy DocsAPI
api = Proxy

servedocs :: Request -> (Network.Wai.Response -> IO ResponseReceived) -> IO ResponseReceived
servedocs _ respond' = respond' $ responseLBS ok200 [plain] docsBS
  where plain = ("Content-Type", "text/html")

readerToEither :: ServantConfig -> AppM :~> EitherT ServantErr IO
readerToEither cfg = Nat $ \x -> runReaderT x cfg

readerServer :: ServantConfig -> Server DocsAPI
readerServer cfg = enter (readerToEither cfg) server
    :<|> servedocs

servantApp :: ServantConfig -> Application
servantApp cfg = serve api (readerServer cfg)
