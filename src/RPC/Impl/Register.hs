module RPC.Impl.Register (register) where

import           ClassyPrelude.Yesod
import           Data.UUID           (toText)
import           Data.UUID.V4        (nextRandom)

import           CommonTypes         (FactoryKey (..))
import           Database            (registerFactory)
import           FactoryAPI          (RegisterRequest)
import           RPC.Impl            (AppM, runDb)

register :: RegisterRequest -> AppM FactoryKey
register _ = do
     factoryuuid <- liftIO nextRandom
     factorykey <- liftIO nextRandom
     -- TODO: use Servant.API.RemoteHost to get host when moving to Servant 0.5
     let host = "" :: String
     registerTime <- liftIO getCurrentTime
     let uuid = toText factoryuuid
         key = toText factorykey
         registerIp = pack $ show host
     runDb $ registerFactory uuid key registerTime registerIp
     return $ FactoryKey uuid key
