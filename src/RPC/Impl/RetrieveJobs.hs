module RPC.Impl.RetrieveJobs (retrievejobs) where

import           ClassyPrelude.Yesod  hiding (Value, fromList, isNothing, on,
                                       (==.))
import qualified Data.ByteString.Lazy as LS
import           Data.Either          (isLeft)
import           Data.List            (nub)
import           Data.Map             (fromList, (!))
import           Data.Time            (diffUTCTime)
import           Database.Esqueleto   hiding (fromSqlKey, update, (=.))
import           Database.Persist.Sql (fromSqlKey)

import qualified CommonTypes          as C
import qualified Database             as D
import           Model
import qualified RetrieveJobs         as RJ
import           RPC.Impl             (AppM, ServantConfig, checkFactoryKey,
                                       checkInput, runDb, scheduleTestJobs,
                                       waitForJobs)
import qualified Service              as SE
import qualified Software             as SO

getSoftwaresMap :: MonadIO m => [SoftwareOSId] -> ReaderT SqlBackend m (Map SoftwareOSId SO.Software)
getSoftwaresMap ids = do
    rows <- select $ from $ \(softwareos `InnerJoin` platform
            `InnerJoin` osversion `InnerJoin` os `InnerJoin` swversion
            `InnerJoin` software `InnerJoin` swfamily) -> do
        on $ software ^. SoftwareSoftwareFamily ==. swfamily ^. SoftwareFamilyId
        on $ swversion ^. SoftwareVersionSoftware ==. software ^. SoftwareId
        on $ softwareos ^. SoftwareOSSoftware ==. swversion ^. SoftwareVersionId
        on $ osversion ^. OSVersionOs ==. os ^. OperatingSystemId
        on $ softwareos ^. SoftwareOSOs ==. osversion ^. OSVersionId
        on $ softwareos ^. SoftwareOSPlatform ==. platform ^. PlatformId
        where_ (softwareos ^. SoftwareOSId `in_` valList ids)
        return (softwareos ^. SoftwareOSId,
                swfamily ^. SoftwareFamilyName,
                software ^. SoftwareName,
                swversion ^. SoftwareVersionName,
                os ^. OperatingSystemName,
                osversion ^. OSVersionName,
                platform ^. PlatformName)
    return $ fromList $ map (\(Value sid, Value swfamily, Value name,
                            Value version, Value os, Value osversion,
                            Value platform)
        -> (sid, SO.Software { softwareFamily  = swfamily,
                               softwareName    = name,
                               softwareVersion = version,
                               osName          = os,
                               osVersion       = osversion,
                               platform        = platform})) rows

-- find jobs that fit the given factory and that have not been taken yet
-- the service lastseen value should correspond to the service instance
findApplicableJobs :: MonadIO m => Key Factory -> UTCTime
    -> ReaderT ServantConfig m [RJ.Job]
findApplicableJobs factoryid lastseen = do
    (rows, swmap) <- runDb $ do
        rows :: [(Value JobId,Value SoftwareOSId, Value ServiceInstanceId, Value ByteString, Entity Service)]<- select
            $ from $ \(job `InnerJoin` serviceinstance
                      `InnerJoin` blob `InnerJoin` service) -> do
            on $ job ^. JobService ==. service ^. ServiceId
            on $ job ^. JobInput ==. blob ^. BlobId
            on $ job ^. JobSoftware ==. serviceinstance ^. ServiceInstanceSoftware
               &&. job ^. JobService ==. serviceinstance ^. ServiceInstanceService
            where_ $ isNothing $ job ^. JobServiceInstance
            where_ $ serviceinstance ^. ServiceInstanceFactory ==. val factoryid
            where_ $ serviceinstance ^. ServiceInstanceLastSeen ==. val lastseen
            offset 0
            limit 1 -- limit to one job for now
            return ( job ^. JobId
                   , job ^. JobSoftware
                   , serviceinstance ^. ServiceInstanceId
                   , blob ^. BlobContent
                   , service )
        -- register which instance is running the job
        mapM_ assignServiceInstance rows
        swmap <- getSoftwaresMap $ nub $ map softwareOSId rows
        return (rows, swmap)

    return $ map (createJob swmap) rows
    where
        toInt = fromIntegral . fromSqlKey
        createJob swmap (Value job, Value sid, _, Value blob,
                  Entity _ Service{serviceName,serviceInputType,serviceOutputType})
                    = RJ.Job (toInt job) (SE.ServiceInstance sw se) (LS.fromStrict blob)
            where
                se = SE.Service serviceName serviceInputType serviceOutputType
                sw = swmap ! sid
        assignServiceInstance (Value jobid, _, Value siid, _, _) =
            update jobid [JobServiceInstance =. Just siid]
        softwareOSId (_, Value sid, _, _, _) = sid :: SoftwareOSId

waitForApplicableJobs :: MonadIO m => Int -> Key Factory -> UTCTime -> ReaderT ServantConfig m [RJ.Job]
waitForApplicableJobs wait factoryid lastSeen = do
    now <- liftIO getCurrentTime
    jobs <- findApplicableJobs factoryid lastSeen
    case jobs of
        [] -> do
            _ <- waitForJobs wait
            now2 <- liftIO getCurrentTime
            let d = 1000000 * diffUTCTime now2 now
                waitLeft = wait - floor d
            if waitLeft > 0
                then waitForApplicableJobs waitLeft factoryid lastSeen
                else return []
        _ -> return jobs

retrievejobs :: RJ.RetrieveJobsRequest -> AppM RJ.RetrieveJobsResponse
retrievejobs request = do
    lastSeen <- liftIO getCurrentTime
    let (RJ.RetrieveJobsRequest key maxWait _ instances) = request
    checkFactoryKey key
    checkInput (null instances) "No instances were provided."
    -- register services for this factory in the database
    result <- runDb $ D.registerServiceInstances (C.factoryId key) lastSeen instances
    checkInput (isLeft result) "Could not find factory"
    let (Right factoryid) = result
    let wait = max 20000 (min maxWait 60000000)
    scheduleTestJobs
    jobs <- waitForApplicableJobs wait factoryid lastSeen
    return $ RJ.RetrieveJobsResponse jobs
