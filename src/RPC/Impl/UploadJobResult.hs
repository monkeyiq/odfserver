{-# LANGUAGE Arrows #-}

module RPC.Impl.UploadJobResult (uploadjobresult) where

import           ClassyPrelude.Yesod      hiding (stderr, stdout)
import qualified Data.ByteString.Char8    as BC
import           Data.ByteString.Lazy     as T (toStrict)
import           Data.Maybe               (fromJust)
import           Data.String.Conversions  (cs)
import           Data.Tree.NTree.TypeDefs (NTree)
import           Database.Persist.Sql
import           Text.Read                (read)
import           Text.XML.HXT.Core

import qualified CommonTypes              as CT
import qualified Database                 as D
import           FileType
import           Model
import           RPC.Impl                 (AppM, checkFactoryKey, checkInput,
                                           runDb)
import           TestScheduler
import           UploadJobResult          as UJ

data ValidationReport = ValidationReport { msg       :: String,
                                           component :: Maybe Text,
                                           line, col :: Maybe Int }
  deriving (Show, Eq)

atTag :: ArrowXml a => String -> a (NTree XNode) XmlTree
atTag  tag = deep (isElem >>> hasName tag)
-- atAttr :: ArrowXml cat => String -> cat XmlTree XmlTree
-- atAttr tag = getAttrl >>> deep (hasName tag)
text :: IOSLA (XIOState ()) (NTree XNode) String
text = getChildren >>> getText
getAttrOptional :: String -> IOSLA (XIOState ()) (NTree XNode) (Maybe String)
getAttrOptional aaa =
  getAttrValue aaa >>> arr Just
    `orElse` constA Nothing

stot:: Maybe String -> Maybe Text
stot s = case s of
         Nothing -> Nothing
         Just "" -> Nothing
         Just n -> Just (cs n)
stoi:: Maybe String -> Maybe Int
stoi s = case s of
         Nothing -> Nothing
         Just "" -> Nothing
         Just n -> Just (read n)

getValidationReport :: IOSLA (XIOState ()) (NTree XNode) ValidationReport
getValidationReport = atTag "error" >>>
  proc x -> do
    lcomponent <- getAttrOptional "component" -< x
    lline      <- getAttrOptional "start-line" -< x
    lcol       <- getAttrOptional "start-column" -< x
    lmsg       <- text -< x
    returnA   -< ValidationReport { msg = lmsg,
                                    component = stot lcomponent,
                                    line = stoi lline,
                                    col  = stoi lcol
                                  }

-- stot :: Maybe String -> Maybe Text
-- stot x = do
--    case x of
--       Nothing -> return Nothing
--       Just s  -> return Just $ cs s


uploadjobresult :: UJ.UploadJobResult -> AppM UJ.UploadJobResultResponse
uploadjobresult UJ.UploadJobResult {
            factory,
            number,
            started,
            finished,
            outputFile,
            exitCode,
            stdout,
            stderr
        } = do
    checkInput (finished < started) "Finished time is smaller than started time"
    checkFactoryKey factory
    let (jobid :: JobId) = toSqlKey $ fromIntegral number
    maybeJob <- runDb $ get jobid
    checkInput (isNothing maybeJob) "Invalid job number"
    let maybeInstance = jobServiceInstance $ fromJust maybeJob
    checkInput (isNothing maybeInstance)
        "No service instance associated with this job."
    let job = fromJust maybeJob
    let instanceId = fromJust maybeInstance
    (Just sinstance) <- runDb $ get instanceId
    (Just factory') <- runDb $ get (serviceInstanceFactory sinstance)
    (Just service) <- runDb $ get (serviceInstanceService sinstance)
    checkInput (factoryUuid factory' /= CT.factoryId factory)
        "This factory is not registered to handle this job."
    validationReport <- case serviceOutputType service of
                   VALIDATIONREPORT -> do
                        let outputFileAsString = BC.unpack $ T.toStrict $ fromMaybe "" outputFile
                        liftIO $
                            runX (
                                 readString [withValidate no] outputFileAsString
                                 >>> getValidationReport )
                   _ -> return []
    runDb $ do
        maybeBlobId <- case outputFile of
            Nothing -> return Nothing
            Just file -> do
                blobid <- D.addBlob file $ serviceOutputType service
                return $ Just blobid
        mapM_
           (\x -> do
              let lmsg  = cs (msg x)
              insert_ Validation {
                  validationJob       = jobid,
                  validationComponent = component x,
                  validationLine      = line x,
                  validationColumn    = col x,
                  validationError     = lmsg }
              ) validationReport
        insert_ JobResult {
            jobResultJob = jobid,
            jobResultStarted = started,
            jobResultFinished = finished,
            jobResultResult = maybeBlobId,
            jobResultExitCode = exitCode,
            jobResultStdout = stdout,
            jobResultStderr = stderr}
        evaluateAutoTests
    return UJ.UploadJobResultResponse
