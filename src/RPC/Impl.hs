module RPC.Impl (
    AppM,
    ServantConfig(..),
    runDb,
    checkInput,
    checkFactoryKey,
    scheduleTestJobs,
    waitForJobs) where

import           ClassyPrelude.Yesod
import qualified CommonTypes                as C
import           Control.Monad.Except
import           Control.Monad.Trans.Either (EitherT)
import           Control.Monad.Trans.Reader (asks)
import qualified Data.ByteString.Lazy       as LS
import qualified Data.Maybe                 as M (fromJust, isNothing)
import           Import
import           Servant                    (ServantErr (..), err400)

import qualified Database                   as D
import qualified TestScheduler              as T

runDb :: MonadIO m => SqlPersistT IO b -> ReaderT ServantConfig m b
runDb query = do
    db <- asks database
    liftIO $ D.runDb query db

waitForJobs :: MonadIO m => Int -> ReaderT ServantConfig m ()
waitForJobs maxWait = do
    db <- asks database
    _ <- liftIO $ D.waitForJobs db maxWait
    return ()

scheduleTestJobs :: AppM ()
scheduleTestJobs = do
    db <- asks database
    T.scheduleTestJobs db

data ServantConfig = ServantConfig { database :: D.Database }

type AppM = ReaderT ServantConfig (EitherT ServantErr IO)

checkInput :: Bool -> LS.ByteString -> AppM ()
checkInput True errMsg = throwError $ err400 { errBody = errMsg }
checkInput False _ = return ()

checkFactoryKey :: C.FactoryKey -> AppM ()
checkFactoryKey (C.FactoryKey uuid key) = do
    maybeFactory <- runDb $ getBy $ UniqueFactory uuid
    checkInput (M.isNothing maybeFactory) "Factory is not registered."
    let Entity _ (Factory{factoryKey=fkey}) = M.fromJust maybeFactory
    checkInput (fkey /= key) "Factory has wrong key."
