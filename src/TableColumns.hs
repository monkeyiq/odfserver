module TableColumns where

import           ClassyPrelude.Yesod         hiding (decodeUtf8, decompress,
                                              encodeUtf8, get)
import           Codec.Compression.Zlib      (compress, decompress)
import           Data.Binary
import qualified Data.ByteString.Base64.Lazy as B (decodeLenient, encode)
import           Data.Text.Lazy.Encoding     (decodeUtf8, encodeUtf8)

import           FileType
import           Model

-- utils

fromPathPiece' :: Binary a => Text -> Maybe a
fromPathPiece' t = case decodeOrFail $ decompress $ B.decodeLenient $ encodeUtf8 $ fromStrict t of
                       Left _ -> Nothing
                       Right (_,_,a) -> Just a
toPathPiece' :: Binary a => a -> Text
toPathPiece' = toStrict . decodeUtf8 . B.encode . compress . encode

data SortDirection = SortAsc | SortDesc
  deriving (Eq, Read, Show, Generic)
instance Binary SortDirection

-- JobsWidget

data JobsWidgetColumn = JobsWidgetCreated
                     | JobsWidgetUser
                     | JobsWidgetService
                     | JobsWidgetInput
                     | JobsWidgetOutput
                     | JobsWidgetAssigned
                     | JobsWidgetDone
  deriving (Eq, Read, Show, Generic)

data JobsWidgetSort = JobsWidgetSort {
    jobSortCol :: JobsWidgetColumn,
    jobSortDir :: SortDirection
} deriving (Eq, Read, Show, Generic)

data JobsWidgetFilter = JobsWidgetFilterUser UserId
                     | JobsWidgetFilterInput FileType
                     | JobsWidgetFilterOutput FileType
                     | JobsWidgetFilterFactory (Maybe FactoryId)
  deriving (Eq, Generic, Read, Show)

data JobsView = JobsView {
    jobsViewOffset :: Int,
    jobsViewLimit  :: Int,
    jobsViewCols   :: [JobsWidgetColumn],
    jobsViewSort   :: [JobsWidgetSort],
    jobsViewFilter :: [JobsWidgetFilter]
} deriving (Eq, Generic, Read, Show)

instance PathPiece JobsView where
    fromPathPiece = fromPathPiece'
    toPathPiece = toPathPiece'

instance Binary JobsView
instance Binary JobsWidgetColumn
instance Binary JobsWidgetSort
instance Binary JobsWidgetFilter

-- TestsWidget

data TestsWidgetColumn = TestsWidgetName
                     | TestsWidgetCreated
                     | TestsWidgetPassCount
                     | TestsWidgetResultCount
                     | TestsWidgetPassFraction
                     | TestsWidgetUser
  deriving (Eq, Read, Show, Generic)

data TestsWidgetSort = TestsWidgetSort {
    testSortCol :: TestsWidgetColumn,
    testSortDir :: SortDirection
} deriving (Eq, Read, Show, Generic)

data TestsWidgetFilter = TestsWidgetFilterUser UserId
  deriving (Eq, Generic, Read, Show)

data TestsView = TestsView {
    testsViewOffset :: Int,
    testsViewLimit  :: Int,
    testsViewCols   :: [TestsWidgetColumn],
    testsViewSort   :: [TestsWidgetSort],
    testsViewFilter :: [TestsWidgetFilter]
} deriving (Eq, Generic, Read, Show)

instance PathPiece TestsView where
    fromPathPiece = fromPathPiece'
    toPathPiece = toPathPiece'

instance Binary TestsView
instance Binary TestsWidgetColumn
instance Binary TestsWidgetSort
instance Binary TestsWidgetFilter

-- TestResultsWidget

data TestResultsWidgetColumn = TestResultsWidgetPassed
                     | TestResultsWidgetTest
                     | TestResultsWidgetFamily
  deriving (Eq, Read, Show, Generic)

data TestResultsWidgetSort = TestResultsWidgetSort {
    testResultSortCol :: TestResultsWidgetColumn,
    testResultSortDir :: SortDirection
} deriving (Eq, Read, Show, Generic)

data TestResultsWidgetFilter = TestResultsWidgetFilterTest TestId
  deriving (Eq, Generic, Read, Show)

data TestResultsView = TestResultsView {
    testResultsViewOffset :: Int,
    testResultsViewLimit  :: Int,
    testResultsViewCols   :: [TestResultsWidgetColumn],
    testResultsViewSort   :: [TestResultsWidgetSort],
    testResultsViewFilter :: [TestResultsWidgetFilter]
} deriving (Eq, Generic, Read, Show)

instance PathPiece TestResultsView where
    fromPathPiece = fromPathPiece'
    toPathPiece = toPathPiece'

instance Binary TestResultsView
instance Binary TestResultsWidgetColumn
instance Binary TestResultsWidgetSort
instance Binary TestResultsWidgetFilter
