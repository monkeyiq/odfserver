module HtmlFilter (pageFilter, rssFilter, commentFilter) where

import           Data.Set              (fromAscList)
import           Import                hiding (fromList)
import           Text.HTML.SanitizeXSS
import           Text.HTML.TagSoup

attFilter :: (Text, Text) -> Bool
attFilter (name, _) = name `member` allowedAtts

filterTag :: Set Text -> Tag Text -> Maybe (Tag Text)
filterTag _ (TagText t) = Just $ TagText t
filterTag tags (TagOpen t atts) | t `member` tags
    = Just $ TagOpen t (filter attFilter atts)
filterTag tags (TagClose t) | t `member` tags = Just $ TagClose t
filterTag _ _ = Nothing

makeFilter :: Text -> Text -> Text
makeFilter tags = filterTags $ balanceTags . safeTags . tagFilter
    where tagFilter :: [Tag Text] -> [Tag Text]
          tagFilter = mapMaybe $ filterTag set
          set :: Set Text
          set = makeSet tags

makeSet :: Text -> Set Text
makeSet = fromAscList . sort . words

allowedAtts :: Set Text
allowedAtts = makeSet "href src alt style colspan rowspan"

rssFilter :: Text -> Text
rssFilter = makeFilter "div p h1 h2 h3 h4 h5 h6 table tr th td ol ul li dl dd dt img hr br a pre i b em tt"

pageFilter :: Text -> Text
pageFilter = rssFilter

commentFilter :: Text -> Text
commentFilter = makeFilter "p i b em"
