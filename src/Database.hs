module Database (
    Database(..),
    addBlob,
    addJob,
    addUpload,
    createDatabase,
    getFactoryKey,
    insertMany_,
    insertOrGet,
    registerFactory,
    registerServiceInstances,
    runDb,
    setFactoryKey,
    signalNewJob,
    waitForJobs
) where

import           ClassyPrelude.Yesod        hiding (first, insertMany_, newChan)
import qualified ClassyPrelude.Yesod        as Y
import qualified Control.Concurrent.Chan    as C
import           Control.Monad.IO.Class     (MonadIO)
import qualified Data.ByteString.Lazy.Char8 as L
import           Data.Digest.Pure.SHA       (bytestringDigest, sha256)
import           Data.Pool                  (Pool)
import           Database.Persist.Sql       (runSqlPool)
import           System.Timeout             (timeout)
import           Yesod.Auth.Email           (saltPass)

import qualified CommonTypes                as CT
import qualified DbWrap                     as DW
import qualified FileType                   as F
import qualified MimeType                   as M
import           Model
import qualified Service                    as S
import qualified Software                   as SW

data Database = Database {
     connPool :: DW.DbWrap,
     chan     :: C.Chan ()
}

runDb :: (Control.Monad.IO.Class.MonadIO m, MonadBaseControl IO m) =>
            SqlPersistT m b -> Database -> m b
runDb action db = DW.runDbRead2 action $ connPool db

waitForJobs :: Database -> Int -> IO ()
waitForJobs db maxWait = do
    _ <- timeout maxWait $ C.readChan (chan db)
    return ()

addTestAccount :: Bool -> Pool SqlBackend -> IO ()
addTestAccount False _ = return ()
addTestAccount True pool = do
    let uid = "user@user.nl"
        pwd = "user"
    sp <- saltPass pwd
    flip runSqlPool pool $ do
        _ <- insertUnique $ User uid uid (Just sp) Nothing True CT.epochTime
        return ()

createDatabase :: Pool SqlBackend -> IO Database
createDatabase pool = do
    addTestAccount True pool
    wrap <- DW.createDbWrap pool
    newChan <- C.newChan
    return $ Database wrap newChan

registerFactory :: MonadIO m => Text -> Text -> UTCTime -> Text -> ReaderT SqlBackend m ()
registerFactory uuid password registerTime registerIp = do
    let factory = Factory uuid password registerTime registerIp registerTime
    insert_ factory

-- get any key from the factory table
-- Factories use this to retrieve their identity from their local database
getFactoryKey :: Database -> IO (Maybe CT.FactoryKey)
getFactoryKey (Database pool _) = do
    -- get any key
    first <- flip DW.runDbRead pool $ selectFirst ([] :: [Filter Factory]) []
    let key = case first of
         Just (Entity _ (Factory uuid key' _ _ _)) ->
             Just $ CT.FactoryKey uuid key'
         Nothing -> Nothing
    return key

-- store the key of a factory
-- Factories use this to store their identity in their local database
setFactoryKey :: Database -> CT.FactoryKey -> IO CT.FactoryKey
setFactoryKey (Database pool _) key = do
    let factory = Factory (CT.factoryId key) (CT.factoryKey key) CT.epochTime "" CT.epochTime
    flip DW.runDbWrite pool $ insert_ factory
    return key

-- Insert a new value if it is not in the table yet. return the identifier
-- to the existing or new value
insertOrGet :: (MonadIO m, PersistEntity val, PersistUnique backend, PersistEntityBackend val ~ backend) => val -> ReaderT backend m (Key val)
insertOrGet value = do
    res <- getByValue value
    case res of
        Nothing -> insert value
        Just (Entity key _) -> return key

insertOrReplaceUnique :: (MonadIO m, PersistEntity val, PersistUnique backend, PersistEntityBackend val ~ backend) => val -> ReaderT backend m (Key val)
insertOrReplaceUnique value = do
    res <- getByValue value
    case res of
        Nothing -> insert value
        Just (Entity key _) -> do
            replace key value
            return key

addBlob :: MonadIO m => L.ByteString -> F.FileType -> ReaderT SqlBackend m BlobId
addBlob bytes fileType = do
    let sha = L.toStrict $ bytestringDigest $ sha256 bytes
    insertOrGet $ Blob sha (L.toStrict bytes) (fromIntegral $ L.length bytes) fileType

addUpload :: MonadIO m => L.ByteString -> UserId -> Text -> UTCTime -> ReaderT SqlBackend m UploadId
addUpload bytes uid filename ctime = do
    let (M.FileInfo mimetype filetype) = M.getFileInfo bytes
    blobid <- addBlob bytes filetype
    res <- getBy $ UniqueUpload filename blobid uid
    case res of
        Nothing -> do
            proseid <- insert $ Prose Nothing Nothing ctime uid True "" "" ""
            let upload = Upload filename blobid uid ctime mimetype proseid
            insert upload
        Just (Entity key _) -> return key

registerServiceInstances :: MonadIO m => Text -> UTCTime -> [S.ServiceInstance] -> ReaderT SqlBackend m (Either String FactoryId)
registerServiceInstances facUuid now serviceInstances = do
    maybeFactory <- getBy $ UniqueFactory facUuid
    case maybeFactory of
        Nothing -> return $ Left "Cannot find factory."
        Just (Entity factoryId _) -> do
            update factoryId [FactoryLastSeen =. now]
            mapM_ (registerSI factoryId) serviceInstances
            return $ Right factoryId
    where
        registerSI factid (S.ServiceInstance sw sr) = do
            let (S.Service name' inputType outputType) = sr
            family <- insertOrGet $ SoftwareFamily $ SW.softwareFamily sw
            software <- insertOrGet $ Software (SW.softwareName sw) family
            swversion <- insertOrGet $ SoftwareVersion software Nothing
                    (SW.softwareVersion sw) Nothing CT.epochTime
            os <- insertOrGet $ OperatingSystem (SW.osName sw) Nothing
            osversion <- insertOrGet $ OSVersion os (SW.osVersion sw)
                    Nothing CT.epochTime
            platform <- insertOrGet $ Platform (SW.platform sw)
            swos <- insertOrGet $ SoftwareOS swversion osversion platform
            serId <- insertOrGet $ Service name' inputType outputType
            let si = ServiceInstance swos serId factid now
            _ <- insertOrReplaceUnique si
            return ()

addJob :: (MonadIO m, MonadBaseControl IO m, PersistEntity val,
                 PersistEntityBackend val ~ SqlBackend) =>
                                 Database -> val -> m (Key val)
addJob db job = do
    jobid <- runDb (insertOrGet job) db
    liftIO $ C.writeChan (chan db) ()
    return jobid

signalNewJob :: Database -> IO ()
signalNewJob db = C.writeChan (chan db) ()

chunk :: Int -> [a] -> [[a]]
chunk _ [] = []
chunk n xs = ys : chunk n zs
  where (ys,zs) = splitAt n xs

-- | safe version of persist insertMany_ that only inserts a limited number
-- of items at a time
insertMany_ :: (MonadIO m, PersistStore (PersistEntityBackend val), PersistEntity val) => Int -> [val] -> ReaderT (PersistEntityBackend val) m ()
insertMany_ valuesPerRow rows = do
    mapM_ Y.insertMany_ $ chunk (499 `div` valuesPerRow) rows
    return ()
