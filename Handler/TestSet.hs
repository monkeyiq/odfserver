module Handler.TestSet (
    getTestSetsR,
    getTestSetR
) where

import           Import

getTestSetsR :: Handler Html
getTestSetsR = undefined

getTestSetR :: TestSetId -> Handler Html
getTestSetR testsetid = do
    testset <- runDB $ get404 testsetid
    defaultLayout $ do
        setTitle $ toHtml $ "Test set " ++ testSetName testset
        $(widgetFile "testset")
