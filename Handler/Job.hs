module Handler.Job (
    getJobR,
    postNewJobR,
    getJobsR
) where

import           Data.String.Conversions (cs)
import           Database.Esqueleto      hiding (count, get)
import           Import                  hiding (Value, count, formatTime, get,
                                          on, (==.))
import qualified Import                  as I

import qualified Database                as D
import           JobsWidget
import           TableColumns
import           TestResultTableWidget   (getValidation)
import           TimeUtil

-- return instances if no job for this (uploadid,serviceid,softwareid) has run
-- yet
getServiceInstances :: (YesodPersist site, YesodPersistBackend site ~ SqlBackend) => UserId -> UploadId -> SoftwareOSId -> ServiceId -> HandlerT site IO [ServiceInstanceId]
getServiceInstances userid uploadid softwareid serviceid = do
    instances <- runDB
        $ select
        $ from $ \(upload `InnerJoin` blob `InnerJoin` service `InnerJoin` serviceinstance
                     `InnerJoin` software) -> do
             on $ serviceinstance ^. ServiceInstanceSoftware ==. software ^. SoftwareOSId
             on $ service ^. ServiceId ==. serviceinstance ^. ServiceInstanceService
             on $ blob ^. BlobFileType ==. service ^. ServiceInputType
             on $ upload ^. UploadContent  ==. blob ^. BlobId
             where_ $ upload ^. UploadId ==. val uploadid
             where_ $ upload ^. UploadUploader ==. val userid
             where_ $ service ^. ServiceId ==. val serviceid
             where_ $ software ^. SoftwareOSId ==. val softwareid
             offset 0
             limit 1000
             return
                 ( serviceinstance ^. ServiceInstanceId )
    return $ map (\(Value si) -> si) instances

postNewJobR :: UploadId -> SoftwareOSId -> ServiceId -> Handler Html
postNewJobR uploadid softwareid serviceid = do
    userid <- requireAuthId
    upload <- runDB $ get404 uploadid
    let blobid = uploadContent upload
    instances <- getServiceInstances userid uploadid softwareid serviceid
    now <- lift getCurrentTime
    case instances of
        [] -> invalidArgs [ "No serviceinstances available." ]
        (_:_) -> do
            let job = Job userid now blobid softwareid serviceid Nothing
            App _ _ _ db _ _ <- getYesod
            jobid <- D.addJob db job
            redirect $ JobR jobid

displayText:: Maybe Text -> String
displayText s = case s of
         Nothing -> ""
         Just n -> cs n
displayInt:: Maybe Int -> String
displayInt s = case s of
         Nothing -> ""
         Just n -> show n

getJobsR :: JobsView -> Handler Html
getJobsR jobsview = do
    let offset' = jobsViewOffset jobsview
        count = jobsViewLimit jobsview
    when (offset' < 0 || count < 1 || count > 100) $ invalidArgs []
    jobswidget <- getJobsWidget True jobsview
    defaultLayout $ do
        setTitle "Jobs"
        $(widgetFile "jobs")

getJobR :: JobId -> Handler Html
getJobR jobid = do
    (job, user, uploads, service, _, mFactory, mResult, validationErrors) <- runDB $ do
        job <- get404 jobid
        user <- get404 $ jobUser job
        let inputBlobId = jobInput job
        uploads <- selectList [UploadContent I.==. inputBlobId] []
        service <- get404 $ jobService job
        software <- get404 $ jobSoftware job
        maybeFactory <-
            case jobServiceInstance job of
                Nothing -> return Nothing
                Just instanceId -> do
                    sinstance <- get404 instanceId
                    factory <- get404 $ serviceInstanceFactory sinstance
                    return $ Just factory
        mResult <- case jobServiceInstance job of
            Nothing -> return Nothing
            Just _ -> do
                mr <- getBy $ UniqueJobResult jobid
                return $ unwrap mr
        validationErrors <- getValidation [inputBlobId]
        let validationErrors' = filter ((jobid ==) . validationJob)
                                <$> lookup inputBlobId validationErrors
        return (job, user, uploads, service, software, maybeFactory, mResult, validationErrors')
    defaultLayout $ do
        setTitle "Job"
        $(widgetFile "job")
    where unwrap Nothing = Nothing
          unwrap (Just (Entity _ r)) = Just r
