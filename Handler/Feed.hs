module Handler.Feed (getFeedR) where

import           Data.Text          as T (replace)
import           Database.Esqueleto
import           Import             hiding (Value, on, (==.))
import           Yesod.Feed         as F

import           CommonTypes        (epochTime)

getFeedR :: Lang -> Handler TypedContent
getFeedR lang = do
    pages <- runDB getPages
    feed <- feedFromPages lang pages
    newsFeed feed

-- | should be lowercase and _ translated to - .e.g. en_US -> en-us
langToRssLang :: Text -> Text
langToRssLang = T.replace "_" "-" . toLower

pageToRssEntry :: (PageId, UTCTime, Text, Html, Text) -> Handler (FeedEntry (Route App))
pageToRssEntry (pageid, created, title, content, _) = return FeedEntry
    { feedEntryLink    = PageR pageid
    , feedEntryUpdated = created
    , feedEntryTitle   = title
    , feedEntryContent = content
    , feedEntryEnclosure = Nothing
    }

feedFromPages :: Lang -> [(PageId, UTCTime, Text, Html, Text)] -> Handler (F.Feed (Route App))
feedFromPages lang pages = do
    entries <- mapM pageToRssEntry pages
    let dates = map (\(_,date,_,_,_) -> date) pages
        updated = head' dates epochTime
    return F.Feed
        { feedAuthor      = "TODO"
        , feedDescription = "Global ODF users and developers"
        , feedEntries     = entries
        , feedLanguage    = langToRssLang lang
        , feedLinkHome    = HomeR
        , feedLinkSelf    = FeedR lang
        , feedLogo        = Nothing
        , feedTitle       = "ODF Community"
        , feedUpdated     = updated
        }

getPages :: MonadIO m => ReaderT SqlBackend m [(PageId, UTCTime, Text, Html, Text)]
getPages = do
    res <- select $ from $ \(page `InnerJoin` prose `InnerJoin` user) -> do
        on $ prose ^. ProseAuthor ==. user ^. UserId
        on $ page ^. PageCurrent ==. prose ^. ProseId
        orderBy [desc (prose ^. ProseCreated)]
        limit 10
        return ( page ^. PageId
               , prose ^. ProseCreated
               , prose ^. ProseTitle
               , prose ^. ProseHtmlContent
               , user  ^. UserName
               )
    return $ map unwrap res
  where
    unwrap (Value a, Value b, Value c, Value d, Value e) = (a, b, c, d, e)
