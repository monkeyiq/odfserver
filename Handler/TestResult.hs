module Handler.TestResult (
    getTestResultR,
    getTestResultsR
) where

import           Import            hiding (count)

import           TableColumns
import           TestResultsWidget

getTestResultR :: TestResultId -> Handler Html
getTestResultR _ = undefined

getTestResultsR :: TestResultsView -> Handler Html
getTestResultsR testresultsview = do
    let offset' = testResultsViewOffset testresultsview
        count = testResultsViewLimit testresultsview
    when (offset' < 0 || count < 1 || count > 100) $ invalidArgs []
    testResultsWidget <- getTestResultsWidget True testresultsview
    defaultLayout $ do
        setTitle "Test Results"
        $(widgetFile "testresults")
