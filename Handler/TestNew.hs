module Handler.TestNew (
    getTestNewR, postTestNewR,
    getTestCloneR, postTestCloneR
) where

import           Codec.Archive.Zip        (Archive)
import           Control.Applicative      as A
import           Data.List                (nub)
import           Data.Maybe               (fromJust)
import           Data.Tree.NTree.TypeDefs
import           Database.Esqueleto       hiding (insertMany_)
import           Import                   hiding (check, insertMany_, on, (==.))
import           Text.XML.HXT.Core        (XNode (..), XmlTree)
import           Yesod.Form.Bootstrap3

import           Database
import           FileType
import           TestInputCreator
import           TestScheduler
import qualified XPath                    as X

type XmlFiles = [(Text, XmlTree)]

data XPathState = XPathTrue
                | XPathWrong Text Text
                | XPathInvalid Text
                | XPathIrrelevant Text deriving Eq

data XPath = XPath {
    xpathPath  :: Text,
    xpathXPath :: Text,
    xpathState :: XPathState
} deriving Eq

data DocumentFromTemplate = DocumentFromTemplate {
    templateText :: Text,
    templateXml  :: XmlTree
}

data FormTest = FormTest {
    formTestName        :: Text,
    formTestInput       :: DocumentFromTemplate,
    formTestOutputTypes :: [FileType],
    formXPaths          :: [XPath]
}

data RequestData = RequestData {
    reqUserId        :: UserId
  , reqOdfType       :: FileType
  , reqTemplate      :: XmlFiles
  , reqArchive       :: Archive
  , reqInput         :: XmlFiles
  , reqXmlParseError :: Maybe Text
}

parseXPath :: Maybe XmlTree -> Maybe XmlTree -> Text -> Text -> XPath
parseXPath template testfile xpath file = XPath file xpath x
  where
    x = case X.parseXPath (unpack xpath) of
            Left err -> XPathInvalid $ pack err
            Right expr ->
                case (template, testfile) of
                    (Just t, Just i) -> check (expr t) (expr i)
                    _ -> XPathWrong "" (file ++ " was not modified.")
    check (Just tr) (Just ir) | tr == ir && tr == "True" = XPathIrrelevant tr
    check (Just _) (Just "True") = XPathTrue
    check (Just _) (Just ir) = XPathWrong ir msg
    check _ _ = XPathWrong "" msg
    msg = "Result is not True"

parseXPaths :: XmlFiles -> XmlFiles -> [Text] -> [XPath]
parseXPaths template input (x:_:ys) | null x = parseXPaths template input ys
parseXPaths template input (x:y:ys) = xpath:parseXPaths template input ys
  where t = snd <$> find (\(n,_) -> n == y) template
        i = snd <$> find (\(n,_) -> n == y) input
        xpath = parseXPath t i x y
parseXPaths _ _ _ = []

xpathsField :: XmlFiles -> XmlFiles -> Field Handler [XPath]
xpathsField template zipfiles = Field
    { fieldParse = \rawVals _ ->
        return $ Right $ Just $ parseXPaths template zipfiles rawVals
    , fieldView = \idAttr nameAttr otherAttrs eResult _ -> do
        let xpaths = case eResult of
                    Left _ -> []
                    Right result -> zip result [0::Int ..]
            numPaths = length xpaths
            empty' = XPath "" "" XPathTrue
        $(widgetFile "test_form_xpath_table")
    , fieldEnctype = UrlEncoded
    }
  where
    files = map fst zipfiles
    row idAttr nameAttr otherAttrs options pos xpath
        = $(widgetFile "test_form_xpath_row")
      where posid = unpack idAttr ++ show pos
            haserror = xpathState xpath /= XPathTrue

wrapInElement :: Text -> Text
wrapInElement xml = "<a " ++ ns ++ ">" ++ xml ++ "</a>"
   where
     ns = concatMap (\(a, b) -> " xmlns:" ++ b ++ "='" ++ a ++ "'") X.prefixes

parseXMLField :: [Text] -> IO (Either Text (Text, XmlTree))
parseXMLField [xml] | null xml = return $ Left "Empty xml"
parseXMLField [xml] = do
    r <- parseXML $ unpack $ wrapInElement xml
    case r of
        Left e -> return $ Left e
        Right (NTree _ [tree]) -> return $ Right (xml, tree)
        _ -> return $ Left "Parsing error"
parseXMLField a = return $ Left $ pack
    $ "Exactly one result expected instead of " ++ show (length a) ++ "."

xmlTemplateField :: Maybe Text -> Field Handler DocumentFromTemplate
xmlTemplateField merror = Field
    { fieldParse = \rawVals _ -> do
        r <- liftIO $ parseXMLField rawVals
        return $ case merror of
            Just err -> Left $ SomeMessage err
            Nothing -> case r of
                Right (raw, xml) -> Right $ Just $ DocumentFromTemplate raw xml
                Left err -> Left $ SomeMessage err
    , fieldView = \idAttr nameAttr otherAttrs eResult _ -> do
        let xml = case eResult of
                    Left unparsed -> unparsed
                    Right (DocumentFromTemplate xml' _) -> xml'
        $(widgetFile "test_form_fragment")
    , fieldEnctype= UrlEncoded
    }

getTargetTypes :: FileType -> [FileType]
getTargetTypes fileType = fromMaybe [] $ find (elem fileType) allOdfTypes

defaultInput :: FileType -> Text
defaultInput fileType | fileType `elem` odtTypes
    = "<office:text>\n  <text:p></text:p>\n</office:text>\n"
defaultInput fileType | fileType `elem` odsTypes
    = "<office:spreadsheet>\n</office:spreadsheet>\n"
defaultInput fileType | fileType `elem` odpTypes
    = "<office:presentation>\n</office:presentation>\n"
defaultInput _ = ""

defaultFragment :: FileType -> DocumentFromTemplate
defaultFragment fileType =
        DocumentFromTemplate (s ++ defaultInput fileType) (NTree (XCmt "") [])
    where s = "<office:styles>\n</office:styles>\n"

-- | A form for writing tests
testForm :: RequestData -> Maybe FormTest -> Form FormTest
testForm req test = renderDivs $ FormTest
    A.<$> areq textField (bfs ("Name" :: Text)) (formTestName <$> test)
    A.<*> areq xmlField (bfs ("Input" :: Text)) xml
    A.<*> areq versionField "Version " setTypes
    A.<*> areq xpathsField' (bfs ("XPath" :: Text)) (formXPaths <$> test)
  where
    xmlField = xmlTemplateField $ reqXmlParseError req
    versionField = checkboxesFieldList $ types odfTypes
    xpathsField' = xpathsField (reqTemplate req) (reqInput req)
    odfTypes = getTargetTypes $ reqOdfType req
    types :: [FileType] -> [(Text, FileType)]
    types = map (\t -> (showOdfVersion t, t))
    setTypes :: Maybe [FileType]
    setTypes = Just $ maybe odfTypes formTestOutputTypes test
    xml = Just $ maybe (defaultFragment $ reqOdfType req) formTestInput test

-- | Wrap the FormTest in a \<form\> with css styling
-- If 'withSave' is @True@ a save button is shown.
wrapTestFormWidget :: Enctype -> Bool -> Widget -> Widget
wrapTestFormWidget enctype withSave widget = do
    setTitleI MsgWriteANewTest
    addScript $ StaticR codemirror_js_codemirror_js
    addScript $ StaticR codemirror_js_xml_js
    addStylesheet $ StaticR codemirror_css_codemirror_css
    $(widgetFile "test_form")

commonPost :: UploadId -> Handler RequestData
commonPost uploadid = do
    r <- runInputPostResult $ ireq (xmlTemplateField Nothing) "f2"
    common uploadid r

common :: UploadId -> FormResult DocumentFromTemplate -> Handler RequestData
common uploadid r = do
    userId <- requireAuthId
    blob <- runDB $ do
        upload <- get404 uploadid
        get404 $ uploadContent upload
    res <- liftIO $ getXmlEntries $ blobContent blob
    case res of
        Left _ -> fail "Cannot read template file."
        Right (archive, template) -> do
            -- xml field has to be processed before the rest of the form
            -- because xpath parsing relies on the xml field
            let (input, merror) = parse template r
            return RequestData {
                reqUserId = userId,
                reqOdfType = blobFileType blob,
                reqTemplate = template,
                reqArchive = archive,
                reqInput = input,
                reqXmlParseError = merror
            }
  where
    parse template (FormSuccess xml)
        = case createTestInput template $ templateXml xml of
              Left err -> (template, Just err)
              Right input -> (input, Nothing)
    parse template _ = (template, Nothing)

getTestNewR :: UploadId -> Handler Html
getTestNewR uploadId = do
    requestData <- common uploadId FormMissing
    let form = testForm requestData Nothing
    (widget, enctype) <- generateFormPost form
    defaultLayout $
        wrapTestFormWidget enctype False widget

checkFormData :: FormResult FormTest -> Either Text FormTest
checkFormData result =
    case result of
        FormSuccess form -> checkForm form
        FormFailure msgs -> Left $ concat msgs
        FormMissing -> Left "The form was missing."
  where
    checkForm f | null $ formXPaths f = Left "At least one XPath test is needed."
    checkForm f | xpathNotAllTrue f = Left "Please fix the XPaths."
    checkForm f = Right f
    xpathNotAllTrue f = not $ all (XPathTrue ==) $ map xpathState $ formXPaths f

findTest :: MonadHandler m => Test -> BlobId -> UploadId -> BlobId -> [AutoTestXPathId] -> ReaderT SqlBackend m (Maybe (TestId, AutoTestId))
findTest test' fragment uploadid file xpaths = do
    res <- select $ from $ \(test `InnerJoin` autotest `InnerJoin` xpath) -> do
        on (autotest ^. AutoTestId ==. xpath ^. AutoTestXPathUseTest)
        on (test ^. TestId ==. autotest ^. AutoTestTest)
        where_ (test ^. TestName ==. val (testName test'))
        where_ (autotest ^. AutoTestInputFragment ==. val fragment)
        where_ (autotest ^. AutoTestInputTemplate ==. val uploadid)
        where_ (autotest ^. AutoTestInputFile ==. val file)
        where_ (notIn (xpath ^. AutoTestXPathUseXpath) (valList xpaths))
        offset 0
        limit 1
        return (test ^. TestId, autotest ^. AutoTestId)
    case res of
        ((Value testid, Value autotestid):_) -> return $ Just (testid, autotestid)
        _ -> return Nothing

saveAutoTest :: RequestData -> UploadId -> FormTest -> Handler TestId
saveAutoTest requestData uploadId form = do
    now <- liftIO getCurrentTime
    let name = formTestName form
        content = encodeUtf8 $ fromStrict $ templateText $ formTestInput form
        newTest = Test Nothing now (reqUserId requestData) name
        file = createOdf (reqInput requestData) (reqArchive requestData)
    runDB $ do
        fragmentId <- addBlob content XML
        fileId <- addBlob file $ reqOdfType requestData
        x <- mapM (\x -> upsert (xpathToDb x) []) $ formXPaths form
        let xids = map (\(Entity key _) -> key) x
        mid <- findTest newTest fragmentId uploadId fileId xids
        (testId, test) <- case mid of
            Just ids -> return ids
            Nothing -> do
                testId <- insert newTest
                let autotest = AutoTest testId fragmentId uploadId fileId
                test <- insert autotest
                return (testId, test)
        insertMany_ 2 $ map (AutoTestOutputType test) $ formTestOutputTypes form
        insertMany_ 2 $ map (\(Entity k _) -> AutoTestXPathUse test k) x
        evaluateAutoTests
        return testId
  where
    xpathToDb (XPath path xpath _) = AutoTestXPath path xpath

postTestNewR :: UploadId -> Handler Html
postTestNewR uploadId = do
    requestData <- commonPost uploadId
    let form = testForm requestData Nothing
    ((result, widget), enctype) <- runFormPost form
    isSave <- runInputPost $ iopt boolField "save"
    let result' = checkFormData result
    case result' of
        Right formData ->
            case isSave of
                Just True -> do
                    testId <- saveAutoTest requestData uploadId formData
                    App{appDatabase} <- getYesod
                    scheduleTestJobs appDatabase
                    setMessage "The test is saved."
                    redirect $ TestR testId
                _ -> defaultLayout $ do
                    setMessage "The test is valid."
                    wrapTestFormWidget enctype True widget
        Left errorMsg ->
            case isSave of
                Just True -> invalidArgs [errorMsg]
                _ -> do
                    setMessage $ toHtml errorMsg
                    defaultLayout $ wrapTestFormWidget enctype False widget

loadTest :: MonadIO m => Entity AutoTest -> ReaderT SqlBackend m FormTest
loadTest (Entity autotestid autotest) = do
    rows <- select $ from $ \(output `InnerJoin` use `InnerJoin` xpath) -> do
        on $ use ^. AutoTestXPathUseXpath ==. xpath ^. AutoTestXPathId
        on $ output ^. AutoTestOutputTypeTest ==. use ^. AutoTestXPathUseTest
        where_ $ output ^. AutoTestOutputTypeTest ==. val autotestid
        return (output ^. AutoTestOutputTypeFileType, xpath)
    let rows' = map (unValue *** entityVal) rows
    test <- get404 $ autoTestTest autotest
    fragment <- get404 $ autoTestInputFragment autotest
    let fragmentText = decodeUtf8 $ blobContent fragment
    Right (fragmentDoc, xml) <- liftIO $ parseXMLField [fragmentText]
    let input = DocumentFromTemplate {
            templateText = fragmentDoc,
            templateXml = xml
        }
        xpaths = nub $ map (\(_,AutoTestXPath a b) -> XPath a b XPathTrue) rows'
    return FormTest {
        formTestName        = testName test,
        formTestInput       = input,
        formTestOutputTypes = nub $ map fst rows',
        formXPaths          = xpaths
    }

getTestCloneR :: TestId -> Handler Html
getTestCloneR testid = do
    (uploadId, formData) <- runDB $ do
        mautotest <- getBy $ UniqueTest testid
        let autotest = fromJust mautotest
        let uploadId = autoTestInputTemplate $ entityVal autotest
        formData <- loadTest autotest
        return (uploadId, formData)
    requestData <- common uploadId $ FormSuccess $ formTestInput formData
    let form = testForm requestData $ Just formData
    (widget, enctype) <- generateFormPost form
    defaultLayout $
        wrapTestFormWidget enctype False widget

postTestCloneR :: TestId -> Handler Html
postTestCloneR testid = do
    mautotest <- runDB $ getBy $ UniqueTest testid
    let autotest = fromJust mautotest
    postTestNewR $ autoTestInputTemplate $ entityVal autotest
