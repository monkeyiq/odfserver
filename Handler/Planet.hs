module Handler.Planet (
    getPlanetR
) where

import           Database.Esqueleto hiding (groupBy)
import           Import             hiding (formatTime, on, (==.))
import qualified Import             as I

import           TimeUtil

planetList :: Widget
planetList = do
    feeds <- handlerToWidget $ runDB $ selectList [FeedActive I.==. True] []
    $(widgetFile "planet_feeds")

groupFeedItems :: (IsSequence seq, Element seq ~ (t2, t3, FeedItem),
                         Element seq ~ (t, t1, FeedItem)) =>
                                                 seq -> [seq]
groupFeedItems = groupBy fromSameDay
    where
        fromSameDay a b = day a == day b
        day (_, _, FeedItem{feedItemPubDate}) = utctDay feedItemPubDate

dayWidget :: [(FeedId, Feed, FeedItem)] -> Widget
dayWidget (x:xs) = do
    let (_,_,FeedItem{feedItemPubDate}) = x
        day = utctDay feedItemPubDate
        items = x:xs
    $(widgetFile "planet_day")
dayWidget _ = return ()

getPlanetR :: Handler Html
getPlanetR = do
    items <- runDB getFeedItems
    let groupedItems = groupFeedItems items
    defaultLayout $ do
        setTitle "Planet ODF"
        $(widgetFile "planet")

getFeedItems :: MonadIO m => ReaderT SqlBackend m [(FeedId, Feed, FeedItem)]
getFeedItems = do
    rows <- select $ from $ \(item `InnerJoin` feed) -> do
        on $ item ^. FeedItemFeed  ==. feed ^. FeedId
--        where_ $ item ^. FeedItemShow ==. val True
        where_ $ feed ^. FeedActive ==. val True
        orderBy [desc (item ^. FeedItemPubDate)]
        offset 0
        limit 50
        return (feed, item)
    return $ map unwrap rows
    where unwrap (Entity a b, Entity _ c) = (a, b, c)
