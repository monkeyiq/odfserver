module Handler.Home (getHomeR) where

import           Import

import           JobsWidget
import           TableColumns

getHomeR :: Handler Html
getHomeR = do
    let jobsview = JobsView 0 20 [JobsWidgetUser] [] []
    jobswidget <- getJobsWidget False jobsview
    defaultLayout $ do
        setTitleI MsgOdfCommunity
        $(widgetFile "home")
