-- | Common handler functions.
module Handler.Common where

import           Codec.Archive.Zip
import           Data.FileEmbed    (embedFile)
import           Import

import           MimeType

-- These handlers embed files in the executable at compile time to avoid a
-- runtime dependency, and for efficiency.

getFaviconR :: Handler TypedContent
getFaviconR = return $ TypedContent "image/x-icon"
                     $ toContent $(embedFile "config/favicon.ico")

getRobotsR :: Handler TypedContent
getRobotsR = return $ TypedContent typePlain
                    $ toContent $(embedFile "config/robots.txt")

getBlobR :: BlobId -> Handler TypedContent
getBlobR blobid = do
    (Blob _ content _ _) <- runDB $ get404 blobid
    let fi = getFileInfo $ fromStrict content
    return $ TypedContent (encodeUtf8 $ mimetype fi) $ toContent content

getUnzipR :: BlobId -> [Text] -> Handler TypedContent
getUnzipR blobid pathpieces = do
    let path = intercalate "/" pathpieces
    (Blob _ bytes _ _) <- runDB $ get404 blobid
    case toArchiveOrFail $ fromStrict bytes of
        Right archive ->
            case findEntryByPath (unpack path) archive of
                Just entry -> do
                    let e = fromEntry entry
                        fi = getFileInfo e
                    return $ TypedContent (encodeUtf8 $ mimetype fi) $ toContent e
                Nothing -> invalidArgs []
        Left _ -> invalidArgs []

getLangR :: Lang -> Handler ()
getLangR lang = do
    setUltDestReferer
    setLanguage lang
    setMessageI $ MsgLanguageSetTo $ languageName lang
    redirectUltDest HomeR
