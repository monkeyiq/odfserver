module Handler.User (
    getUserR,
    getUserEditR,
    postUserEditR
) where

import           Control.Applicative   as A
import           Import
import           Yesod.Form.Bootstrap3

import           JobsWidget
import           TableColumns
import           TestsWidget
import           TimeUtil

data FormUser = FormUser {
    formUserName :: Text
}

userForm :: FormUser -> Form FormUser
userForm user = renderDivs $ FormUser
    A.<$> areq textField (bfs ("Name" :: Text)) (Just $ formUserName user)

getUserR :: UserId -> Handler Html
getUserR userid = do
    maid <- maybeAuthId
    (User name email _ _ active joined) <- runDB $ get404 userid
    joinedTime <- formatRelativeTime joined
    let jobsview = JobsView 0 20 [] [] [JobsWidgetFilterUser userid]
    jobWidget <- getJobsWidget False jobsview
    let testsview = TestsView 0 20 [] [] [TestsWidgetFilterUser userid]
    testsWidget <- getTestsWidget False testsview
    defaultLayout $ do
        setTitle "User"
        $(widgetFile "user")

editWidget :: UserId -> Enctype -> Widget -> Widget
editWidget _ enctype widget = $(widgetFile "user_form")

getUserEditR :: UserId -> Handler Html
getUserEditR userid = do
    user <- runDB $ get404 userid
    let formData = FormUser $ userName user
    (widget, enctype) <- generateFormPost $ userForm formData
    defaultLayout $ do
        setTitle "User"
        editWidget userid enctype widget

postUserEditR :: UserId -> Handler Html
postUserEditR userid = do
    user <- runDB $ get404 userid
    let formData = FormUser $ userName user
    ((result, widget), enctype) <- runFormPost $ userForm formData
    case result of
        FormSuccess (FormUser name) -> do
            runDB $ update userid [UserName =. name]
            setMessage "Your profile was updated."
            redirect $ UserR userid
        _ -> do
            setMessage "Your profile was not updated."
            defaultLayout $ do
                setTitle "User"
                editWidget userid enctype widget
