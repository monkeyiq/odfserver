module Handler.TestSetNew (
    getTestSetNewR,
    postTestSetNewR
) where

import           Control.Applicative as A
import           Data.Maybe          (fromJust)
import           Import              hiding (insertMany_)

import           Database

data FormTestSet = FormTestSet {
    formTestSetName  :: Text,
    formTestSetTests :: [TestId],
    formTestSetRef   :: Maybe TestSetId
}

testSetForm :: [Entity Test] -> FormTestSet -> Form FormTestSet
testSetForm tests testset = renderDivs $ FormTestSet
    A.<$> areq textField "Name" (Just $ formTestSetName testset)
    A.<*> areq testsField "Tests" setTests
    A.<*> areq hiddenField "" (Just $ formTestSetRef testset)
  where
    entityToTuple (Entity testid Test{testName}) = (testName, testid)
    testsField = checkboxesFieldList $ map entityToTuple tests
    setTests = Just $ formTestSetTests testset

-- get highest testsetid for a testset by the given user
getHighestUserTestSetId :: MonadIO m => UserId -> ReaderT SqlBackend m (Maybe TestSetId)
getHighestUserTestSetId userid = do
    res <- selectList [TestSetAuthor ==. userid] [LimitTo 1, Desc TestSetId]
    case res of
        Entity testsetid _:_ -> return $ Just testsetid
        _ -> return Nothing

common :: Handler (UserId, Maybe TestSetId, [Entity Test])
common = do
    userid <- requireAuthId
    (maybePrevious, tests) <- runDB $ do
        maybePrevious <- getHighestUserTestSetId userid
        tests <- selectList [] []
        return (maybePrevious, tests)
    return (userid, maybePrevious, tests)

getTestSetNewR :: Handler Html
getTestSetNewR = do
    (_, maybePrevious, tests) <- common
    let form = testSetForm tests $ FormTestSet "" [] maybePrevious
    (widget, enctype) <- generateFormPost form
    defaultLayout $ $(widgetFile "testset_form")

postTestSetNewR :: Handler Html
postTestSetNewR = do
    (userid, maybePrevious, allTests) <- common
    let form = testSetForm allTests $ FormTestSet "" [] maybePrevious
    ((result, widget), enctype) <- runFormPost form
    case result of
        FormSuccess (FormTestSet name tests mprev) ->
            -- check that this is not a resubmission of a form
            if maybePrevious /= mprev
                then redirect $ TestSetR $ fromJust maybePrevious
                else do
                    testsetid <- runDB $ do
                        now <- liftIO getCurrentTime
                        testsetid <- insert $ TestSet Nothing now userid name
                        insertMany_ 2 $ map (TestSetMember testsetid) tests
                        return testsetid
                    setMessage "Your test set was created."
                    redirect $ TestSetR testsetid
        _ -> do
            setMessage "Your profile was not updated."
            defaultLayout $ $(widgetFile "testset_form")
