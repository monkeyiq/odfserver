module Handler.Comment (
    getCommentEditR, postCommentEditR,
    getCommentNewR, postCommentNewR,
    getCommentR,
) where

import           Database.Esqueleto hiding (isNothing, update, (=.))
import           Import             hiding (groupBy, on, (==.), (>.))
import qualified Yesod.Persist      as P ((==.))

import           CommentWidgets
import           Handler.Page
import           Handler.Upload
import           Prose

-- | Structure that holds the different possible types a 'Prose' may have.
data ArticleId = Page' PageId | Upload' UploadId | Comment' CommentId | NoId

-- | Find out what type a 'Prose' instance has.
-- (If this become unwieldy, it can become a column in the Prose table.)
proseIdToArticleId :: MonadIO m => ProseId -> ReaderT SqlBackend m ArticleId
proseIdToArticleId proseid = do
    res <- select $ from $ \(p `LeftOuterJoin` page `LeftOuterJoin` upload `LeftOuterJoin` comment) -> do
        on (just (p ^. ProseId) ==. comment ?. CommentComment)
        on (just (p ^. ProseId) ==. upload ?. UploadDescription)
        on (just (p ^. ProseId) ==. page ?. PageCurrent)
        where_ (p ^. ProseId ==. val proseid)
        groupBy (p ^. ProseId, page ?. PageId, upload ?. UploadId, comment ?. CommentId)
        offset 0
        limit 1
        return ( page ?. PageId
               , upload ?. UploadId
               , comment ?. CommentId
               )
    case res of
        ((Value (Just pageid),_,_):_) -> return $ Page' pageid
        ((_,Value (Just uploadid),_):_) -> return $ Upload' uploadid
        ((_,_,Value (Just commentid)):_) -> return $ Comment' commentid
        _ -> return NoId

-- | Create a route to a page that shows the Prose instance.
parentRoute :: MonadIO m => ProseId -> ReaderT SqlBackend m (Route App)
parentRoute proseid = do
    res <- proseIdToArticleId proseid
    return $ case res of
        Page' pageid -> PageR pageid
        Upload' uploadid -> UploadR uploadid
        Comment' commentid' -> CommentR commentid'
        _ -> HomeR

-- | Create a route to a comment in a discussion thread.
-- The route points to the page showing the 'Prose'. It has a URL fragment
-- that points to the comment. The fragment is encoded in 'CommentWidgets'.
-- This route is followed after a comment has been saved.
commentRoute :: MonadIO m => ProseId -> CommentId -> ReaderT SqlBackend m (Fragment (Route App) String)
commentRoute root commentid = do
    url <- parentRoute root
    return $ url :#: ("comment-" ++ show (fromSqlKey commentid))

-- | Widget that shows a page for a Prose instance.
-- This is used to show the context in which a comment is being written.
articleWidget :: ProseId -> Widget
articleWidget proseid = do
    (prose, user, res) <- handlerToWidget $ runDB $ do
        root <- getCommentRoot proseid
        prose <- get404 root
        user <- get404 $ proseAuthor prose
        res <- proseIdToArticleId root
        return (prose, user, res)
    case res of
        Page' pageid ->
            pageWidget (Just pageid) user False prose
        Upload' uploadid ->
            uploadWidget uploadid
        _ -> [whamlet| ERROR|]

-- get highest proseid for a comment by a given user to a given proseid
getHighestUserCommentProseId :: MonadIO m => UserId -> ProseId -> ReaderT SqlBackend m (Maybe ProseId)
getHighestUserCommentProseId userid parentid = do
    res <- select $ from $ \(comment `InnerJoin` prose) -> do
        on $ comment ^. CommentComment ==. prose ^. ProseId
        where_ $ comment ^. CommentParent ==. val parentid
                 &&. prose ^. ProseAuthor ==. val userid
        orderBy [desc (comment ^. CommentId)]
        limit 1
        return (prose ^. ProseId)
    case res of
        Value proseid:_ -> return $ Just proseid
        _ -> return Nothing

-- get commentid and proseid that follows the given proseid for a page by the given user
getNextUserCommentId :: MonadIO m => UserId -> ProseId -> Maybe ProseId -> ReaderT SqlBackend m (Maybe (CommentId, ProseId))
getNextUserCommentId userid parentid mprevproseid = do
    res <- select $ from $ \(comment `InnerJoin` prose) -> do
        on $ comment ^. CommentComment ==. prose ^. ProseId
        let clause = comment ^. CommentParent ==. val parentid
                 &&. prose ^. ProseAuthor ==. val userid
        where_ $ case mprevproseid of
                     Just prev -> clause &&. prose ^. ProseId >. val prev
                     Nothing -> clause
        orderBy [asc (comment ^. CommentId)]
        limit 1
        return (comment ^. CommentId, prose ^. ProseId)
    case res of
        (Value commentid, Value proseid):_ -> return $ Just (commentid, proseid)
        _ -> return Nothing

getCommentR :: CommentId -> Handler Html
getCommentR commentid = do
    route <- runDB $ do
        comment <- get404 commentid
        parentRoute $ commentParent comment
    defaultLayout $ commentWidget commentid route

getCommentNewR :: ProseId -> Handler Html
getCommentNewR parentid = do
    userid <- requireAuthId
    (maybePrevious, parent, root) <- runDB $ do
        maybePrevious <- getHighestUserCommentProseId userid parentid
        parent <- get404 parentid
        root <- getCommentRoot parentid
        return (maybePrevious, parent, root)
    let edit = proseFormWidget $ FormProse (proseTitle parent)
            (Textarea "") (Just parentid) maybePrevious
    defaultLayout $ do
        setTitleI MsgWriteANewComment
        articleWidget root
        discussionWidget root $ Just (parentid, edit, False)

postCommentNewR :: ProseId -> Handler Html
postCommentNewR parentid = do
    (userid, user, result, widget, enctype, isSave) <- runProseForm
    case result of
        FormSuccess entry -> do
            newProse <- createProse userid entry
            case isSave of
                Just True -> do
                    route <- runDB $ do
                        let prev = formProseReference entry
                        next <- getNextUserCommentId userid parentid prev
                        root <- getCommentRoot parentid
                        commentid <- case next of
                            Just (commentid, proseid) -> do
                                updated <- do
                                    (oldP, nextP) <-
                                        deriveProse proseid userid entry
                                    updateComment commentid proseid oldP nextP
                                when updated $ setMessageI MsgCommentWasUpdated
                                return commentid
                            Nothing -> do
                                proseid <- insert newProse
                                insert $ Comment root parentid proseid
                        commentRoute root commentid
                    redirect route
                _ -> defaultLayout $ commentPostForm parentid False userid
                        user enctype widget newProse
        _ -> do
            setMessageI MsgSomethingWentWrong
            redirect $ CommentNewR parentid

getCommentEditR :: CommentId -> Handler Html
getCommentEditR commentid = do
    _ <- requireAuthId
    (proseid, prose, root) <- runDB $ do
        comment <- get404 commentid
        let proseid = commentComment comment
        prose <- get404 proseid
        root <- getCommentRoot proseid
        return (proseid, prose, root)
    let formProse = FormProse (proseTitle prose) (Textarea $ proseContent prose)
            Nothing (Just proseid)
        edit = proseFormWidget  formProse
    defaultLayout $ do
        setTitleI MsgEditAComment
        articleWidget root
        discussionWidget root $ Just (proseid, edit, True)

updateComment :: MonadIO m => CommentId -> ProseId -> Prose -> Prose -> ReaderT SqlBackend m Bool
updateComment commentid oldProseId oldProse newProse = do
    -- only save if something changed
    let updated = proseTitle oldProse /= proseTitle newProse
         || proseContent oldProse /= proseContent newProse
    when updated $ do
        newProseId <- insert newProse
        update commentid [CommentComment =. newProseId]
        -- make all the replies point to the new Prose
        updateWhere [CommentParent P.==. oldProseId] [CommentParent =. newProseId]
    return updated

postCommentEditR :: CommentId -> Handler Html
postCommentEditR commentid = do
    (userid, user, result, widget, enctype, _) <- runProseForm
    isSave <- runInputPost $ iopt boolField "save"
    case result of
        FormSuccess entry -> do
            (comment, proseid, prose, newProse) <- runDB $ do
                comment <- get404 commentid
                let proseid = commentComment comment
                (prose, newProse) <- deriveProse proseid userid entry
                return (comment, proseid, prose, newProse)
            when (Just proseid /= formProseReference entry) $
                redirect $ CommentEditR commentid
            case isSave of
                Just True -> do
                    (updated, route) <- runDB $ do
                        updated <- updateComment commentid proseid prose newProse
                        route <- commentRoute (commentRoot comment) commentid
                        return (updated, route)
                    when updated $ setMessageI MsgCommentWasUpdated
                    redirect route
                _ -> defaultLayout $ do
                    let parentid = commentParent comment
                    commentPostForm parentid True userid user enctype widget
                        newProse
        _ -> do
            setMessageI MsgSomethingWentWrong
            redirect PageNewR

commentPostForm :: ProseId -> Bool -> UserId -> User -> Enctype -> Widget -> Prose -> Widget
commentPostForm parentid isnew userid user enctype widget newProse = do
    articleWidget parentid
    root <- handlerToWidget $ runDB $ getCommentRoot parentid
    let edit = wrapProseFormWidget enctype True widget
        title = proseTitle newProse
        body = proseHtmlContent newProse
        ctime = proseCreated newProse
        comment' = commentBodyWidget Nothing title userid
            (userName user) ctime body Nothing $ Just edit
    discussionWidget root $ Just (parentid, comment', not isnew)
