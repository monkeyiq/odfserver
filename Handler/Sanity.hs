module Handler.Sanity (
    getSanityR
) where

import           Database.Esqueleto hiding (count)
import           Import             hiding (Value, groupBy, on, (!=.), (==.))

import           FileType

getAutoTestsWithInconsistentTypes :: MonadHandler m => ReaderT SqlBackend m [(TestId, FileType, FileType)]
getAutoTestsWithInconsistentTypes = do
    rows <- select $ from $ \(test `InnerJoin` autotest `InnerJoin` upload `InnerJoin` blob1 `InnerJoin` blob2) -> do
        on $ upload ^. UploadContent ==. blob2 ^. BlobId
        on $ autotest ^. AutoTestInputFile ==. blob1 ^. BlobId
        on $ autotest ^. AutoTestInputTemplate ==. upload ^. UploadId
        on $ test ^. TestId ==. autotest ^. AutoTestTest
        where_ $ blob1 ^. BlobFileType !=. blob2 ^. BlobFileType
        return (test ^. TestId, blob1 ^. BlobFileType, blob2 ^. BlobFileType)
    return $ map (\(Value a,Value b,Value c)->(a,b,c)) rows

getResultsWithWrongJob :: MonadHandler m => ReaderT SqlBackend m [(Entity Test,BlobId,BlobId)]
getResultsWithWrongJob = do
    rows <- select $ from $ \(autoresult `InnerJoin` testresult `InnerJoin` test
                             `InnerJoin` autotest `InnerJoin` jobresult
                             `InnerJoin` job) -> do
        on $ jobresult ^. JobResultJob ==. job ^. JobId
        on $ autoresult ^. AutoTestResultJob ==. jobresult ^. JobResultId
        on $ test ^. TestId ==. autotest ^. AutoTestTest
        on $ testresult ^. TestResultTest ==. test ^. TestId
        on $ autoresult ^. AutoTestResultResult ==. testresult ^. TestResultId
        where_ $ autotest ^. AutoTestInputFile !=. job ^. JobInput
        groupBy (test ^. TestId, autotest ^. AutoTestInputFile,job ^. JobInput)
        orderBy [asc (test ^. TestId)]
        return (test,autotest ^. AutoTestInputFile,job ^. JobInput)
    return $ map (\(a,Value b,Value c) -> (a,b,c)) rows

getSanityR :: Handler Html
getSanityR = do
    autoTestsWithInconsistentTypes <- runDB getAutoTestsWithInconsistentTypes
    resultsWithWrongJob <- runDB getResultsWithWrongJob
    defaultLayout $ do
        setTitle "sanity checks"
        $(widgetFile "sanity")
