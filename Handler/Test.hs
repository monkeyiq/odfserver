module Handler.Test (
    getTestR,
    getTestsR
) where

import           Database.Esqueleto     hiding (count)
import           Import                 hiding (Value, count, on, (==.))
import           Text.Highlighting.Kate (defaultFormatOpts, formatHtmlBlock,
                                         highlightAs)

import           FileType
import           TableColumns
import           TestResultsWidget
import           TestResultTableWidget
import           TestsWidget

xmlToHtml :: Text -> Html
xmlToHtml xml = formatHtmlBlock defaultFormatOpts
              $ highlightAs "xml" $ unpack xml

getAutoTest :: MonadHandler m => TestId -> ReaderT SqlBackend m (Test,Entity User, Blob, Entity Upload)
getAutoTest testid = do
    res <- select $ from $ \(test `InnerJoin` autotest `InnerJoin` user `InnerJoin` blob `InnerJoin` upload) -> do
        on (autotest ^. AutoTestInputTemplate ==. upload ^. UploadId)
        on (autotest ^. AutoTestInputFragment ==. blob ^. BlobId)
        on (test ^. TestAuthor ==. user ^. UserId)
        on (test ^. TestId ==. autotest ^. AutoTestTest)
        where_ (test ^. TestId ==. val testid)
        offset 0
        limit 1
        return (test, user, blob, upload)
    case res of
        (Entity _ test, user, Entity _ blob, upload):_ ->
            return (test, user, blob, upload)
        _ -> lift notFound

-- | Return the types of the output files for the given test
getOutputType :: MonadHandler m => TestId -> ReaderT SqlBackend m [FileType]
getOutputType testid = do
    res <- select $ from $ \(autotest `InnerJoin` output) -> do
        on (autotest ^. AutoTestId ==. output ^. AutoTestOutputTypeTest)
        where_ $ autotest ^. AutoTestTest ==. val testid
        return (output ^. AutoTestOutputTypeFileType)
    return $ map (\(Value a) -> a) res

getXPaths :: MonadHandler m => TestId -> ReaderT SqlBackend m [AutoTestXPath]
getXPaths testid = do
    res <- select $ from $ \(autotest `InnerJoin` use `InnerJoin` xpath) -> do
        on (use ^. AutoTestXPathUseXpath ==. xpath ^. AutoTestXPathId)
        on (autotest ^. AutoTestId ==. use ^. AutoTestXPathUseTest)
        where_ $ autotest ^. AutoTestTest ==. val testid
        return xpath
    return $ map (\(Entity _ a) -> a) res

getTestR :: TestId -> Handler Html
getTestR testid = do
    (test, Entity userid user, blob, Entity uploadid upload, outputs, xpaths) <- runDB $ do
        (test, user, blob, upload) <- getAutoTest testid
        outputs <- getOutputType testid
        xpaths <- getXPaths testid
        return (test, user, blob, upload, outputs, xpaths)
    let xml = decodeUtf8 $ blobContent blob
        html = xmlToHtml xml
    let trv = TestResultsView 0 20 [] [] [TestResultsWidgetFilterTest testid]
    testResultsWidget <- getTestResultsWidget False trv
    resultTableWidget <- getTestResultTableWidget testid
    defaultLayout $ do
        setTitle "Test"
        $(widgetFile "test")

getTestsR :: TestsView -> Handler Html
getTestsR testsview = do
    let offset' = testsViewOffset testsview
        count = testsViewLimit testsview
    when (offset' < 0 || count < 1 || count > 100) $ invalidArgs []
    testswidget <- getTestsWidget True testsview
    defaultLayout $ do
        setTitle "Tests"
        $(widgetFile "tests")
