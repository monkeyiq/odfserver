module Handler.Upload (
    getUploadGetR,
    getUploadR,
    getUploadsR, postUploadsR,
    uploadWidget
) where

import           Control.Applicative as A
import           Data.Conduit.Binary (sinkLbs)
import           Database.Esqueleto
import           Import              hiding (on, (==.))

import           CommentWidgets      (discussionWidget)
import qualified Database            as D
import           FileType

data File = File {
    file :: FileInfo
}

uploadForm :: Form File
uploadForm = renderDivs $ File
    A.<$> fileAFormReq ""

getUploadsR :: Handler Html
getUploadsR = do
    (widget, enctype) <- generateFormPost uploadForm
    uploads <- runDB $ select $ from $
        \(upload `InnerJoin` blob `InnerJoin` user) -> do
            on $ upload ^. UploadUploader ==. user ^. UserId
            on $ upload ^. UploadContent  ==. blob ^. BlobId
--                E.orderBy [E.desc (request ^. RequestTime)]
            offset 0
            limit 1000
            return
                ( upload
                , blob ^. BlobSize
                , user ^. UserName
                )

    defaultLayout $ do
        setTitleI MsgUploadedFiles
        $(widgetFile "uploads")

postUploadsR :: Handler Html
postUploadsR = do
    userid <- requireAuthId
    ((result, _), _) <- runFormPost uploadForm
    case result of
        FormSuccess (File{file}) -> do
            let name = fileName file
            fileBytes <- runResourceT $ fileSource file $$ sinkLbs
            ctime <- lift getCurrentTime
            uploadid <- runDB $ D.addUpload fileBytes userid name ctime
            redirect $ UploadR uploadid
        _ -> do
            setMessageI MsgSomethingWentWrong
            redirect UploadsR

listConversions :: UploadId -> Handler Widget
listConversions uploadid = do
    -- find all possible conversions that can be run on this upload
    conversions <- runDB
        $ select
        $ from $ \(upload `InnerJoin` blob `InnerJoin` service `InnerJoin` serviceinstance `InnerJoin` software) -> do
             on $ serviceinstance ^. ServiceInstanceSoftware ==. software ^. SoftwareOSId
             on $ service ^. ServiceId ==. serviceinstance ^. ServiceInstanceService
             on $ blob ^. BlobFileType ==. service ^. ServiceInputType
             on $ upload ^. UploadContent  ==. blob ^. BlobId
             where_ $ upload ^. UploadId ==. val uploadid
             offset 0
             limit 1000
             return
                 ( service ^. ServiceName
                 , service ^. ServiceOutputType
                 , service ^. ServiceId
                 , software ^. SoftwareOSId
                 )
    return $(widgetFile "file_conversions")

-- | Widget that shows an upload without discussion thread.
uploadWidget :: UploadId -> Widget
uploadWidget uploadid = do
    (upload, blob, user) <- handlerToWidget $ runDB $ do
        upload <- get404 uploadid
        blob <- get404 $ uploadContent upload
        user <-get404 $ uploadUploader upload
        return (upload, blob, user)
    -- list possible conversions
    conversionWidget <- handlerToWidget $ listConversions uploadid
    let filename = uploadFilename upload
        mimetype = uploadMimetype upload
    $(widgetFile "file_upload")

getUploadR :: UploadId -> Handler Html
getUploadR uploadid = do
    upload <- runDB $ get404 uploadid
    defaultLayout $ do
        uploadWidget uploadid
        discussionWidget (uploadDescription upload) Nothing

getUploadGetR :: UploadId -> Text -> Handler TypedContent
getUploadGetR uploadid filename = do
    (Upload f blobid _ _ mimetype _) <- runDB $ get404 uploadid
    (Blob _ content _ _) <- runDB $ get404 blobid
    if f == filename
        then return $ TypedContent (encodeUtf8 mimetype) $ toContent content
        else notFound
