module Handler.Page (
    getPageEditR, postPageEditR,
    getPageHistoryR,
    getPageNewR, postPageNewR,
    getPageR,
    getPageLongR,
    getPagesR,
    getProseHistoryR,
    getProseR,
    pageWidget
) where

import           Database.Esqueleto hiding (isNothing, update, (=.))
import           Import             hiding (Value, count, formatTime, groupBy,
                                     on, (==.), (>.), (||.))

import           CommentWidgets     (discussionWidget)
import           Prose
import           TimeUtil

-- | Widget that shows a page without discussion thread.
pageWidget :: Maybe PageId -> User -> Bool -> Prose -> Widget
pageWidget mpageid author mayEdit prose = $(widgetFile "page")

prosePage :: Maybe PageId -> ProseId -> Prose -> Bool -> Handler Html
prosePage mpageid proseid prose mayEdit = do
    let title = proseTitle prose
        authorid = proseAuthor prose
    author <- runDB $ get404 authorid
    defaultLayout $ do
        setTitle $ toHtml title
        pageWidget mpageid author mayEdit prose
        discussionWidget proseid Nothing

getProseAndId :: PageId -> Handler (ProseId, Prose)
getProseAndId pageid = runDB $ do
    page <- get404 pageid
    prose <- get404 (pageCurrent page)
    return (pageCurrent page, prose)

getPageR :: PageId -> Handler Html
getPageR pageid = do
    maybeUserId <- maybeAuthId
    (proseid, prose) <- getProseAndId pageid
    let mayEdit = maybeUserId == Just (proseAuthor prose)
    prosePage (Just pageid) proseid prose mayEdit

getPageLongR :: PageId -> Text -> Handler Html
getPageLongR pageid title = do
    (_, prose) <- getProseAndId pageid
    when (title /= proseTitle prose) $
        redirect $ PageLongR pageid $ proseTitle prose
    getPageR pageid


getProseR :: ProseId -> Handler Html
getProseR proseid = do
    prose <- runDB $ get404 proseid
    prosePage Nothing proseid prose False

getPageHistoryR :: PageId -> Handler Html
getPageHistoryR pageid = do
    page <- runDB $ get404 pageid
    getProseHistoryR $ pageCurrent page

getProseHistoryR :: ProseId -> Handler Html
getProseHistoryR proseid = do
    (title, versions) <- runDB $ do
        prose <- get404 proseid
        let firstid = fromMaybe proseid $ proseFirstVersion prose
        versions <- select $ from $ \p -> do
            where_ $ p ^. ProseId ==. val firstid
                     ||. p ^. ProseFirstVersion ==. val (Just firstid)
            orderBy [desc (p ^. ProseCreated)]
            return
                ( p ^. ProseId
                , p ^. ProseTitle
                , p ^. ProseCreated
                )
        return (proseTitle prose, versions)
    defaultLayout $ do
        setTitleI $ MsgHistoryFor title
        $(widgetFile "page_history")

-- get highest proseid for a page by the given user
getHighestUserPageProseId :: MonadIO m => UserId -> ReaderT SqlBackend m (Maybe ProseId)
getHighestUserPageProseId userid = do
    res <- select $ from $ \(page `InnerJoin` prose) -> do
        on $ page ^. PageCurrent ==. prose ^. ProseId
        where_ (prose ^. ProseAuthor ==. val userid)
        orderBy [desc (page ^. PageId)]
        limit 1
        return (prose ^. ProseId)
    case res of
        Value proseid:_ -> return $ Just proseid
        _ -> return Nothing

-- get pageid and proseid that follows the given proseid for a page by the given
-- user
getNextUserPageId :: MonadIO m => UserId -> Maybe ProseId -> ReaderT SqlBackend m (Maybe (PageId, ProseId))
getNextUserPageId userid mprevproseid = do
    res <- select $ from $ \(page `InnerJoin` prose) -> do
        on $ page ^. PageCurrent ==. prose ^. ProseId
        let userClause = prose ^. ProseAuthor ==. val userid
        where_ $ case mprevproseid of
                     Just prev -> userClause &&. prose ^. ProseId >. val prev
                     Nothing -> userClause
        orderBy [asc (page ^. PageId)]
        limit 1
        return (page ^. PageId, prose ^. ProseId)
    case res of
        (Value pageid, Value proseid):_ -> return $ Just (pageid, proseid)
        _ -> return Nothing

getPageNewR :: Handler Html
getPageNewR = do
    userid <- requireAuthId
    -- use previous prose id for a page by this user as a marker to avoid
    -- creating multiple pages when the form is posted more than once
    maybePrevious <- runDB $ getHighestUserPageProseId userid
    let widget = proseFormWidget FormProse
                                 { formProseTitle = ""
                                 , formProseContent = Textarea ""
                                 , formProseParent = Nothing
                                 , formProseReference = maybePrevious }
    defaultLayout $ do
        setTitleI MsgWriteANewPage
        $(widgetFile "widget-wrap")

postPageNewR :: Handler Html
postPageNewR = do
    (userid, user, result, widget', enctype, isSave) <- runProseForm
    case result of
        FormSuccess entry -> do
            newProse <- createProse userid entry
            case isSave of
                Just True -> do
                    let prev = formProseReference entry
                    next <- runDB $ getNextUserPageId userid prev
                    pageid <- case next of
                        Just (pageid, proseid) -> do
                            updated <- runDB $ do
                                (oldProse, nextProse) <-
                                    deriveProse proseid userid entry
                                updatePage pageid oldProse nextProse
                            when updated $ setMessageI MsgPageWasUpdated
                            return pageid
                        Nothing -> runDB $ do
                            textid <- insert newProse
                            insert $ Page textid
                    redirect $ PageR pageid
                _ -> defaultLayout $ do
                    setTitleI MsgWriteANewPage
                    pageWidget Nothing user False newProse
                    let widget = wrapProseFormWidget enctype True widget'
                    $(widgetFile "widget-wrap")
        _ -> do
            setMessageI MsgSomethingWentWrong
            redirect PageNewR

getPageEditR :: PageId -> Handler Html
getPageEditR pageid = do
    _ <- requireAuthId
    (proseid, prose) <- getProseAndId pageid
    let formProse = FormProse
                    { formProseTitle = proseTitle prose
                    , formProseContent = Textarea $ proseContent prose
                    , formProseParent = Nothing
                    , formProseReference = Just proseid }
    let widget = proseFormWidget formProse
    defaultLayout $ do
        setTitleI MsgModifyAPage
        $(widgetFile "widget-wrap")

updatePage :: MonadIO m => PageId -> Prose -> Prose -> ReaderT SqlBackend m Bool
updatePage pageid oldProse newProse = do
    -- only save if something changed
    let updated = proseTitle oldProse /= proseTitle newProse
         || proseContent oldProse /= proseContent newProse
    when updated $ do
        newProseId <- insert newProse
        update pageid [PageCurrent =. newProseId]
    return updated

postPageEditR :: PageId -> Handler Html
postPageEditR pageid = do
    (userid, author, result, widget', enctype, _) <- runProseForm
    isSave <- runInputPost $ iopt boolField "save"
    case result of
        FormSuccess entry -> do
            (proseid, prose, newProse) <- runDB $ do
                page <- get404 pageid
                let proseid = pageCurrent page
                (prose, newProse) <- deriveProse proseid userid entry
                return (proseid, prose, newProse)
            when (Just proseid /= formProseReference entry) $
                redirect $ PageEditR pageid
            case isSave of
                Just True -> do
                    updated <- runDB $ updatePage pageid prose newProse
                    when updated $ setMessageI MsgPageWasUpdated
                    redirect $ PageR pageid
                _ -> defaultLayout $ do
                    pageWidget (Just pageid) author False newProse
                    let widget = wrapProseFormWidget enctype True widget'
                    $(widgetFile "widget-wrap")
        _ -> do
            setMessageI MsgSomethingWentWrong
            redirect PageNewR

getPages :: Handler [(Value PageId, Value Text, Value UTCTime, Value Text, Value Int)]
getPages = runDB $ select $ from $
    \(page `InnerJoin` prose `InnerJoin` user `LeftOuterJoin` comment) -> do
        on $ prose ^. ProseId ==. comment ^. CommentRoot
        on $ prose ^. ProseAuthor ==. user ^. UserId
        on $ page ^. PageCurrent  ==. prose ^. ProseId
        groupBy (prose ^. ProseId, page ^. PageId, user ^. UserId)
        orderBy [desc (prose ^. ProseCreated)]
        offset 0
        limit 1000
        return
            ( page ^. PageId
            , prose ^. ProseTitle
            , prose ^. ProseCreated
            , user ^. UserName
            , count $ comment ^. CommentRoot
            )

getPagesR :: Handler Html
getPagesR = do
    pages <- getPages
    defaultLayout $ do
        setTitle "Pages"
        $(widgetFile "pages")
