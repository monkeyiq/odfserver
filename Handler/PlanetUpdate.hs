module Handler.PlanetUpdate (
    getPlanetUpdateR
) where

import           Control.Concurrent.Async (mapConcurrently)
import           Data.ByteString.Lazy     (ByteString)
import           Data.Text                (strip)
import           Data.Time.Format         (parseTimeM, rfc822DateFormat)
import           Import                   hiding (ByteString, Element, httpLbs)
import           Network.HTTP.Client
import qualified Text.Atom.Feed           as A
import           Text.Feed.Import
import           Text.Feed.Query
import qualified Text.Feed.Types          as F
import           Text.RSS.Syntax
import           Text.XML.Light.Output

import           Database
import           HtmlFilter
import qualified MimeType                 as M

feedurls :: [Text]
feedurls = [
   "http://feeds2.feedburner.com/robweir/antic-atom",
   "https://blogs.apache.org/OOo/feed/entries/atom",
   "http://standardsandfreedom.net/index.php/feed/",
   "http://www.computerworlduk.com/blogs/rss",
   "http://www.docxpresso.com/rss.xml",
   "https://joinup.ec.europa.eu/news/all/feed",
   "http://www.blogger.com/feeds/9361273/posts/default",
   "http://news.google.com/news?pz=1&ned=us&hl=en&q=odf+format&output=rss",
   "http://googleforwork.blogspot.nl/",
--   "https://news.ycombinator.com/rss",
--   "https://www.kickstarter.com/projects/feed.atom",
   "https://blogs.office.com/?feed=rss2",
   "https://www.oasis-open.org/feed",
   "https://www.oasis-open.org/news/feed",
   "https://groups.diigo.com/group/open_document-format/rss",
   "http://recipes.opendocsociety.org/recipes/;rss",
   "http://odf-converter.sourceforge.net/newblog/rss.php",
   "https://en.wikipedia.org/w/index.php?title=OpenDocument&feed=atom&action=history",
   "http://planet.documentfoundation.org/atom.xml",
   "http://www.vandenoever.info/index.rss",
   "http://stackoverflow.com/feeds/tag/odf"
   ]

addFeedToDb :: MonadIO m => Text -> ReaderT SqlBackend m (Maybe (Key Feed))
addFeedToDb url = insertUnique $ Feed url "" Nothing True

addFeedsToDb :: Handler ()
addFeedsToDb = runDB $ do
    _ <- mapM addFeedToDb feedurls
    return ()

ignoreHttpException :: (MonadIO m, MonadBaseControl IO m) => a -> m a -> m a
ignoreHttpException val = handle (\(_ :: HttpException) -> return val)

retrieveFeed :: Manager -> Entity Feed -> IO (FeedId, Feed, ByteString)
retrieveFeed httpManager (Entity key val) = ignoreHttpException (key, val, "") $ do
    request <- parseUrl $ unpack $ feedUrl val
    response <- httpLbs request httpManager
    return (key, val, responseBody response)

updateFeed :: (FeedId, Feed, ByteString) -> Handler (FeedId, Maybe String)
updateFeed (feedId, feed, bytes) = do
    now <- liftIO getCurrentTime
    let mfeed = parseFeedSource $ decodeUtf8 bytes
        (newFeed, rssImage, items) = case mfeed of
                    Nothing -> (feed, Nothing, [])
                    Just f -> convertFeed now feedId feed f
    _ <- runDB $ do
        repsert feedId newFeed
        mapM (\i -> upsert i []) items
    return (feedId, rssImage)

updateFeedImage :: Manager -> (FeedId, URLString) -> Handler ()
updateFeedImage manager (feedId, imageURL) = ignoreHttpException () $ do
    request <- parseUrl $ unpack imageURL
    response <- lift $ httpLbs request manager
    let bytes = responseBody response
        (M.FileInfo _ filetype) = M.getFileInfo $ responseBody response
    runDB $ do
        feed <- get404 feedId
        blobid <- addBlob bytes filetype
        repsert feedId $ feed {feedIcon = Just blobid}
        return ()
    return ()

updateFeeds :: Handler ()
updateFeeds = do
    feeds <- runDB $ selectList [] []
    App{appHttpManager} <- getYesod
    feedBodies <- lift $ mapConcurrently (retrieveFeed appHttpManager) feeds
    mimages <- mapM updateFeed feedBodies
    let images = mapMaybe withImage mimages
    _ <- mapM (updateFeedImage appHttpManager) images
    return ()
    where withImage (a, Just b) = Just (a, b)
          withImage (_, Nothing) = Nothing

getPlanetUpdateR :: Handler ()
getPlanetUpdateR = do
    addFeedsToDb
    updateFeeds
    redirect PlanetR

maybeListToMaybe :: [Maybe a] -> Maybe a
maybeListToMaybe = listToMaybe . catMaybes

convertFeed :: UTCTime -> FeedId -> Feed -> F.Feed -> (Feed, Maybe URLString, [FeedItem])
convertFeed _ feedid feed ffeed = (newFeed, image, catMaybes mitems)
    where
        feedDates = map getDate [getFeedPubDate, getFeedDate]
        getDate getter = getter ffeed >>= parseRSSDate
        mfeedDate = maybeListToMaybe feedDates
        newFeed = feed {feedName = pack $ getFeedTitle ffeed}
        image = getFeedLogoLink ffeed
        mitems :: [Maybe FeedItem]
        mitems = map (parseItem' feedid mfeedDate) $ feedItems ffeed

parseItem' :: FeedId -> Maybe UTCTime -> F.Item -> Maybe FeedItem
parseItem' feedid mfeedDate item = do
    itemDate <- parseDate' mfeedDate item
    itemTitle <- getItemTitle item
    itemBody <- parseBody' item
    itemUrl <- getItemLink item
    let mauthor = parseAuthor item
    return FeedItem {
        feedItemFeed = feedid,
        feedItemPubDate = itemDate,
        feedItemTitle = pack itemTitle,
        feedItemBody = rssFilter $ pack itemBody,
        feedItemUrl = pack itemUrl,
        feedItemAuthor = pack <$> mauthor,
        feedItemShow = False
    }

parseAuthor :: F.Item -> Maybe String
parseAuthor item = maybeListToMaybe [
      getItemAuthor item
    , atom item
    ]
    where
        atom (F.AtomItem (A.Entry{A.entryAuthors,A.entrySource})) =
            persons (entryAuthors ++ sa)
            where sa = case entrySource of
                           Nothing -> []
                           Just source -> A.sourceAuthors source
        atom _ = Nothing
        persons l = listToMaybe $ map A.personName l

parseDate' :: Maybe UTCTime -> F.Item -> Maybe UTCTime
parseDate' mfeedDate item = case getItemPublishDate item of
    Just (Just d) -> Just d
    Just Nothing ->
        case getItemPublishDateString item of
            Nothing -> mfeedDate
            Just s -> parseRSSDate s
    Nothing -> Nothing

nonEmpty :: Maybe String -> Maybe String
nonEmpty (Just a) | strip (pack a) == "" = Nothing
nonEmpty (Just a) = Just a
nonEmpty _ = Nothing

parseBody' :: F.Item -> Maybe String
parseBody' item = maybeListToMaybe [
       nonEmpty $ getItemSummary item
     , nonEmpty $ getItemDescription item
     , atom item
    ]
    where
        atom (F.AtomItem (A.Entry{A.entryContent})) = atomContent entryContent
        atom _ = Nothing
        atomContent (Just (A.XHTMLContent element)) = Just $ showElement element
        atomContent (Just (A.TextContent content)) = Just content
        atomContent (Just (A.HTMLContent content)) = Just content
        atomContent _ = Nothing

parseRSSDate :: String -> Maybe UTCTime
parseRSSDate = parseAll
 where parse input format = parseTimeM True defaultTimeLocale format input
       parseAll str = listToMaybe $ mapMaybe (parse str) fmts
       fmts = ["%-d-%-m-%Y %l:%M %p", rfc822DateFormat, "%Y-%m-%d"]
