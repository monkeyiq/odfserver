module TestTools (
    getLocation,
    login,
    printHeaders,
    getTailInteger
) where

import           Data.ByteString
import           Network.HTTP.Types (Status (..))
import           Network.Wai.Test   (SResponse (..))
import           TestImport         hiding (Job)
import           Text.Read          (read)
import           Text.Regex.Posix
import           Yesod.Test         (YesodExample)

login :: Text -> Text -> YesodExample App ()
login user pass =
    request $ do
        setMethod "POST"
        setUrl ("/auth/page/email/login" :: Text)
        addPostParam "email" user
        addPostParam "password" pass

getTailInteger :: String -> Maybe Int64
getTailInteger text = val
    where pat = "[^0-9]([0-9]+)$" :: String
          m = text =~ pat :: (String, String, String, [String])
          val = case m of
                    (_, _, _, [v]) -> Just $ read v
                    _ -> Nothing

printHeaders :: Yesod.Test.YesodExample site ()
printHeaders = withResponse
    ( \ SResponse { simpleHeaders = h } ->
        lift $ print h
    )

getLocation :: YesodExample App ByteString
getLocation = do
    (Just maybeLocation) <- extractLocation
    return maybeLocation

extractLocation :: YesodExample App (Maybe ByteString)
extractLocation = withResponse
    ( \ SResponse { simpleStatus = s, simpleHeaders = h } -> do
        let code = statusCode s
        assertEqual ("Expected a 302 or 303 redirection status "
                     ++ "but received " ++ show code)
                    (code `oelem` [302,303])
                    True
        return $ lookup "Location" h
    )
