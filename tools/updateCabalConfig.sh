#!/usr/bin/env bash

# cabal.config can contain very specific versions for packages.
# When this file is available, cabal does not need to do an extensive search
# in package versions.
# This script updates cabal.config
# First it removes the current cabal.config. Then it creates a new one with
# 'cabal freeze'
# And then some very common packages are removed.

if [ ! -e odftestserver.cabal ]; then
    echo "cabal file is missing"
    exit 1
fi

rm cabal.config
cabal freeze

grep -v '^ \+\(array\|base\|containers\|deepseq\|ghc-prim\|integer-gmp\|pretty\|template-haskell\) ' cabal.config > cabal.config.tmp
mv cabal.config.tmp cabal.config
