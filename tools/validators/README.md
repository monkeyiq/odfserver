# Build the jar file
```bash
ant
```

# Run the validator factory

```bash
java -jar odfvalidationfactory.jar
```


For the factories, things will be simplifed soon but for right now this is the process.
You can detect a specific office suite using the configure mode as follows. The -h tells
what server to talk with and -d 1 turns off SSL certificate verification if you are using
self signed certs.


```bash
java -jar officesuiteconfigure.jar -c calligra.properties  -s Calligra -h https://localhost:3443 -d 1
```

If you want to see the possibilities for the -s option run

```bash
java -jar officesuitelistpossiblesuites.jar
```

To run this new factory all you need to know is the name of the generated config (from -c) above.

```bash
java -jar officesuitefactory.jar --config calligra.properties
```

The console is left with messages to give an impression of what is going down.


In the future this will be simplified to two commands; a single "configure all"
and a single "run all" that will detect everything and run all the detected servers.


