#!/usr/bin/env bash

SCRIPTDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

CP="$SCRIPTDIR/bin:$CLASSPATH"
for fi in $SCRIPTDIR/lib/*jar; do
    CP="$CP:$fi"
done

java -cp "$CP" org.odfserver.odfvalidator.OdfValidator "$@"
