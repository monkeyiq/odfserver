
package org.odfserver;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.time.Instant;
import java.util.Properties;
import java.util.LinkedList;
import java.util.List;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonStructure;
import javax.json.JsonWriter;
import javax.json.JsonObjectBuilder;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.io.IOUtils;
import org.w3c.dom.Document;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;

public abstract class Factory {
	protected Properties m_config;
	protected String m_serverHostAndPort;
	protected boolean m_disableEncryption;

	protected int m_nextNumber;
	protected String m_detectedSoftwareVersion;
	private JsonObject m_json_software;

	public Factory() {
		m_nextNumber = 1;
		m_detectedSoftwareVersion = "0";
		m_serverHostAndPort = "https://localhost:3443";
		m_disableEncryption = false;
		m_config = new Properties();
	}

	public abstract String configPath();

	public void setServerHostAndPort(String v) {
		m_serverHostAndPort = v;
	}

	public void setDisableEncryption(boolean v) {
		m_disableEncryption = v;
		if (m_disableEncryption)
			SslTweaks.disableSslVerification();
	}

	static DOMImplementationRegistry domRegistry = getRegistry();

	static public final DOMImplementationRegistry getRegistry() {
		DOMImplementationRegistry d = null;
		try {
			d = DOMImplementationRegistry.newInstance();
		} catch (Exception e) {
			throw new Error(e);
		}
		return d;
	}

	static public final void serialize(Document xml, Writer out) {
		DOMImplementationLS impl = (DOMImplementationLS) domRegistry
				.getDOMImplementation("LS");
		LSSerializer serializer = impl.createLSSerializer();
		LSOutput lso = impl.createLSOutput();
		lso.setCharacterStream(out);
		serializer.write(xml, lso);
	}

	static public final void prettyPrint(Document xml) {
		Writer out = new StringWriter();
		serialize(xml, out);
		System.out.println(out.toString());
	}

	static public final String toBase64(byte[] s) throws IOException {
		return DatatypeConverter.printBase64Binary(s);
	}

	static public final String toBase64(Document xml) throws IOException {
		ByteArrayOutputStream bytearray = new ByteArrayOutputStream();
		OutputStreamWriter out = new OutputStreamWriter(bytearray, "UTF-8");
		serialize(xml, out);
		return toBase64(bytearray.toByteArray());
	}

	static public final String digestPath(String path) throws Exception {
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		FileInputStream fis = new FileInputStream(path);
		byte[] dataBytes = new byte[1024];
		int nread = 0;
		while ((nread = fis.read(dataBytes)) != -1) {
			md.update(dataBytes, 0, nread);
		}
		;
		fis.close();
		byte[] bytes = md.digest();

		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < bytes.length; i++) {
			sb.append(Integer.toHexString(0xFF & bytes[i]));
		}
		return sb.toString();
	}

	static public String jsonToString(JsonStructure value)
			throws UnsupportedEncodingException {
		ByteArrayOutputStream jsonbytes = new ByteArrayOutputStream();
		JsonWriter writer = Json.createWriter(jsonbytes);
		writer.write(value);
		return (jsonbytes.toString("UTF-8"));
	}

	static String convertStreamToString(java.io.InputStream is) {
		java.util.Scanner s1 = new java.util.Scanner(is);
		try {
			java.util.Scanner s2 = s1.useDelimiter("\\A");
			return s2.hasNext() ? s2.next() : "";
		} finally {
			s1.close();
		}
	}

	public static String getOSName() {
		return "Fedora Linux";
	}

	public static String getOSVersion() {
		return "22";
	}

	public static String getOSPlatform() {
		return "x86_64";
	}

	static public JsonObject createJsonsoftwareObject(String softwareFamily,
			String softwareName, String softwareVersion) {
		JsonObject ret = Json.createObjectBuilder()
				.add("softwareFamily", softwareFamily)
				.add("softwareName", softwareName)
				.add("softwareVersion", softwareVersion)
				.add("osName", getOSName()).add("osVersion", getOSVersion())
				.add("platform", getOSPlatform()).build();

		return ret;
	}

	public void registerSoftware(String softwareFamily, String softwareName,
			String softwareVersion) {
		m_json_software = createJsonsoftwareObject(softwareFamily, softwareName,
				softwareVersion);
	}

	List<JsonObject> m_services = new LinkedList<JsonObject>();

	public JsonObject registerService(String name, String inType,
			String outType) {
		JsonObject jo = Json.createObjectBuilder().add("name", name)
				.add("inputType", inType).add("outputType", outType).build();
		m_services.add(jo);
		return jo;
	}

	public JsonObject createRetrieveJobsRequest() {
		JsonObjectBuilder sb = Json.createObjectBuilder();
		sb.add("software", m_json_software);
		for (JsonObject srv : m_services) {
			sb.add("service", srv);
		}

		JsonObject request = Json.createObjectBuilder()
				.add("factoryKey", getFactoryKey()).add("maxWaitTime", 0)
				.add("nextNumber", m_nextNumber)
				.add("serviceInstances", Json.createArrayBuilder().add(sb))
				.build();

		return request;
	}

	public JsonObject postRetrieveJobs() throws Exception {
		JsonObject reply = (JsonObject) post("/factory/retrievejobs",
				createRetrieveJobsRequest());
		return reply;
	}

	public JsonStructure post(String earlEnding, JsonStructure request)
			throws Exception {
		String req = jsonToString(request);

		URL u = new URL(m_serverHostAndPort + "/" + earlEnding);
		System.out.println("u: " + u);
		HttpURLConnection conn = (HttpURLConnection) u.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "application/json");
		conn.setRequestProperty("Accept", "application/json");
		System.out.println("u2: " + u);
		{
			OutputStream oss = conn.getOutputStream();
			System.out.println("sending: " + req);
			oss.write(req.getBytes());
		}

		System.out.println("u3: " + u);
		InputStream iss = conn.getInputStream();

		// String response = convertStreamToString( iss );
		// System.out.println( "response: " + response );

		JsonReader rdr = Json.createReader(iss);
		JsonStructure response = rdr.read();
		System.out.println("post() response: " + jsonToString(response));
		return response;
	}

	public void configWrite() throws Exception {
		OutputStream output = new FileOutputStream(configPath());
		m_config.setProperty("serverHostAndPort", m_serverHostAndPort);
		m_config.setProperty("disableEncryption",
				String.valueOf(m_disableEncryption));
		m_config.store(output, null);
	}

	public void configRead() throws Exception {
		try {
			FileInputStream iss = new FileInputStream(configPath());
			m_config.load(iss);

			if (m_config.containsKey("disableEncryption"))
				setDisableEncryption(
						m_config.getProperty("disableEncryption", "false")
								.equals("true"));
			if (m_config.containsKey("serverHostAndPort"))
				setServerHostAndPort(
						m_config.getProperty("serverHostAndPort", ""));
		} catch (java.io.FileNotFoundException e) {
			System.out.println("Warning, config file not found...");
		}

	}

	public void ensureRegistered() throws Exception {
		ensureRegistered(false);
	}

	public void ensureRegistered(boolean force) throws Exception {
		if (!force) {
			if (m_config.getProperty("factoryKey", "") != ""
					&& m_config.getProperty("factoryId", "") != "") {
				System.out.println(
						"Already have factoryKey and factoryId so reusing them...");
				return;
			}
		}

		JsonStructure request = Json.createArrayBuilder().build();
		JsonObject reply = (JsonObject) post("/factory/register", request);

		m_config.setProperty("factoryKey", reply.getString("factoryKey"));
		m_config.setProperty("factoryId", reply.getString("factoryId"));
		configWrite();
	}

	public static void writeFile(String path, byte[] data)
			throws IOException, FileNotFoundException {
		FileOutputStream fos = new FileOutputStream(path);
		fos.write(data);
		fos.close();
	}

	public static byte[] readFile(String path)
			throws IOException, FileNotFoundException {
		FileInputStream iss = new FileInputStream(path);
		byte[] ret = IOUtils.toByteArray(iss);
		iss.close();
		return ret;
	}

	public JsonStructure uploadjobresult(String base64Content, int number,
			Instant started, Instant finished, int exitCode, String stdout,
			String stderr) throws IOException, Exception {
		int maxStdOutSize = 1024;

		JsonObject request = Json.createObjectBuilder()
				.add("factory", Json.createObjectBuilder()
						.add("factoryKey", m_config.getProperty("factoryKey"))
						.add("factoryId", m_config.getProperty("factoryId")))
				.add("number", number).add("started", started.toString())
				.add("finished", finished.toString()).add("exitCode", exitCode)
				.add("stdout",
						stdout.substring(0,
								Math.min(stdout.length(), maxStdOutSize)))
				.add("stderr",
						stderr.substring(0,
								Math.min(stderr.length(), maxStdOutSize)))
				.add("outputFile", base64Content).build();
		JsonStructure reply = (JsonStructure) post("/factory/uploadjobresult",
				request);
		return reply;
	}

	public JsonStructure uploadjobresult(String base64Content, int number,
			Instant started, Instant finished, int exitCode)
					throws IOException, Exception {
		return uploadjobresult(base64Content, number, started, finished,
				exitCode, "", "");
	}

	public JsonStructure uploadjobresult(Document doc, int number,
			Instant started, Instant finished, int exitCode)
					throws IOException, Exception {
		String fileBase64 = toBase64(doc);
		return uploadjobresult(fileBase64, number, started, finished, exitCode);
	}

	public JsonStructure getFactoryKey() {
		return (Json.createObjectBuilder()
				.add("factoryKey", m_config.getProperty("factoryKey"))
				.add("factoryId", m_config.getProperty("factoryId")).build());
	}

	protected String getInputODFVersion(JsonObject job)
			throws UnsupportedEncodingException {
		String ret = "";
		System.out.println("job: " + jsonToString(job));
		JsonObject service = job.getJsonObject("service");
		if (service != null) {
			System.out.println("service: " + jsonToString(service));
			JsonObject conversion = service.getJsonObject("service");
			if (conversion != null) {
				System.out.println("conversion: " + jsonToString(conversion));
				String s = conversion.getString("inputType");
				System.out.println("s: " + s);
				if (s.equals("ODT1_0"))
					ret = "1.0";
				else if (s.equals("ODT1_1"))
					ret = "1.1";
				else if (s.equals("ODT1_2"))
					ret = "1.2";
				else if (s.equals("ODT1_2EXT"))
					ret = "1.2 ext";
			}
		}
		return ret;
	}

};
