
package org.odfserver.officesuitefactory;

import java.io.UnsupportedEncodingException;
import java.time.Instant;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import org.odfserver.Factory;

public class AppGoogleDocs extends AppBase {
	String m_desiredODFVersion;

	JsonObject m_json_software;
	JsonObject m_json_service_10;
	JsonObject m_json_service_11;
	JsonObject m_json_service_12;
	JsonObject m_json_service_12ext;
	String m_json_service_10_string;
	String m_json_service_11_string;
	String m_json_service_12_string;
	String m_json_service_12ext_string;

	public AppGoogleDocs() throws UnsupportedEncodingException {
		m_detectedSoftwareVersion = "0.0";
		setupJsonStrings();

	}

	public void loadConfig(Properties p) throws UnsupportedEncodingException {
		super.loadConfig(p);
		setupJsonStrings();
	}

	private void setupJsonStrings() throws UnsupportedEncodingException {
        m_json_software = Factory.createJsonsoftwareObject( "GoogleDocs", "GoogleDocs", m_detectedSoftwareVersion );

		String n = getName();
		m_json_service_10 = Json.createObjectBuilder()
				.add("outputType", "ODT1_2EXT").add("name", n)
				.add("inputType", "ODT1_0").build();

		m_json_service_11 = Json.createObjectBuilder()
				.add("outputType", "ODT1_2EXT").add("name", n)
				.add("inputType", "ODT1_1").build();

		m_json_service_12 = Json.createObjectBuilder()
				.add("outputType", "ODT1_2EXT").add("name", n)
				.add("inputType", "ODT1_2").build();

		m_json_service_12ext = Json.createObjectBuilder()
				.add("outputType", "ODT1_2EXT").add("name", n)
				.add("inputType", "ODT1_2EXT").build();

		m_json_service_10_string = jsonToString(m_json_service_10);
		m_json_service_11_string = jsonToString(m_json_service_11);
		m_json_service_12_string = jsonToString(m_json_service_12);
		m_json_service_12ext_string = jsonToString(m_json_service_12ext);
	}

	public String getName() {
		return "GoogleDocs";
	}

	public String getExecutableName() {
		return "drive-linux-x64";
	}

	public void sniff(Properties p) throws Exception {
		try {
			String appPath = getExecutableFullPath();
			sniffCheckHaveAppPathOrDie(p, appPath);
			System.out.println("sniff() appPath: " + appPath);

			p.setProperty("appPath", appPath);
			p.setProperty("detection-time",
					String.valueOf(Instant.now().getEpochSecond()));

			m_detectedSoftwareVersion = "0.1";

			/*
			 * run it to see the version
			 */
			String ver = executeProcessToRegexCapture(
					new ProcessBuilder(appPath, "--version").start(),
					Pattern.compile(".*gdrive v[ ]*(?<selected>[0-9.]+).*"));
			sniffCheckHaveVersionOrDie(p, ver, appPath);

			String quota = executeProcessToRegexCapture(
					new ProcessBuilder(appPath, "quota").start(),
					Pattern.compile(".*Total:[ ]*(?<selected>[0-9.]+) .*"));
			System.out.println("found available quota: " + quota);
			if (quota == null || quota.equals("")) {
				p.setProperty("enabled", "false");
				String emsg = "Can't find the quota of used storage. This normally means you have not authenticated with GDrive properly.for executable at: "
						+ appPath;
				p.setProperty("detection-error", emsg);
				System.err.println(emsg);
				throw new Exception(emsg);
			}

			runSanityCheckConversion(p);
			p.setProperty("enabled", "true");
		} catch (Exception e) {
			p.setProperty("enabled", "false");
			p.setProperty("detection-error", e.toString());
			System.err.println(e.toString());
			throw e;
		}
	}

	public JsonObject getJsonSoftware() {
		return m_json_software;
	}

	public JsonObjectBuilder addServices(JsonObjectBuilder b) {
		b.add("service", m_json_service_10).add("service", m_json_service_11)
				.add("service", m_json_service_12)
				.add("service", m_json_service_12ext);
		return b;
	}

	public boolean inspectDesiredService(JsonObject j, String js) {
		m_desiredODFVersion = "1.2ext";
		return (!m_desiredODFVersion.isEmpty());
	}

	public List<String> getEnvironmentToPassAlong() {
		List<String> l = new LinkedList<String>();
		l.add("DISPLAY");
		l.add("XAUTHORITY");
		l.add("HOME");
		return l;
	}

	public String perform(String inputFilePath) throws Exception {
		System.out.println("webodf::perform() ");

		String format = "odf";
		ProcessBuilder pb = null;

		pb = new ProcessBuilder(m_appPath, "upload", "--convert", "--file",
				inputFilePath);
		performProcess("", pb);
		System.out.println("m_stdout: " + m_stdout);
		String id = pickSelectedFromMultiLineString(m_stdout,
				Pattern.compile(".*Id:[ ]*(?<selected>.*)"));
		if (id == null || id.equals("")) {
			throwError(
					"No ID was returned after uploading document to Google Docs.");
		}
		throwError(
				"TEST: No ID was returned after uploading document to Google Docs.");
		String stdout = m_stdout;
		String stderr = m_stderr;
		int exitCode = m_exitCode;

		System.out.println(m_appPath + " " + "download" + " -i" + " " + id
				+ " --format " + format + " --stdout");
		performProcess("", new ProcessBuilder(m_appPath, "download", "-i", id,
				"--format", format, "--stdout"));
		if (m_exitCode != 0) {
			throwError("Failed to download Google Document for ID:" + id
					+ "stderr: " + m_stderr);

		}
		writeFile(m_outputFilePath, m_stdoutraw);
		// performProcess( "", new ProcessBuilder(
		// m_appPath, "delete", "-i", id ));

		m_stdout = stdout;
		m_stderr = stderr;
		m_exitCode = exitCode;
		return m_outputFilePath;
	}
};
