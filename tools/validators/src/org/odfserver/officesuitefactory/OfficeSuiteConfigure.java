package org.odfserver.officesuitefactory;

import java.io.UnsupportedEncodingException;

public class OfficeSuiteConfigure extends OfficeSuiteBase {

	String m_configPath;
	String m_detectedTool;

	public OfficeSuiteConfigure(String configPath)
			throws UnsupportedEncodingException {
		m_configPath = configPath;
		m_detectedTool = "unknown";

	}

	public String configPath() {
		return (m_configPath);
	}

	public void run(OfficeSuiteConfigureRunConfiguration conf) {
		try {
			System.out.println("             suite: " + conf.app.getName());
			System.out.println("              host: " + conf.serverHostAndPort);
			System.out.println(" disableEncryption: " + conf.disableEncryption);
			System.out.println("     configuration: " + conf.configuration);

			setServerHostAndPort(conf.serverHostAndPort);
			setDisableEncryption(conf.disableEncryption);

			AppBase app = conf.app;

			configRead();
			app.sniff(m_config);
			app.configure(m_config);
			ensureRegistered();
			configWrite();

			System.out.println(
					"factory is setup and registered with the server...");
			System.out.println("Please start this factory using:");
			System.out.println("");
			System.out.println("java -jar officesuitefactory.jar --config "
					+ conf.configuration.getCanonicalPath());

		} catch (Exception e) {
			System.out.println(e);
			System.exit(1);
		}
	}

	public static void main(String[] args) {
		try {
			OfficeSuiteConfigureRunConfiguration conf;
			try {
				conf = OfficeSuiteConfigureRunConfiguration
						.parseArguments(args);
			} catch (ArgumentException e) {
				System.out.println(e);
				return;
			}
			System.out.println("ok, have options parsed...");
			System.out.println("suite:" + conf.app.getName());

			OfficeSuiteConfigure me = new OfficeSuiteConfigure(
					conf.configuration.getCanonicalPath());
			me.run(conf);
		} catch (Exception e) {
			System.out.println(e);
		}

	}
};
