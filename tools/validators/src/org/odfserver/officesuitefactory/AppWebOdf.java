
package org.odfserver.officesuitefactory;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.time.Instant;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import org.odfserver.Factory;

public class AppWebOdf extends AppBase {
	String m_desiredODFVersion;
	String m_webodfjs;

	JsonObject m_json_software;
	JsonObject m_json_service_10;
	JsonObject m_json_service_11;
	JsonObject m_json_service_12;
	JsonObject m_json_service_12ext;
	String m_json_service_10_string;
	String m_json_service_11_string;
	String m_json_service_12_string;
	String m_json_service_12ext_string;

	public AppWebOdf() throws UnsupportedEncodingException {
		m_detectedSoftwareVersion = "0.0";
		setupJsonStrings();

	}

	public void loadConfig(Properties p) throws UnsupportedEncodingException {
		super.loadConfig(p);
		m_webodfjs = p.getProperty("webodfjs");

		setupJsonStrings();
	}

	private void setupJsonStrings() throws UnsupportedEncodingException {
        m_json_software = Factory.createJsonsoftwareObject( "WebODF", "WebODF", m_detectedSoftwareVersion );

		String n = getName();
		m_json_service_10 = Json.createObjectBuilder()
				.add("outputType", "ODT1_2EXT").add("name", n)
				.add("inputType", "ODT1_0").build();

		m_json_service_11 = Json.createObjectBuilder()
				.add("outputType", "ODT1_2EXT").add("name", n)
				.add("inputType", "ODT1_1").build();

		m_json_service_12 = Json.createObjectBuilder()
				.add("outputType", "ODT1_2EXT").add("name", n)
				.add("inputType", "ODT1_2").build();

		m_json_service_12ext = Json.createObjectBuilder()
				.add("outputType", "ODT1_2EXT").add("name", n)
				.add("inputType", "ODT1_2EXT").build();

		m_json_service_10_string = jsonToString(m_json_service_10);
		m_json_service_11_string = jsonToString(m_json_service_11);
		m_json_service_12_string = jsonToString(m_json_service_12);
		m_json_service_12ext_string = jsonToString(m_json_service_12ext);
	}

	public String getName() {
		return "WebODF";
	}

	public String getExecutableName() {
		return "qtjsruntime";
	}

	public void sniff(Properties p) throws Exception {
		try {
			String appPath = getExecutableFullPath();
			if (appPath == null) {
				p.setProperty("enabled", "false");
				String emsg = "can't find " + getExecutableName()
						+ " please make sure it is in your PATH.";
				p.setProperty("detection-error", emsg);
				System.err.println(emsg);
				throw new Exception(emsg);
			}

			System.out.println("sniff() appPath: " + appPath);

			p.setProperty("appPath", appPath);
			p.setProperty("detection-time",
					String.valueOf(Instant.now().getEpochSecond()));

			m_detectedSoftwareVersion = "0.1";

			/*
			 * run it to see the version
			 */
			String ver = m_detectedSoftwareVersion;
			sniffCheckHaveVersionOrDie(p, ver, appPath);

			File f = new File(appPath);
			File webodfjs = new File(
					f.getCanonicalPath() + "/../../../webodf/webodf.js");
			p.setProperty("webodfjs", webodfjs.getCanonicalPath());

			runSanityCheckConversion(p);

			p.setProperty("enabled", "true");
		} catch (Exception e) {
			p.setProperty("enabled", "false");
			p.setProperty("detection-error", e.toString());
			System.err.println(e.toString());
			throw e;
		}
	}

	public JsonObject getJsonSoftware() {
		return m_json_software;
	}

	public JsonObjectBuilder addServices(JsonObjectBuilder b) {
		b.add("service", m_json_service_10).add("service", m_json_service_11)
				.add("service", m_json_service_12)
				.add("service", m_json_service_12ext);
		return b;
	}

	public boolean inspectDesiredService(JsonObject j, String js) {
		m_desiredODFVersion = "1.2ext";
		return (!m_desiredODFVersion.isEmpty());
	}

	public List<String> getEnvironmentToPassAlong() {
		List<String> l = new LinkedList<String>();
		l.add("DISPLAY");
		l.add("XAUTHORITY");
		l.add("KDEDIRS");
		l.add("HOME");
		return l;
	}

	public String perform(String inputFilePath) throws Exception {
		System.out.println("webodf::perform() ");

		String exportodf = "1";
		String exportFormat = "--export-pdf";
		String template = new String(readResource("webodf-index.html.in"),
				"UTF-8");
		String customPagePath = "/tmp/thepage.html";

		template = template.replaceAll("\\$THEODFOPUTPUTPATH",
				m_outputFilePath);
		template = template.replaceAll("\\$WEBODFJSPATH", m_webodfjs);
		template = template.replaceAll("\\$THEODFFILEPATH", inputFilePath);
		template = template.replaceAll("\\$EXPORTODF", exportodf);

		writeFile(customPagePath, template.getBytes());

		ProcessBuilder pb = new ProcessBuilder(m_appPath, exportFormat,
				m_outputFilePath + ".pdf", customPagePath);
		return performProcess(m_outputFilePath, pb);
	}
};
