
package org.odfserver.officesuitefactory;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Properties;

import org.odfserver.Factory;

public abstract class OfficeSuiteBase extends Factory {
	public OfficeSuiteBase() throws UnsupportedEncodingException {
	}

	public static List<AppBase> getApps() throws UnsupportedEncodingException {
		return AppBase.getApps();
	}

	public static AppBase getApp(String name)
			throws ArgumentException, UnsupportedEncodingException {
		return AppBase.getApp(name);
	}

	public static AppBase createInstanceForConfig(Properties p)
			throws ArgumentException, UnsupportedEncodingException {
		String n = p.getProperty("name");
		AppBase app = getApp(n);
		app.loadConfig(p);
		return app;
	}

};
