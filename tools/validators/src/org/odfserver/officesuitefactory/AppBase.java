
package org.odfserver.officesuitefactory;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonStructure;

import org.odfserver.Factory;

public abstract class AppBase {
	protected String m_appPath;
	protected String m_detectedSoftwareVersion;
	protected String m_stdout;
	protected byte[] m_stdoutraw;
	protected String m_stderr;
	protected String m_outputFilePath;
	protected int m_exitCode;
	protected boolean m_isTimeout;

	/**
	 * This is called to try to detect the Office Suite on the local machine See
	 * also sniffCheckHaveAppPathOrDie() sniffCheckHaveVersionOrDie()
	 * executeProcessToRegexCapture()
	 */
	public abstract void sniff(Properties p) throws Exception;

	/**
	 * Called for each document before perform(). This method is here to detect
	 * which version of ODF is desired using the data from getJsonSoftware() and
	 * the json from the server in string and object format.
	 */
	public abstract boolean inspectDesiredService(JsonObject j, String js);

	/**
	 * perform the conversion, most likely by passing a started Process to
	 * performProcess()
	 */
	public abstract String perform(String inputFilePath) throws Exception;

	public abstract String getName();

	public abstract String getExecutableName();

	public abstract JsonObject getJsonSoftware();

	public abstract JsonObjectBuilder addServices(JsonObjectBuilder b);

	protected void throwError(String emsg) throws Exception {
		throwError(null, emsg);
	}

	protected void throwError(Properties p, String emsg) throws Exception {
		if (p != null) {
			p.setProperty("enabled", "false");
			p.setProperty("detection-error", emsg);
		}
		System.err.println(emsg);
		throw new Exception(emsg);
	}

	protected void sniffCheckHaveAppPathOrDie(Properties p, String appPath)
			throws Exception {
		if (appPath == null) {
			String emsg = "can't find " + getExecutableName()
					+ " please make sure it is in your PATH.";
			throwError(p, emsg);
		}
	}

	protected void sniffCheckHaveVersionOrDie(Properties p, String ver,
			String appPath) throws Exception {
		System.out.println("software version:" + ver);
		if (ver == null || ver.equals("")) {
			String emsg = "can't work out software version for executable at: "
					+ appPath;
			throwError(p, emsg);
		}
		p.setProperty("version", ver);
	}

	protected String pickSelectedFromMultiLineString(BufferedReader reader,
			Pattern needle) throws Exception {
		String line = reader.readLine();
		String ret = null;
		for (; line != null; line = reader.readLine()) {
			System.out.println("line: " + line);
			Matcher m = needle.matcher(line);
			if (m.matches()) {
				ret = m.group("selected");
				return ret;
			}
		}
		return ret;
	}

	protected String executeProcessToRegexCapture(Process proc, Pattern pat)
			throws Exception {
		BufferedReader reader = new BufferedReader(
				new InputStreamReader(proc.getInputStream(), "UTF-8"));
		return pickSelectedFromMultiLineString(reader, pat);
	}

	protected String pickSelectedFromMultiLineString(String haystack,
			Pattern needle) throws Exception {
		System.out.println(
				"pickSelectedFromMultiLineString() haystack:" + haystack);
		BufferedReader reader = new BufferedReader(new StringReader(haystack));
		return pickSelectedFromMultiLineString(reader, needle);
	}

	public AppBase() {
	}

	static public String jsonToString(JsonStructure value)
			throws UnsupportedEncodingException {
		return Factory.jsonToString(value);
	}

	public void loadConfig(Properties p) throws UnsupportedEncodingException {
		m_appPath = p.getProperty("appPath");
		m_detectedSoftwareVersion = p.getProperty("version");
	}

	public int getExitCode() {
		return m_exitCode;
	}

	public String getStdOut() {
		return m_stdout;
	}

	public String getStdErr() {
		return m_stderr;
	}

	static final Map<String, String> exeCache = new HashMap<String, String>();

	public static String resolveExe(String exe) {
		if (exeCache.containsKey(exe)) {
			return exeCache.get(exe);
		}
		boolean isWindows = System.getProperty("os.name").toLowerCase()
				.indexOf("win") >= 0;
		File f = new File(exe);
		if (!f.exists()) {
			String paths[] = System.getenv("PATH").split(File.pathSeparator);
			for (String p : paths) {
				f = new File(p, exe);
				if (f.exists()) {
					break;
				}
				if (isWindows) {
					// look for windows executable
					f = new File(p, exe + ".exe");
					System.err.println("Checking " + f.getAbsolutePath());
					if (f.exists()) {
						break;
					}
				}
			}
		}
		try {
			f = f.getCanonicalFile();
		} catch (IOException e) {
		}
		if (f.isFile()) {
			exeCache.put(exe, f.getPath());
			return f.getPath();
		}
		return null;
	}

	public String chomp(String s) {
		if (s != null)
			return s.trim();
		return s;
	}

	public String getExecutableFullPath() {
		return resolveExe(getExecutableName());
	}

	static List<AppBase> m_appList = new LinkedList<AppBase>();

	public static List<AppBase> getApps() throws UnsupportedEncodingException {
		if (m_appList.isEmpty()) {
			m_appList.add(new AppAbiword());
			m_appList.add(new AppCalligra());
			m_appList.add(new AppWebOdf());
			m_appList.add(new AppGoogleDocs());
		}
		return m_appList;
	}

	public static AppBase getApp(String name)
			throws ArgumentException, UnsupportedEncodingException {
		List<AppBase> applist = getApps();
		for (AppBase app : applist) {
			if (app.getName().equals(name))
				return app;
		}
		throw new ArgumentException("App not found:" + name);
	}

	public void configure(Properties p) {
		p.setProperty("name", getName());
	}

	public void runSanityCheckConversion(Properties p) {
		// FIXME.
	}

	public String createTemporaryFilePath() {
		// FIXME
		return "/tmp/output.odt";
	}

	public void prepareToPerform() {
		m_stdout = "";
		m_stdoutraw = null;
		m_stderr = "";
		m_outputFilePath = createTemporaryFilePath();
		m_exitCode = 0;
		m_isTimeout = false;
	}

	public List<String> getEnvironmentToPassAlong() {
		return new LinkedList<String>();
	}

	protected String performProcess(String outputFilePath, ProcessBuilder pb)
			throws Exception {
		pb.environment().clear();
		Map<String, String> env = pb.environment();
		List<String> envToPass = getEnvironmentToPassAlong();
		for (String s : envToPass) {
			env.put(s, System.getenv(s));
			System.out.println("env " + s + " = " + System.getenv(s));
		}

		System.out.println("starting proc...");
		Process p = pb.start();
		System.out.println("started proc...");
		try {
			p.getOutputStream().close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		Reader stdout = new Reader(p.getInputStream());
		stdout.start();
		Reader stderr = new Reader(p.getErrorStream());
		stderr.start();
		try {
			Waiter w = new Waiter(Thread.currentThread());
			w.start();
			p.waitFor();
			w.clear();
		} catch (InterruptedException e) {
			m_isTimeout = true;
		} finally {
			stdout.stopReading();
			stderr.stopReading();
			p.destroy();
			if (!m_isTimeout) {
				m_exitCode = p.exitValue();
			}
			try {
				m_stdoutraw = stdout.out.toByteArray();
				m_stdout = stdout.out.toString("UTF-8");
				m_stderr = stderr.out.toString("UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (OutOfMemoryError e) {
				// happens when there a lot of output on stderr or stdout
			}
			// cr.setDurationMs((int) ((System.nanoTime() - start) / 1000000));
		}

		// System.out.println("waiting for proc...");
		// // p.waitFor();
		// System.out.println("done with proc...");
		// m_stdout = IOUtils.toString(p.getInputStream(), "utf-8");
		// System.out.println("...1");
		// m_stderr = IOUtils.toString(p.getErrorStream(), "utf-8");
		// System.out.println("...2");
		// m_exitCode = p.exitValue();
		// System.out.println("...3");
		return outputFilePath;
	}

	public byte[] readResource(String relativePath) throws IOException {
		System.out.println("readResource(): " + relativePath);
		URL url = this.getClass().getResource(relativePath);
		System.out.println("readResource(2): " + url);
		InputStream is = url.openStream();
		byte[] b = new byte[is.available()];
		is.read(b);
		return b;
	}

	public static void writeFile(String path, byte[] data)
			throws IOException, FileNotFoundException {
		if (data == null) {
			System.err.println("writeFile given no data!");
			return;
		}

		FileOutputStream fos = new FileOutputStream(path);
		fos.write(data);
		fos.close();
	}
};

class Reader extends Thread {

	final InputStream in;
	final ByteArrayOutputStream out;
	boolean keepRunning;

	Reader(InputStream in) {
		this.in = in;
		out = new ByteArrayOutputStream();
		keepRunning = true;
	}

	public void stopReading() {
		setKeepRunning(false);
		this.interrupt();
		try {
			join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	synchronized private void setKeepRunning(boolean value) {
		keepRunning = value;
	}

	synchronized private boolean keepRunning() {
		return keepRunning;
	}

	public void run() {
		final int l = 1024;
		byte b[] = new byte[l];
		try {
			while (keepRunning()) {
				int n = in.available();
				while (n > 0) {
					if (n > l) {
						n = l;
					}
					in.read(b, 0, n);
					out.write(b, 0, n);
					n = in.available();
				}
				try {
					sleep(1);
				} catch (InterruptedException e) {
				}
			}
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

class Waiter extends Thread {
	private Thread thread;

	Waiter(Thread t) {
		this.thread = t;
	}

	void clear() {
		stopWaiting();
		interrupt();
		try {
			join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private synchronized void stopWaiting() {
		thread = null;
	}

	private synchronized Thread getThread() {
		return thread;
	}

	public void run() {
		try {
			sleep(60000);
		} catch (InterruptedException e) {
		}
		Thread thread = getThread();
		if (thread != null) {
			thread.interrupt();
		}
	}

}
