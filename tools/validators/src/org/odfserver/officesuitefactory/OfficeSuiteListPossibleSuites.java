package org.odfserver.officesuitefactory;

import java.io.UnsupportedEncodingException;
import java.util.List;

public class OfficeSuiteListPossibleSuites extends OfficeSuiteBase {

	public OfficeSuiteListPossibleSuites() throws UnsupportedEncodingException {
	}

	public String configPath() {
		return ("");
	}

	public void run() {
		try {
			System.out.println(
					"The following Office Suites are supported for detection.");
			System.out.println("");
			List<AppBase> applist = getApps();
			for (AppBase app : applist) {
				System.out.println(app.getName());
			}
			System.out.println("");
		} catch (Exception e) {
			System.out.println(e);
			System.exit(1);
		}
	}

	public static void main(String[] args) {
		try {
			OfficeSuiteListPossibleSuites me = new OfficeSuiteListPossibleSuites();
			me.run();
		} catch (Exception e) {
			System.out.println(e);
		}

	}
};
