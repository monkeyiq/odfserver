
package org.odfserver.officesuitefactory;

import java.io.UnsupportedEncodingException;
import java.time.Instant;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.xml.bind.DatatypeConverter;

public class OfficeSuiteFactory extends OfficeSuiteBase {

	String m_configPath;
	int m_nextNumber;

	public OfficeSuiteFactory(String configPath)
			throws UnsupportedEncodingException {
		m_configPath = configPath;
		m_nextNumber = 1;
	}

	public String configPath() {
		return (m_configPath);
	}

	public void run(String[] argv) {
		/**
		 * If this fails then we don't start, so no need to report anything to
		 * the server
		 */
		AppBase app = null;

		try {
			configRead();
			ensureRegistered();
			app = createInstanceForConfig(m_config);
		} catch (Exception e) {
			System.out.println(e);
			System.exit(1);
			return;
		}

		while (true) {
			try {
				JsonObjectBuilder b = Json.createObjectBuilder().add("software",
						app.getJsonSoftware());
				b = app.addServices(b);

				JsonObject request = Json.createObjectBuilder()
						.add("factoryKey", getFactoryKey())
						.add("maxWaitTime", 0).add("nextNumber", m_nextNumber)
						.add("serviceInstances",
								Json.createArrayBuilder().add(b))
						.build();

				JsonObject reply = (JsonObject) post("/factory/retrievejobs",
						request);
				JsonArray jobs = reply.getJsonArray("jobs");
				for (int ji = 0; ji < jobs.size(); ji++) {
					JsonObject job = jobs.getJsonObject(ji);
					JsonObject service = job.getJsonObject("service");
					String inputFile = job.getString("inputFile");
					int number = job.getInt("number");
					JsonObject conversion = service.getJsonObject("service");

					byte[] inputFileBytes = DatatypeConverter
							.parseBase64Binary(inputFile);
					String inputFilePath = "/tmp/input.odt";
					writeFile(inputFilePath, inputFileBytes);

					System.out.println("job number: " + number);
					System.out
							.println("conversion: " + jsonToString(conversion));

					m_nextNumber = number + 1;
					String conversionStr = jsonToString(conversion);
					String desiredODFVersion = "";

					if (!app.inspectDesiredService(conversion, conversionStr)) {
						System.out.println("ERROR: service not found");
					} else {
						System.out.println(
								"desiredODFVersion: " + desiredODFVersion);

						app.prepareToPerform();

						Instant started = Instant.now();

						try {
							String resultPath = app.perform(inputFilePath);
							Instant finished = Instant.now();
							byte[] resultdoc = readFile(resultPath);

							// send that result back to /uploadjobresult
							uploadjobresult(toBase64(resultdoc), number,
									started, finished, app.getExitCode(),
									app.getStdOut(), app.getStdErr());

						} catch (Exception e) {
							Instant finished = Instant.now();
							// send that result back to /uploadjobresult
							uploadjobresult("", number, started, finished,
									app.getExitCode(), app.getStdOut(),
									e.toString() + "\n\n" + app.getStdErr());

						}
					}
				}

				Thread.sleep(5000);
			} catch (Exception e) {
				System.out.println(e);
				try {
					Thread.sleep(15000);
				} catch (Exception e2) {
					System.out.println(e2);
				}
			}
		}
	}

	public static void main(String[] argv) {
		try {
			String confPath = "";
			if (argv.length > 1) {
				if ("-c".equals(argv[0]) || "--config".equals(argv[0]))
					confPath = argv[1];
			}

			OfficeSuiteFactory me = new OfficeSuiteFactory(confPath);
			me.run(argv);
		} catch (UnsupportedEncodingException e) {
			System.out.println(e);
		}

	}
};
