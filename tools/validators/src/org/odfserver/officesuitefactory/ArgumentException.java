package org.odfserver.officesuitefactory;

public class ArgumentException extends Exception {
	private static final long serialVersionUID = 2289722464605670683L;

	ArgumentException(String msg) {
		super(msg);
	}

}
