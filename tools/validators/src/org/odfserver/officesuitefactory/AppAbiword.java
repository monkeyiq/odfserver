
package org.odfserver.officesuitefactory;

import java.io.UnsupportedEncodingException;
import java.time.Instant;
import java.util.Properties;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import org.apache.commons.io.IOUtils;

import org.odfserver.Factory;


public class AppAbiword extends AppBase {
	String m_desiredODFVersion;

	JsonObject m_json_software;
	JsonObject m_json_service_10;
	JsonObject m_json_service_11;
	JsonObject m_json_service_12;
	JsonObject m_json_service_12ext;
	String m_json_service_10_string;
	String m_json_service_11_string;
	String m_json_service_12_string;
	String m_json_service_12ext_string;

	public AppAbiword() throws UnsupportedEncodingException {
		m_detectedSoftwareVersion = "0.0";
		setupJsonStrings();

	}

	public void loadConfig(Properties p) throws UnsupportedEncodingException {
		super.loadConfig(p);
		setupJsonStrings();
	}

	private void setupJsonStrings() throws UnsupportedEncodingException {
        m_json_software = Factory.createJsonsoftwareObject( "Abiword", "Abiword", m_detectedSoftwareVersion );
		String n = getName();
		m_json_service_10 = Json.createObjectBuilder()
				.add("outputType", "ODT1_1").add("name", n)
				.add("inputType", "ODT1_0").build();

		m_json_service_11 = Json.createObjectBuilder()
				.add("outputType", "ODT1_1").add("name", n)
				.add("inputType", "ODT1_1").build();

		m_json_service_12 = Json.createObjectBuilder()
				.add("outputType", "ODT1_1").add("name", n)
				.add("inputType", "ODT1_2").build();

		m_json_service_12ext = Json.createObjectBuilder()
				.add("outputType", "ODT1_1").add("name", n)
				.add("inputType", "ODT1_2EXT").build();

		m_json_service_10_string = jsonToString(m_json_service_10);
		m_json_service_11_string = jsonToString(m_json_service_11);
		m_json_service_12_string = jsonToString(m_json_service_12);
		m_json_service_12ext_string = jsonToString(m_json_service_12ext);
	}

	public String getName() {
		return "Abiword";
	}

	public String getExecutableName() {
		return "abiword";
	}

	public void sniff(Properties p) throws Exception {
		try {
			String appPath = getExecutableFullPath();

			p.setProperty("appPath", appPath);
			p.setProperty("detection-time",
					String.valueOf(Instant.now().getEpochSecond()));

			/*
			 * run it to see the version
			 */
			Process proc = new ProcessBuilder(appPath, "--version").start();
			String ver = IOUtils.toString(proc.getInputStream(), "utf-8");
			ver = chomp(ver);
			sniffCheckHaveVersionOrDie(p, ver, appPath);

			runSanityCheckConversion(p);
			p.setProperty("enabled", "true");
		} catch (Exception e) {
			p.setProperty("enabled", "false");
			p.setProperty("detection-error", e.toString());
			System.err.println(e.toString());
			throw e;
		}
	}

	public JsonObject getJsonSoftware() {
		return m_json_software;
	}

	public JsonObjectBuilder addServices(JsonObjectBuilder b) {
		b.add("service", m_json_service_10).add("service", m_json_service_11)
				.add("service", m_json_service_12)
				.add("service", m_json_service_12ext);
		return b;
	}

	public boolean inspectDesiredService(JsonObject j, String js) {
		m_desiredODFVersion = "1.1";
		return (!m_desiredODFVersion.isEmpty());
	}

	public String perform(String inputFilePath) throws Exception {
		return performProcess(m_outputFilePath, new ProcessBuilder(m_appPath,
				"-t", "odt", "-o", m_outputFilePath, inputFilePath));
	}
};
