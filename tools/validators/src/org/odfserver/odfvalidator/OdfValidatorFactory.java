
package org.odfserver.odfvalidator;

import java.io.UnsupportedEncodingException;
import java.time.Instant;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.xml.bind.DatatypeConverter;

import org.odfserver.SslTweaks;
import org.w3c.dom.Document;

public class OdfValidatorFactory extends OdfValidator {

	public OdfValidatorFactory() throws UnsupportedEncodingException {

		registerSoftware("Validation", "ODF Validator",
				m_detectedSoftwareVersion);
		registerService("odfvalidator", "ODT1_0", "VALIDATIONREPORT");
		registerService("odfvalidator", "ODT1_1", "VALIDATIONREPORT");
		registerService("odfvalidator", "ODT1_2", "VALIDATIONREPORT");
		registerService("odfvalidator", "ODT1_2EXT", "VALIDATIONREPORT");
	}

	public String configPath() {
		return ("odfvalidatorfactory.properties");
	}

	public void run(String[] argv) {
		/**
		 * If this fails then we don't start, so no need to report anything to
		 * the server
		 */
		try {
			if (argv.length > 0) {
				setServerHostAndPort(argv[0]);
			}
			if (argv.length > 1) {
				if ("disable".equals(argv[1]))
					SslTweaks.disableSslVerification();
			}

			configRead();
			ensureRegistered();
		} catch (Exception e) {
			System.out.println(e);
			System.exit(1);
		}

		while (true) {
			try {
				JsonObject reply = postRetrieveJobs();
				JsonArray jobs = reply.getJsonArray("jobs");
				for (int ji = 0; ji < jobs.size(); ji++) {
					JsonObject job = jobs.getJsonObject(ji);
					JsonObject service = job.getJsonObject("service");
					String inputFile = job.getString("inputFile");
					int number = job.getInt("number");
					JsonObject conversion = service.getJsonObject("service");

					try {
						byte[] inputFileBytes = DatatypeConverter
								.parseBase64Binary(inputFile);
						String inputFilePath = "/tmp/input.odt";
						writeFile(inputFilePath, inputFileBytes);

						System.out.println("job number: " + number);
						System.out.println(
								"conversion: " + jsonToString(conversion));

						String conversionStr = jsonToString(conversion);
						String desiredODFVersion = getInputODFVersion(job);

						System.out.println(
								"desiredODFVersion: " + desiredODFVersion);

						Instant started = Instant.now();
						Document doc = validate(inputFilePath,
								desiredODFVersion);
						prettyPrint(doc);
						Instant finished = Instant.now();
						int exitCode = 0;

						// send that result back to /uploadjobresult
						uploadjobresult(doc, number, started, finished,
								exitCode);
					} catch (Exception e) {
						// send that result back to /uploadjobresult
						Instant started = Instant.now();
						Instant finished = Instant.now();
						uploadjobresult("", number, started, finished, 1, "",
								e.toString());
					}
				}

				Thread.sleep(5000);
			} catch (Exception e) {
				System.out.println(e);
				try {
					Thread.sleep(15000);
				} catch (Exception e2) {
					System.out.println(e2);
				}
			}
		}
	}

	public static void main(String[] argv) {
		try {
			OdfValidatorFactory me = new OdfValidatorFactory();
			me.run(argv);
		} catch (UnsupportedEncodingException e) {
			System.out.println(e);
		}

	}
};
