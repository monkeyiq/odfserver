
package org.odfserver.odfvalidator;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;
import java.io.StringReader;
import java.util.List;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.odfserver.Factory;
import org.odftoolkit.odfvalidator.Logger;
import org.odftoolkit.odfvalidator.ODFValidator;
import org.odftoolkit.odfvalidator.OdfValidatorMode;
import org.odftoolkit.odfvalidator.OdfVersion;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class OdfValidator extends Factory {

	public OdfValidator() {
	}

	public String configPath() {
		return ("odfvalidator.properties");
	}

	public int validateSingleFile(String desiredODFVersion, String ODFInputPath,
			String digest, Document doc, Element fe) throws Exception {

		if (desiredODFVersion.equals("")) {
			throw new Exception("Invalid ODF Version for input document");
		}

		OdfVersion odfvers = OdfVersion.valueOf(desiredODFVersion, false);
		OdfValidatorMode validatorMode = OdfValidatorMode.VALIDATE;
		if (desiredODFVersion.contains("ext"))
			validatorMode = OdfValidatorMode.EXTENDED_CONFORMANCE;
		System.out.println("validateSingleFile() v " + desiredODFVersion
				+ "  isExt? " + (desiredODFVersion.contains("ext")));

		List<String> ODFInputPathList = new Vector<String>();
		ODFInputPathList.add(ODFInputPath);

		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(bytes);
		ODFValidator validator = new ODFValidator(null, Logger.LogLevel.ERROR,
				odfvers, false);
		boolean hasIssues = validator.validate(ps, ODFInputPathList, null,
				validatorMode, false, null);
		String content = bytes.toString("UTF-8");
		if (!hasIssues) {
			fe.setAttribute("result", "success");
			return (0);
		}

		Pattern errlinepatternMostSpecific = Pattern.compile(ODFInputPath
				+ "/(?<component>[^\\[]+)\\[(?<line>[0-9]+),(?<column>[0-9]+)\\]:[ ]*(Error:[ ])?(?<msg>.*)");
		Pattern errlinepatternFreeScope = Pattern.compile(
				ODFInputPath + "(/(?<component>[^:]+))?:[ ]*(?<msg>.*)");
		BufferedReader reader = new BufferedReader(new StringReader(content));
		String line = reader.readLine();
		while (line != null) {

			Matcher mMostSpecific = errlinepatternMostSpecific.matcher(line);

			Matcher m = mMostSpecific;
			if (m.matches()) {
				Element e = doc.createElement("error");
				e.setAttribute("component", m.group("component"));
				e.setAttribute("start-line", m.group("line"));
				e.setAttribute("start-column", m.group("column"));
				e.insertBefore(doc.createTextNode(m.group("msg")),
						e.getLastChild());
				fe.appendChild(e);
			} else {
				Matcher mFreeScope = errlinepatternFreeScope.matcher(line);
				m = mFreeScope;
				// System.out.println("m2: " + m.matches());
				if (m.matches()) {
					Element e = doc.createElement("error");
					if (m.group("component") != null)
						e.setAttribute("component", m.group("component"));
					e.insertBefore(doc.createTextNode(m.group("msg")),
							e.getLastChild());
					fe.appendChild(e);
				} else {
					// System.out.println("generic catch all...");
					Element e = doc.createElement("error");
					e.insertBefore(doc.createTextNode(line), e.getLastChild());
					fe.appendChild(e);
				}
			}

			line = reader.readLine();
		}

		return (1);
	}

	public Document validate(String filepath) throws Exception {
		return validate(filepath, "1.2");
	}

	public Document validate(String filepath, String desiredODFVersion)
			throws Exception {
		String ODFInputPath = filepath;
		String digest = "unknown";

		Document doc = null;
		Element fe = null;

		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = dbf.newDocumentBuilder();
			doc = builder.newDocument();
			Element root = doc.createElement("validation-report");
			doc.appendChild(root);
			fe = doc.createElement("file");
			root.appendChild(fe);

			if (java.lang.System.getenv("ODFSERVER_FILENAME_INPUT") != null) {
				ODFInputPath = java.lang.System
						.getenv("ODFSERVER_FILENAME_INPUT");
			}
			if (ODFInputPath == "") {
				System.out.println(
						"<validation-report><error>No Input ODF File given!</error></validation-report>");
				System.exit(1);
			}

			// try to get the absolute path, set the path first so the error
			// handling
			// can report the original path of the file is not found, then
			// update the
			// path to the absolute one.
			fe.setAttribute("name", ODFInputPath);
			File ODFInputPathFile = new File(ODFInputPath);
			ODFInputPath = ODFInputPathFile.getAbsolutePath();
			fe.setAttribute("name", ODFInputPath);

			// expand the DOM shell doc to include the sha-256.
			digest = digestPath(ODFInputPath);
			fe.setAttribute("sha256", digest);

			// process this ODF
			validateSingleFile(desiredODFVersion, ODFInputPath, digest, doc,
					fe);
			return doc;
		} catch (Exception ex) {
			throw ex;
		}
	}

	public void run(String[] argv) {
		String filepath = "";
		if (argv.length > 0) {
			filepath = argv[0];
		}

		Document doc;
		try {
			doc = validate(filepath);
			prettyPrint(doc);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String[] argv) {
		OdfValidator me = new OdfValidator();
		me.run(argv);
	}
};
