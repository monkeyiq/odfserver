#!/usr/bin/env bash

SCRIPTDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
cd "$SCRIPTDIR"

CP="$`pwd`/bin:$CLASSPATH"
for fi in `pwd`/lib/*jar; do
    CP="$CP:$fi"
done

hostport=${1:-https://localhost:3443}
insecure=${2:-disable}
java -cp "$CP" org.odfserver.odfvalidator.OdfValidatorFactory "$hostport" "$insecure"

