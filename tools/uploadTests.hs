{-# LANGUAGE OverloadedStrings #-}
import           Control.Monad                         (guard, unless)
import           Data.ByteString                       (ByteString)
import qualified Data.ByteString.Lazy                  as BL (ByteString)
import           Data.List                             (find)
import           Data.Maybe                            (catMaybes, fromJust,
                                                        isJust)
import           Data.Text                             (pack, unpack)
import           Data.Text.Encoding                    (decodeUtf8, encodeUtf8)
import qualified Data.Text.Lazy                        as L (unpack)
import qualified Data.Text.Lazy.Encoding               as LE (decodeUtf8)
import           Network.Connection                    (TLSSettings (..))
import           Network.HTTP.Client                   (CookieJar, Manager,
                                                        Response, checkStatus,
                                                        cookieJar, httpLbs,
                                                        method, newManager,
                                                        parseUrl, redirectCount,
                                                        responseBody,
                                                        responseCookieJar,
                                                        responseHeaders,
                                                        responseStatus,
                                                        urlEncodedBody)
import           Network.HTTP.Client.MultipartFormData (formDataBody, partBS,
                                                        partFile)
import           Network.HTTP.Client.TLS               (mkManagerSettings)
import           Network.HTTP.Types.Header             (hLocation)
import           Network.HTTP.Types.Status             (statusCode)
import           System.Console.GetOpt
import           System.Environment                    (getArgs, getProgName)
import           Text.Regex
import           Text.XML.HXT.Core
import           Text.XML.HXT.DOM.XmlNode              (NTree (..))
import           Text.XML.HXT.TagSoup                  (withTagSoup)

data Session = Session {
    manager :: Manager,
    cookies :: CookieJar,
    host    :: String
}

data Test = Test {
    testName     :: String,
    testFormat   :: String,
    testFragment :: String,
    testXPaths   :: [(String, String)]
}

autotestns :: String
autotestns = "http://www.example.org/documenttests"

testQName :: QName
testQName = mkNsName "test" autotestns

inputQName :: QName
inputQName = mkNsName "input" autotestns

outputQName :: QName
outputQName = mkNsName "output" autotestns

fileQName :: QName
fileQName = mkNsName "file" autotestns

xpathQName :: QName
xpathQName = mkNsName "xpath" autotestns

parseXPaths :: XmlTree -> [(String,String)]
parseXPaths file = map (\p -> (path,ns1 $ ns2 $ ns3 p)) xpaths
    where
        ns1 p = subRegex (mkRegex "\\bs:(\\w)") p "style:\\1"
        ns2 p = subRegex (mkRegex "\\bt:(\\w)") p "text:\\1"
        ns3 p = subRegex (mkRegex "\\bo:(\\w)") p "office:\\1"
        (path:_) = runLA (getAttrValue "path") file
        xpaths = runLA (getChildren >>> hasQName xpathQName >>> getAttrValue "expr") file

parseTest :: XmlTree -> Test
parseTest testElement = Test {
        testName = name,
        testFormat = inputType,
        testFragment = subRegex (mkRegex "^  ") cleanFragment "",
        testXPaths = xpaths
    }
    where
        (name:_) = runLA (getAttrValue "name") testElement
        (inputType:_) = runLA (getChildren >>> hasQName inputQName >>> getAttrValue "type") testElement
        (fragment:_) = runLA (getChildren >>> hasQName inputQName
            >>> indentDoc
            >>> writeDocumentToString []) testElement
        -- remove redundant newlines from start of the xml fragment
        cleanFragment = dropWhile ('\n' ==) fragment
        files = runLA (getChildren >>> hasQName outputQName
                       >>> getChildren >>> hasQName fileQName) testElement
        xpaths = concatMap parseXPaths files

getTests :: XmlTree -> [Test]
getTests doc = map parseTest testElements
    where
        testElements = runLA (deep (hasQName testQName)) doc


replacePrefixes :: NTree XNode -> XmlTree
replacePrefixes (NTree n a) = NTree (rp n) (map replacePrefixes a)
  where
    prefixes = map (first tons) [ ("chart", "chart")
               , ("dr3d", "dr3d")
               , ("drawing", "draw")
               , ("form", "form")
               , ("manifest", "manifest")
               , ("meta", "meta")
               , ("office", "office")
               , ("presentation", "presentation")
               , ("script", "script")
               , ("smil-compatible", "smil")
               , ("style", "style")
               , ("svg-compatible", "svg")
               , ("table", "table")
               , ("text", "text")
               , ("xsl-fo-compatible", "fo") ]
           ++ [ ("http://purl.org/dc/elements/1.1/", "dc"),
                ("http://www.w3.org/1999/xhtml", "xhtml"),
                ("http://www.w3.org/1999/xlink", "xlink")]
    tons :: String -> String
    tons ns = "urn:oasis:names:tc:opendocument:xmlns:" ++ ns ++ ":1.0"
    np p qn = mkQName p (localPart qn) (namespaceUri qn)
    nqn qn = case find (\(ns,_) -> namespaceUri qn == ns) prefixes of
                 Nothing -> qn
                 Just (_, b) -> np b qn
    rp (XTag qname atts) = XTag (nqn qname) $ map replacePrefixes atts
    rp (XAttr qname) = XAttr (nqn qname)
    rp node = node

parse :: FilePath -> IO [Test]
parse path = do
    doc <- runX (readDocument [withValidate no, withCheckNamespaces yes] path)
    let v = map replacePrefixes doc
    return $ concatMap getTests v

getManager :: IO Manager
getManager = newManager $ mkManagerSettings
            TLSSettingsSimple{settingDisableCertificateValidation=True,
                              settingDisableSession=False,
                              settingUseServerName=False} Nothing

get :: Session -> String -> IO (Response BL.ByteString)
get session path = do
    req <- parseUrl (host session ++ path)
    let r = req { method = "GET", cookieJar = Just $ cookies session }
    httpLbs r (manager session)

post :: Session -> String -> [(ByteString,ByteString)] -> IO (Response BL.ByteString)
post session path params = do
    req <- parseUrl (host session ++ path)
    let r = urlEncodedBody params $ req { method = "POST"
                                        , cookieJar = Just $ cookies session
                                        , redirectCount = 0
                                        , checkStatus = \_ _ _ -> Nothing}
    httpLbs r (manager session)

login :: Manager -> String -> String -> String -> IO CookieJar
login man hostname email password = do
    initialRequest <- parseUrl (hostname ++ "/auth/page/email/login")
    let req = urlEncodedBody [("email", b email),("password", b password)]
              $ initialRequest { method = "POST" }
    res <- httpLbs req man
    return $ responseCookieJar res
  where b = encodeUtf8 . pack

getToken :: Session -> String -> IO (Maybe String)
getToken session path = do
    res <- get session path
    let body = L.unpack $ LE.decodeUtf8 $ responseBody res
    a <- runX (readString [ withTagSoup ] body >>> deep
            (isElem
             >>> hasName "input"
             >>> hasAttrValue "name" (== "_token")
             >>> getAttrValue "value"
             >>> mkText
            )
        )
    return $ case a of
        (NTree (XText token) _:_) -> Just token
        _ -> Nothing

createTest :: Session -> [(String,Int)] -> Test -> IO ()
createTest session templates test = do
    let format = testFormat test
        mid = lookup (testFormat test) templates
    unless (isJust mid) $ print $ "No template provided for " ++ format
    guard $ isJust mid
    let path = "/new/test/" ++ show (fromJust mid)
    token <- getToken session path
    let params = [("save", "yes"),
                 ("_token", b $ fromJust token),
                 ("f1", b $ testName test),
                 ("f2", b $ testFragment test),
                 ("f3", "1"), -- all odf versions
                 ("f3", "2"),
                 ("f3", "3"),
                 ("f3", "4")]
        xpaths = testXPaths test
        params' = map (\(n, p) -> [("f4", b p), ("f4", b n)]) xpaths
        allParams = params ++ concat params'
    case xpaths of
        [] -> return ()
        _ -> do
            putStrLn $ testFragment test
            mapM_ (\(p,x) -> putStrLn $ p ++ " " ++ x) xpaths
            putStrLn ""
            r <- post session path allParams
            print $ responseStatus r
            guard (statusCode (responseStatus r) == 303)
            return ()
  where
      b = encodeUtf8 . pack

data Options = Options
    { optServerUrl   :: String
    , optUserEmail   :: String
    , optPassword    :: String
    , optOdtTemplate :: Maybe FilePath
    , optOdsTemplate :: Maybe FilePath
    , optOdpTemplate :: Maybe FilePath
    } deriving Show

defaultOptions :: Options
defaultOptions = Options
    { optServerUrl   = undefined
    , optUserEmail   = undefined
    , optPassword    = undefined
    , optOdtTemplate = Nothing
    , optOdsTemplate = Nothing
    , optOdpTemplate = Nothing
    }

options :: [OptDescr (Options -> Options)]
options =
    [ Option ['h'] ["url"]
        (ReqArg (\u opts -> opts { optServerUrl = u }) "URL")
        "server to upload the tests to"
    , Option ['u'] ["user"]
        (ReqArg (\u opts -> opts { optUserEmail = u }) "USER_EMAIL")
        "user email to log into the server"
    , Option ['p'] ["password"]
        (ReqArg (\u opts -> opts { optPassword = u }) "PASSWORD")
        "password to log into the server"
    , Option [] ["odt"]
        (ReqArg (\o opts -> opts { optOdtTemplate = Just o }) "FILE")
        "template file for odt tests"
    , Option [] ["ods"]
        (ReqArg (\o opts -> opts { optOdsTemplate = Just o }) "FILE")
        "template file for ods tests"
    , Option [] ["odp"]
        (ReqArg (\o opts -> opts { optOdpTemplate = Just o }) "FILE")
        "template file for odp tests"
    ]

uploadOpts :: [String] -> IO (Options, [String])
uploadOpts argv = do
    progName <- getProgName
    let header = "Usage: " ++ progName ++ " [OPTION...]"
    case getOpt Permute options argv of
        (o,n,[]  ) -> return (foldl (flip id) defaultOptions o, n)
        (_,_,errs) -> ioError (userError (concat errs ++ usageInfo header options))

uploadFile :: Session -> FilePath -> IO (Response BL.ByteString)
uploadFile session path = do
    token <- getToken session "/upload"
    let bsToken = encodeUtf8 $ pack $ fromJust token
    req <- parseUrl (host session ++ "/upload")
    let r = req { method = "POST"
                , cookieJar = Just $ cookies session
                , redirectCount = 0
                , checkStatus = \_ _ _ -> Nothing}
    flip httpLbs (manager session) =<<
        formDataBody [ partBS "_token" bsToken
                     , partFile "f1"  path] r

uploadTemplate :: Session -> (String, Maybe FilePath) -> IO (Maybe (String, Int))
uploadTemplate session (ext, mpath) =
    case mpath of
        Nothing -> return Nothing
        Just path -> do
            r <- uploadFile session path
            guard (statusCode (responseStatus r) == 303)
            let headers = responseHeaders r
                location = fromJust $ lookup hLocation headers
                fileid = drop (length $ host session) $ unpack $ decodeUtf8 location
                id' = reverse $ takeWhile ('/' /=) $ reverse fileid
            return $ Just (ext, read id')

main :: IO ()
main = do
    args <- getArgs
    (opts, testfiles) <- uploadOpts args
    tests <- mapM parse testfiles

    man <- getManager
    let hostname = optServerUrl opts
    cookiejar <- login man hostname (optUserEmail opts) (optPassword opts)
    let session = Session man cookiejar hostname
        templatePaths = [ ("odt1.2", optOdtTemplate opts)
                        , ("ods1.2", optOdsTemplate opts)
                        , ("odp1.2", optOdpTemplate opts)]
    templates <- mapM (uploadTemplate session) templatePaths
    print $ catMaybes templates

    mapM_ (createTest session $ catMaybes templates) $ concat tests
