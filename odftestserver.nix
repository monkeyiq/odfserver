{ mkDerivation, aeson, async, base, base64-bytestring, binary
, blaze-html, blaze-markup, bytestring, case-insensitive
, classy-prelude, classy-prelude-conduit, classy-prelude-yesod
, concurrent-extra, conduit, conduit-combinators, conduit-extra
, containers, data-default, directory, either, errors, esqueleto
, fast-logger, feed, file-embed, filepath, hjsmin, hspec
, http-client, http-conduit, http-types, hxt, hxt-tagsoup, hxt-xpath, markdown
, mime-mail, monad-control, monad-logger, mono-traversable, mtl
, network, pandoc, persistent, persistent-postgresql
, persistent-sqlite, persistent-template, pretty, process, random
, regex-posix, resource-pool, resourcet, safe, servant
, servant-client, servant-docs, servant-pandoc, servant-server, SHA
, shakespeare, stdenv, stm, stm-lifted, tagsoup, template-haskell
, temporary, text, time, tls, transformers, unordered-containers
, utf8-string, uuid, vector, wai, wai-extra, wai-logger, warp
, warp-tls, xml, xml-conduit, xss-sanitize, yaml, yesod, yesod-auth
, yesod-core, yesod-form, yesod-newsfeed, yesod-persistent
, yesod-sitemap, yesod-static, yesod-test, zip-archive, HandsomeSoup
# these are added by hand
, cabal-install, stack, yesod-bin, happy, alex, regex-compat
}:
mkDerivation {
  pname = "odftestserver";
  version = "0.1";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    aeson async base base64-bytestring binary blaze-html blaze-markup
    bytestring case-insensitive classy-prelude classy-prelude-conduit
    classy-prelude-yesod concurrent-extra conduit conduit-combinators
    conduit-extra containers data-default directory either errors
    esqueleto fast-logger feed file-embed hjsmin http-client
    http-conduit http-types hxt hxt-tagsoup hxt-xpath markdown mime-mail monad-control
    monad-logger mtl pandoc persistent persistent-postgresql
    persistent-sqlite persistent-template pretty random resource-pool
    safe servant servant-docs servant-pandoc servant-server SHA
    shakespeare stm stm-lifted tagsoup template-haskell text time tls
    transformers unordered-containers utf8-string uuid vector wai
    wai-extra wai-logger warp warp-tls xml xml-conduit xss-sanitize
    yaml yesod yesod-auth yesod-core yesod-form yesod-newsfeed
    yesod-persistent yesod-sitemap yesod-static zip-archive HandsomeSoup
# these are added by hand
    cabal-install stack yesod-bin happy alex regex-compat
  ];
  executableHaskellDepends = [
    aeson base base64-bytestring bytestring classy-prelude-yesod
    concurrent-extra containers directory either filepath mtl network
    persistent persistent-sqlite process resource-pool resourcet
    servant servant-client servant-server SHA temporary text time tls
    transformers xml-conduit yesod-auth zip-archive
  ];
  testHaskellDepends = [
    aeson async base base64-bytestring binary blaze-html blaze-markup
    bytestring case-insensitive classy-prelude classy-prelude-conduit
    classy-prelude-yesod concurrent-extra conduit conduit-combinators
    conduit-extra containers data-default directory either errors
    esqueleto fast-logger feed file-embed hjsmin hspec http-client
    http-conduit http-types markdown mime-mail monad-control
    monad-logger mono-traversable mtl pandoc persistent
    persistent-postgresql persistent-sqlite persistent-template pretty
    random regex-posix resource-pool safe servant servant-docs
    servant-pandoc servant-server SHA shakespeare stm stm-lifted
    tagsoup template-haskell temporary text time tls transformers
    unordered-containers utf8-string uuid vector wai wai-extra
    wai-logger warp warp-tls xml xml-conduit xss-sanitize yaml yesod
    yesod-auth yesod-core yesod-form yesod-newsfeed yesod-persistent
    yesod-sitemap yesod-static yesod-test zip-archive
  ];
  license = "AGPL";
}
